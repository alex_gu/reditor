#include "debugdialog.h"
#include "ui_debugdialog.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "rhighlighter.h"

DebugDialog::DebugDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DebugDialog)
{
    scene=NULL;
    ui->setupUi(this);
    new Highlighter(ui->plainTextEdit->document());
    window()->resize(300,parent->window()->height());
}

DebugDialog::~DebugDialog()
{
    delete ui;
}

void DebugDialog::setDebug(rScene *s, QStringList d,QStringList t)
{
    dInfo=d;
    scene=s;
    ui->plainTextEdit->setPlainText(t.join("\n"));
}

void DebugDialog::on_plainTextEdit_cursorPositionChanged()
{
    if(scene){//todo test on mult shemes
        int pos = ui->plainTextEdit->textCursor().blockNumber();
        if(pos<dInfo.length()){
            QString id=dInfo.at(pos).section(' ',1,1);
            if(id.contains("_")) id=id.section('_',0,0);
            scene->clearSelection();
            foreach (rSheme* sh, scene->shemes.values()) {
                foreach(rLogic* lo,sh->logics.values()){
                    if(lo->traId==id){
                        lo->setSelected(true);
                        scene->views()[0]->centerOn(lo);
                    }
                }
            }
        }

    }
}
