#ifndef RPLUGIN_H
#define RPLUGIN_H

#include "rplugin_global.h"
#include <extensionsystem/iplugin.h>
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/documentmanager.h>
#include <coreplugin/vcsmanager.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/ieditor.h>
#include <texteditor/autocompleter.h>
#include <texteditor/texteditorplugin.h>
#include <texteditor/basetexteditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/icore.h>
#include <coreplugin/minisplitter.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/openeditorsview.h>
#include <coreplugin/editormanager/openeditorswindow.h>
#include <texteditor/plaintexteditor.h>
#include <aggregation/aggregate.h>

class rIDocument;
class MainW;
class Options;
namespace rplugin {
namespace Internal {
class rpluginPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.rPlugin" FILE "rPlugin.json")

public:
    rpluginPlugin();
    ~rpluginPlugin();
    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();
private:
    rIDocument* getEditorDoc(Core::IEditor* e);
    rIDocument* curDoc();
    rIDocument* findDupDoc(rIDocument* doc);
    QList<rIDocument*> ridocs();
    QToolBar* actionsToolBar1;
    QToolBar* actionsToolBar2;
    QToolBar* actionsToolBar3;
    QPointer<rIDocument> focusEd;
    QTextStream bufferStream;
    QString bufferString;
    QTimer tautoupdate;
    QAction* advModeAct;
    MainW* mw;
private slots:
    void mainWRetranslated();
    void focusCall();
    void forceEditor();
    void triggerAction2();
    void autoupdate();
    void currentEditorChanged(Core::IEditor *editor);
    void currentEditorStateChanged(Core::IEditor *editor);
    void editorCreated(Core::IEditor *editor, const QString &fileName);
    void editorOpened(Core::IEditor *editor,bool nosign=false);
    void editorAboutToClose(Core::IEditor *editor);
    void editorsClosed(QList<Core::IEditor *> editors);
};

} // namespace Internal
} // namespace rplugin

#endif // RPLUGIN_H

