#include "rtranslator.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rnode.h"
#include "rctexedit.h"
#include "options.h"
#include "redselection.h"

int to_0(int v){
    return (v<0)?0:v;
}

rTranslator::rTranslator(QObject *parent, bool sim, bool inte) :
    QObject(parent)
{
    simple=sim;
    integrate=inte;
    tabs="    ";
    scene=NULL;
    s=NULL;
}

rTranslator::~rTranslator()
{
}

int rTranslator::doOut(rTextEdit *ed,QString ifemp,QString tab,QString id,QString bef,QString aft){
    if(!out){
        return 1;
    }
    if(integrate){
        tab="";
    }
    QString d;
    if(ed){
        d=ed->text();
    }
    bool empt=(!ed)||(d.length()==0);
    int l=0;
    if((integrate)&&(ed)){
        sl.append("//edit"+ed->id);st.append(id);
        sl.append("");st.append(id);
    }
    if(bef.length()){
        sl.append(tab+bef); st.append(id);
        l++;
    }
    if((empt)&&(ifemp.length())){
        if((l==0)||(is_comment_at_and(sl.last()))){
            sl.append(tab+ifemp); st.append(id);
            l++;
        }else{
            sl.last().append(ifemp);
        }
    }
    if((ed)&&(integrate||!empt)){
        int loffset=0;
        QStringList li=ed->text().split('\n');
        if(integrate){
            loffset=sl.last().length();
            if(loffset){
                sl.last().append(" ");
                loffset++;
            }
        }
        for(int i=0;i<li.count();i++){
            if((i==0)&&(l>0)){
                if(integrate){
                    loffset=sl.last().length();
                }
                sl.last().append(li[i]);
            }else{
                sl.append(tab+li[i]); st.append(id);
                l++;
            }
        }
        if(integrate){
            sl.append("");st.append(id);
            sl.append(tab+"//end"+QString::number(loffset));st.append(id);
        }
    }
    if(aft.length()){
        if((l==0)||(is_comment_at_and(sl.last()))){
            sl.append(tab+aft); st.append(id);
        }else{
            sl.last().append(aft);
        }
    }
    return 0;
}

rLogic* rTranslator::loById(QString id){
    foreach (rLogic* lo, s->logics.values()) {
        if(lo->traId==id)
            return lo;
    }
    return NULL;
}

QString rTranslator::clearTabs(QString s){
    while (s.left(1)=="\t"){
        s.remove(0,1);
    }
    return s;
}

int rTranslator::tabCount(QString &s){
    int res=0;
    int p=0;
    while(s[p++]=='\t'){
        res++;
    }
    return res;
}

void rTranslator::addTab(QString &s){
    s='\t'+s;
}

void rTranslator::remTab(QString &s){
    s.remove(0,1);
}


bool rTranslator::is_comment_at_and(QString s){
    return (s.indexOf("//")>=0);
}

QVector<QString> rTranslator::getLoPars(rLogic* lo,bool blockOnly, bool firstOnly){
    QVector<QString> ret;
    if(lo){
        do{
            lo=loById(lo->parentTraId);
            if(lo){
                ret.append(lo->traId);
                if(firstOnly){
                    break;
                }
            }

        }while(lo);
        if(blockOnly){
            foreach(QString id,ret){
                rLogic* ll=loById(id);
                if((!((ll->kind()==kSheme)||(ll->kind()==kSpecial)||(ll->kind()==kSpecialD)))){
                    ret.remove(ret.indexOf(id));
                }
            }
        }
    }
    ret.prepend("");
    return ret;
}

QString rTranslator::getLoParBl(rLogic* lo){
    QVector<QString>  t=getLoPars(lo,true,false);
    if(t.count()>1){
        return t[1];
    }else{
        return "";
    }
}


QString rTranslator::mainLoOfNo(int no_id,char call_dir, bool cond,rLogic *call, QList<QString> *parents,bool dies){
    if(call_dir=='r'){
        if((parents)&&(call)){
            QVector<rLogic*> l_l=(s->nodes.item(no_id)->logics('l','r'));
            for(int k=parents->count()-1;k>=0;k--){
                QString lo_id=(*parents)[k];
                rLogic* lo=loById(lo_id);
                if((lo)&&(l_l.contains(lo))){
                    if(lo!=call){
                        if((lo->kindS()==kSpecial)||
                                ((lo->kindS()==kSheme)&&(
                                     ((s->nodes.item(no_id)->logics('l','l').count()==0)||(dies))
                                     ||(!cond)
                                     ))){
                            if((dies==false)||(lo->kind()==kSheme)){
                                return lo->traId+"e";
                            }
                        }
                    }
                }
            }
        }
    }
    if(dies) cond=false;
    QVector<rLogic*> ll=s->nodes.item(no_id)->logics('l','l');
    QVector<rLogic*> rl=s->nodes.item(no_id)->logics('r','r');
    if((rl.count())&&(rl.first()->kind()==kSheme)) rl.remove(0);
    if(noDir(no_id)=='r'){
        if((call_dir=='l')&&(rl.count())||
                ((call_dir=='r')&&(cond==false)&&(rl.count()))){
            ll.clear();
        }
        if((ll+rl).count()){
            return (ll+rl).first()->traId;
        }
    }else{
        if((call_dir=='r')&&(ll.count())||
                ((call_dir=='l')&&(cond==false)&&(ll.count()))){
            rl.clear();
        }
        if((rl+ll).count()){
            return (rl+ll).first()->traId;
        }
    }
    return id_no_way;
}

rLogic* rTranslator::invLastlogpath(int node){
    if(inv_lastLogPath_cache.keys().contains(node)){
        return inv_lastLogPath_cache[node];
    }
    rLogic* res=NULL;
    int n;
    foreach(rLogic* lo,s->logics.values()){
        if(lo->ignoreInLogList){
            continue;
        }
        n=-1;
        if(lastLogPath(lo,&n)){
            if(n==node){
                res=lo;
                break;
            }
        }
    }
#ifdef mdebug
    qDebug()<<"inv_lastLogPath no:"<<node<<" res:"<<(res?res->traId:"NULL");
#endif
    if((inv_lastLogPath_cache.keys().contains(node))){
        qDebug()<<"inv_lastLogPath cache error";
    }else{
        inv_lastLogPath_cache.insert(node,res);
    }
    return res;
}

bool rTranslator::firstLogPath(rLogic* call,int endnode){
    bool rd=(call->dir()=='r');//todo
    char chr=rd?'r':'l';
    char chl=rd?'l':'r';
    bool res=false;
    rLogic* firstlog;
    firstlog=call;
    while((firstlog)&&(endnode!=(rd?firstlog->to:firstlog->fr))){
        QVector<rLogic*> tol=s->nodes.item(rd?firstlog->to:firstlog->fr)->logics(chl,chr);//todo lgen
        if((tol.indexOf(firstlog)!=0)||
                ((rd)&&(firstlog->lx+firstlog->lw>s->nodes.item(endnode)->lx))||
                ((!rd)&&(firstlog->lx<s->nodes.item(endnode)->lx))){
            firstlog=NULL;
            break;
        }
        QVector<rLogic*> frl=s->nodes.item(rd?firstlog->to:firstlog->fr)->logics(chr,chr);//todo lgen
        if((frl.count())&&(noDir(rd?firstlog->to:firstlog->fr)==chr)){
            firstlog=frl[0];
        }else{
            firstlog=NULL;
        }
    }
    if(firstlog){
        res=true;
    }
    return res;
}

bool rTranslator::recurLast(rLogic* call, rLogic* lmain,int* end){
    bool rd=(call->dir()=='r');
    char chr=rd?'r':'l';
    char chl=rd?'l':'r';
    qreal call_ly=call->ly;

    foreach(rLogic* l,call->prodSeqList){
        call_ly=qMin(call_ly,l->ly);
    }
    QVector<rLogic*> fr=s->nodes.item(rd?lmain->fr:lmain->to)->logics(chr,chr);
    if((fr.count())&&(fr[0]->kind()==kSheme)){
        if(fr[0]==call){
            return false;
        }
        fr.remove(0);
    }
    int plast=-1;
    for(int i=fr.indexOf(lmain);i<fr.count();i++){
        plast=i;
        if(fr[i]->kindS()!=(rd?kRight:kLeft)){
            break;
        }
    }
    bool find=false;
    int ke=fr.indexOf(lmain);
    for(int i=plast;i>=ke;i--){
        rLogic* last=fr[i];
        if(call==last){
            continue;
        }
        QVector<rLogic*> tol=s->nodes.item(rd?last->to:last->fr)->logics(chl,chr,true);
        QString p=getLoParBl(last);
        foreach(rLogic* l,tol){
            if(p!=getLoParBl(l)){
                tol.remove(tol.indexOf(l));
            }
        }
        bool cond1=(/*(last->kindS()==(rd?kSpecial:kSpecial))||*/
                    (tol.indexOf(last)==tol.count()-1));
        if(call_ly>=s->nodes.item(rd?last->to:last->fr)->ly){
            if(firstLogPath(call,rd?last->to:last->fr)){
                find=true;
                if(((call_ly==s->nodes.item(rd?last->to:last->fr)->ly))&&(cond1)){
                    int nend=(rd?last->to:last->fr);
                    if((*end<0)){
                        *end=nend;
                    }
                }
            }
        }else{
            if((cond1)&&(noDir(rd?last->to:last->fr)==chr)){
                QVector<rLogic*> to_out=s->nodes.item(rd?last->to:last->fr)->logics(chr,chr);
                if((to_out.count())&&(getLoParBl(to_out[0])==p)&&(recurLast(call,to_out[0],end))){
                    find=true;
                }
            }
        }
        if((find)&&((*end)>=0)){            
            break;
        }
    }
    return find;
}

bool rTranslator::lastLogPath(rLogic *call,int* node,QList<QString> *parents){
    if(iteration==0){
        return false;
    }
    if(lastLogPath_cache.keys().contains(call)){
        if(node){
            *node=lastLogPath_cache[call][1];
        }
        return lastLogPath_cache[call][0];
    }
    bool rd=(call->dir()=='r');
    char chr=rd?'r':'l';
    char chl=rd?'l':'r';
    bool res=false;
    int resnode=-1;
    if(noDir(rd?call->fr:call->to)==chr){
        res=recurLast(call,call,&resnode);
    }

#ifdef mdebug
    qDebug()<<"lastLogPath lo:"<<call->traId<<" res:"<<res<<" node:"<<resnode;
#endif
    if((lastLogPath_cache.keys().contains(call))){
        qDebug()<<"lastLogPath cache error";
    }else{
        QList<int> rr;
        rr.append(res);
        rr.append(resnode);
        lastLogPath_cache.insert(call,rr);
    }
    if((res)&&(node)){
        *node=resnode;
    }
    return res;
}

rLogic* rTranslator::invSeqnext(QString next){
    if(inv_seqNext_cache.keys().contains(next)){
        return inv_seqNext_cache[next];
    }
    rLogic* res=NULL;
    foreach(rLogic* lo,s->logics.values()){
        if(lo->ignoreInLogList){
            continue;
        }
        if(seqNext(lo,NULL,1)==next){
            res=lo;
            break;
        }
    }
#ifdef mdebug
    qDebug()<<"inv_seqNext next:"<<next<<" res:"<<(res?res->traId:NULL);
#endif
    if((inv_seqNext_cache.keys().contains(next))){
        qDebug()<<"inv_seqNext cache error";
    }else{
        inv_seqNext_cache.insert(next,res);
    }
    return res;
}

QString rTranslator::seqNext(rLogic *call, QList<QString> *parents, int cachen){
    if(seqNext_cache[cachen].keys().contains(call)){
        return seqNext_cache[cachen][call];
    }
    QString res="";
    bool rd=(call->dir()=='r');//todo
    char chr=rd?'r':'l';
    char chl=rd?'l':'r';
    QVector<rLogic*> frs=s->nodes.item(rd?call->fr:call->to)->logics(chr,chr);
    if((noDir(rd?call->fr:call->to)==chr)||(frs.indexOf(call)>0)){
        if(!simple){
            int nn=-1;
            if(lastLogPath(call,&nn,parents)){
                if(nn>=0){
                    res=mainLoOfNo(nn,chr,true,call,parents);
                }
            }
        }//candidates at call.to
        if((res.count()==0)&&(noDir(rd?call->to:call->fr)==chr)){
            QVector<rLogic*> cand;
            foreach(rLogic* cl,s->nodes.item(rd?call->to:call->fr)->logics(chl,chr,true)){
                QVector<rLogic*> fr=s->nodes.item(rd?cl->fr:cl->to)->logics(chr,chr);
                int pos_block=0;
                for(int p=fr.indexOf(cl)-1;p>=0;p--){
                    if(fr[p]->kindS()!=(rd?kRight:kLeft)){
                        break;
                    }
                    pos_block++;
                }
                if((cl->kindS()!=kSheme)&&
                        ((simple)||(!lastLogPath(cl,NULL,parents)))&&
                        ((pos_block==0)||(fr.indexOf(cl)==fr.count()-1)||(cl->kindS()==(rd?kSpecial:kSpecial)))){
                    cand+=cl;
                }
            }
            rLogic* lastLogN=NULL;
            if(!simple){
                lastLogN=invLastlogpath(rd?call->to:call->fr);
                if(lastLogN){
                    cand.prepend(lastLogN);
                }
            }
            rLogic* match_cand=NULL;
            int cand_node=rd?call->to:call->fr;
            if(cand.count()) {
                int nn=to_0(genLoOfNo_no_map[cand_node]);
                if(iteration>=1){
                    if((nn<cand.count())&&(vis_iteration[iteration-1].contains(cand[nn]->traId))){
                    }else{
                        if(nn<cand.count()){
                            if(!genLoOfNo_no_map_used){
                                genLoOfNo_no_map_used=true;
                                genLoOfNo_no_map[cand_node]++;
                            }
                        }
                    }
                }
                nn=to_0(genLoOfNo_no_map[cand_node]);
                if(nn<cand.count()){
                    match_cand=cand[nn];
                }
            }
            if((match_cand)&&(match_cand==call)){
                if(!simple){
                    if(lastLogN==match_cand){
                        lastLogPath(lastLogN,&cand_node);
                    }
                }
                res=mainLoOfNo(cand_node,chr,true,match_cand,parents);
            }
        }
    }else{
        if(frs.indexOf(call)==0){
            QVector<rLogic*> nos=s->nodes.item(rd?call->fr:call->to)->logics(chl,chl);
            if(nos.count()){
                res=nos[0]->traId;
            }
        }
    }
#ifdef mdebug
    qDebug()<<"seqNext lo:"<<call->traId<<" res:"<<res<<" cache n:"<<cachen;
#endif
    if((seqNext_cache[cachen].keys().contains(call))){
        qDebug()<<"seqNext cache error";
    }else{
        seqNext_cache[cachen].insert(call,res);
    }
    return res;
}

char rTranslator::noDir(int no_id){
    if(s->nodes.key(s->startNode())==no_id){
        return 'r';
    }
    if(s->nodes.key(s->endNode())==no_id){
        return 'r';
    }
    QVector<rLogic*> ll=s->nodes.item(no_id)->logics('l','r',true);
    QVector<rLogic*> rl=s->nodes.item(no_id)->logics('r','l',true);
    if((rl.count())&&(ll.count()==0||(ll[0]->ly>rl[0]->ly))){
        return 'l';
    }
    return 'r';
}

bool rTranslator::needTrans(QString &sOld, QString &sNew,rLogic *lo, bool &needUndo)
{
    needUndo=true;
    if((!sOld.length())||(!sNew.length())) {
        return true;
    }
    scene=lo->sheme->scene;
    if(lo->shreplace){
        return true;
    }
    if((lo->UpText==lo->sheme->header())&&(lo->sheme->loreplace)){
        return true;
    }
    findRepls();
    if(lo->shreplace){
        return true;
    }
    if((lo->UpText==lo->sheme->header())&&(lo->sheme->loreplace)){
        return true;
    }
    if((lo->kindS()==kSheme)||(lo->kindS()==kSpecial)){
        QList<QString> pats;
        pats.append("do");
        pats.append("switch");
        pats.append("case");
        pats.append("default");
        int p1=-1;
        int p2=-1;
        foreach(QString s,pats){
            if(sOld.indexOf(s)>=0){
                p1=pats.indexOf(s);
                break;
            }
        }
        foreach(QString s,pats){
            if(sNew.indexOf(s)>=0){
                p2=pats.indexOf(s);
                break;
            }
        }
        if(p1!=p2){
            return true;
        }
    }
    return false;
}

void rTranslator::CleanTabSpace(QStringList &ut){
    for(int i=0;i<ut.length();i++){
        QString s=ut[i];
        while((s.length())&&((s[0]=='\t')||(s[0]==' '))) s.remove(0,1);
        ut.replace(i,s);
    }
}

int rTranslator::proc(rLogic* lo,QString tab,QString par_b, bool full){
    if(lo==NULL){
        return -1;
    }
    QString id=lo->traId;
    if(vis.contains(id)){
        return -1;
    }
    QVector<rLogic*> t_l;
    vis.append(id);
    parents.append(id);
    lo->parentTraId=par_b;
    bool  empty_cond=lo->UpText->isEmpty();
    bool false_cond=false;
    QString emptyrep="";
    doOut(0,label+id+":"+(((empty_cond)&&(lo->DownText->isEmpty())&&
                            ((lo->kindS()==kLeft)||(lo->kindS()==kRight)))?";":""),tab,"lab "+id);
    if((lo->kind()!=kSheme)&&(lo->prodSeq)){
        foreach(rLogic* l2,lo->prodSeqList){
            proc(l2,tab,par_b,false);
        }
    }
    bool dies=(lo->kindS()!=lo->kind());
    switch (lo->kind()){
    case kRight:
    case kRightD:
    case kLeft:
    case kLeftD:
    {
        if(isSimpleBlockGen(lo)){
            doOut(lo->UpText,"",tab,"lup "+id,"","{");
            doOut(lo->DownText,"",tab+"\t","ldo "+id);
            QString next=mainLoOfNo(lo->dest(),lo->dir(),true,lo,&parents);
            proc(loById(next),"\t"+tab,par_b);
            doOut(0,"}",tab,"end "+id);
            parents.removeLast();
            if(lo->dir()=='l'){
                QString g_next=mainLoOfNo(((lo->dir()=='r')?s->nodes.key(s->endNode()):s->nodes.key(s->startNode())),lo->dir(),true,lo,&parents,dies);
                doOut(0,"goto "+label+g_next+";",tab,"got "+id+" "+g_next);
            }
        }else if((lo->shreplace)||(isSimpleSeqGen(lo))){
            doOut(lo->UpText,"",tab,"lup "+id,"","");
            doOut(lo->DownText,"",tab,"ldo "+id);
            QString next=mainLoOfNo(lo->dest(),lo->dir(),true,lo,&parents);
            doOut(0,(lo->shreplace)?" ":"",tab,"end "+id);//!!
            parents.removeLast();
            next=seqNext(lo,&parents);
            if(next.count()>0){
                doOut(0,"goto "+label+next+";",tab,"got "+id+" "+next);
                proc(loById(next),tab,par_b);
            }
            proc(loById(next),tab,par_b);
        }else{
            t_l=s->nodes.item(lo->sour())->logics(lo->dir(),lo->dir());
            int pos_block=0;
            for(int p=t_l.indexOf(lo)-1;p>=0;p--){
                if(t_l[p]->dir()!=lo->dir()){
                    break;
                }
                pos_block++;
            }
            if((t_l.indexOf(lo)<t_l.count()-1)){
                if((empty_cond)&&(!lo->ignoreInLogList)){//square
                    empty_cond=false;
                    emptyrep="true";
                }
            }
            if(!empty_cond){
                doOut(lo->UpText,emptyrep,tab,"lup "+id,"if(","){");
            }else{
                doOut(lo->UpText,"",tab,"lup "+id);
            }
            doOut(lo->DownText,"",tab+(empty_cond?"":"\t"),"ldo "+id);
            QString next=mainLoOfNo(lo->dest(),lo->dir(),true,lo,&parents);
            lo->next=next;
            if(full){
                QString g_next=next;
                if(dies){
                    g_next=mainLoOfNo(lo->dest(),lo->dir(),true,lo,&parents,true);
                }
                doOut(0,QString(empty_cond?"":"\t")+"goto "+label+g_next+";",tab,"got "+id+" "+g_next);
                if((invSeqnext(next)==NULL)){
                    proc(loById(next),(empty_cond?"":"\t")+tab,id);
                }
            }
            if(!empty_cond){
                doOut(0,"}else{",tab,"els "+id);
                if(full){
                    if(t_l.indexOf(lo)+1<t_l.count()){
                        proc(t_l[t_l.indexOf(lo)+1],(false_cond?"":"\t")+tab,id);
                    }else{
                        if(noDir(lo->sour())==lo->dir()){
                            QString nextf=mainLoOfNo(lo->dest(),lo->dir(),false,lo,&parents);
                            if(dies) nextf=mainLoOfNo(lo->dest(),lo->dir(),false,lo,&parents,true);
                            doOut(0,QString("\t")+"goto "+label+nextf+";",tab,"got "+id+" "+nextf);
                        }
                    }
                }
                doOut(0,"}",tab,"end "+id);
            }
            parents.removeLast();
            if(full){
                next=seqNext(lo,&parents);
                if(next.count()>0){
                    doOut(0,"goto "+label+next+";",tab,"got "+id+" "+next);
                    proc(loById(next),tab,par_b);
                }
            }
        }
    }break;
    case kSpecial:
    case kSpecialD:
    case kSheme:{
        QString header=lo->UpText->text();
        int htype=0;
        if((lo->kindS()==kSpecial)&&(header.length()==0)){
            htype=1;
        }else if(header.left(6)=="switch"){
            do{
                if(header.left(7)=="switch("){
                    htype=2;
                    break;
                }
                header.remove(6,1);
            }while(header.length()>7);
        }
        if((htype>0)){
            if((htype==2)||(integrate)){
                doOut(lo->UpText,"",tab,"lup "+id,"","{");
                tab+="\t";
            }
            doOut(lo->DownText,"",tab,"ldo "+id);
            t_l=s->nodes.item(lo->fr)->logics('r','r');
            for(int i=t_l.indexOf(lo)+1;i<t_l.count();i++){
                vis.append(t_l[i]->traId);
                t_l[i]->parentTraId=id;
                if(htype==2){
                    doOut(t_l[i]->UpText,"default:",tab,"lup "+t_l[i]->traId,"","{");
                }else if(htype==1){
                    doOut(t_l[i]->UpText,"true",tab,"lup "+t_l[i]->traId,"while(","){");
                }
                doOut(t_l[i]->DownText,"",tab+"\t","ldo "+t_l[i]->traId);
                QString next=mainLoOfNo(t_l[i]->to,'r',true,t_l[i],&parents);
                QString g_next=next;
                if(t_l[i]->kindS()!=t_l[i]->kind()) g_next=mainLoOfNo(t_l[i]->to,'r',true,t_l[i],&parents,true);
                int fix_start=to_0(sl.count()-1);
                doOut(0,QString("\t")+"goto "+label+g_next+";",tab,"got "+t_l[i]->traId+" "+g_next);
                QVector<rLogic*> logs_to=s->nodes.item(t_l[i]->to)->logics('l','r');
                if((((logs_to.last()==t_l[i])||(((loById(next))&&(loById(next)->kindS()==kLeft)&&(logs_to.count()>1)&&(logs_to[1]==lo)))))//main| left at sheme end
                        ){
                    proc(loById(next),tab+"\t",t_l[i]->traId);
                }
                if(out){
                    for(int p=fix_start;p<sl.count();p++){
                        QString sst=st[p];
                        QString ssl=sl[p];
                        if(sst.section(' ',2,2)==lo->traId+"e"){
                            sst=sst.replace(id+"e",t_l[i]->traId+"e");
                            ssl=ssl.replace(label+id+"e",label+t_l[i]->traId+"e");
                            st.replace(p,sst);
                            sl.replace(p,ssl);
                        }
                    }
                }
                doOut(0,QString("\t")+label+t_l[i]->traId+"e"+":;",tab,"lab "+t_l[i]->traId+"e");
                if(htype==1){
                    doOut(0,"}",tab,"end "+t_l[i]->traId);
                }else if(htype==2){
                    doOut(0,"}break;",tab,"ldo "+t_l[i]->traId);
                }
            }
            if(htype==2){
                tab.remove(0,1);
                doOut(0,"}",tab,"end "+id);
            }
        }else{
            bool reverse=false;
            if(header.section(' ',0,0)=="do"){
                reverse=true;
            }
            if((!empty_cond)||(integrate)){
                doOut(lo->UpText,"",tab,"lup "+id,"",((!empty_cond)?"{":""));
            }
            if(!reverse){
                doOut(lo->DownText,"",tab+(empty_cond?"":"\t"),"ldo "+id);
            }
            t_l=s->nodes.item(lo->fr)->logics('r','r');
            if(full){
                if(t_l.indexOf(lo)+1<t_l.count()){
                    proc(t_l[t_l.indexOf(lo)+1],"\t"+tab,id);
                }
            }
            doOut(0,QString("\t")+label+id+"e"+":;",tab,"lab "+id+"e");
            if((!empty_cond)||(integrate)){
                doOut(0,((!empty_cond)?"}":""),tab,"end "+id);
                if(reverse){
                    doOut(lo->DownText,"",tab,"lupr "+id,"",";");
                }
            }
        }
        QString next=mainLoOfNo(lo->to,'r',true,lo,&parents);
        parents.removeLast();
        if(lo->kind()!=kSheme){
            if(full){
                QString g_next=next;
                if(dies){
                    g_next=mainLoOfNo(lo->to,'r',true,lo,&parents,true);
                }
                doOut(0,"goto "+label+g_next+";",tab,"got "+id+" "+g_next);
                if((noDir(lo->fr)=='l')&&(noDir(lo->to)=='r')){
                    proc(loById(next),tab,par_b);
                }
                next=seqNext(lo,&parents);
                if(next.count()>0){
                    doOut(0,"goto "+label+next+";",tab,"got "+id+" "+next);
                    proc(loById(next),tab,par_b);
                }
            }
        }
    }break;
    }
    return 0;
}

void rTranslator::translateSheme(rSheme* she,QHash<int,int> &HLogs,QStringList &result,QStringList &deb_info,int lang,bool no_output){
    QTime myTimer;
    myTimer.start();
    label="_l";
    s=she;
    out=!no_output;
    int id=1;
    while(HLogs.contains(id)){
        id++;
    }
    id_start=QString::number(id);
    foreach(rLogic* lo,s->logics.values()){
        id=1;
        while(HLogs.contains(id)){
            id++;
        }
        HLogs.insert(id,1);
        lo->traId=QString::number(id);
        lo->parentTraId="-1";
    }
    id=1;
    while(HLogs.contains(id)){
        id++;
    }
    id_end=QString::number(id);
    id_no_way="_NO_WAY";
    foreach(rLogic* lo,s->logics.values()){
        lo->ignoreInLogList=false;
    }
    foreach(rLogic* lo,s->logics.values()){
        if(lo->ignoreInLogList){
            continue;
        }
        bool ex=false;
        if((s->nodes.item(lo->fr)->kind()==kSquare)&&(s->nodes.item(lo->to)->kind()==kSquare)){
            if((lo->kindS()!=kLeft)&&(lo->kind()!=kSheme)){
                foreach(rLogic* lo2,s->nodes.item(lo->fr)->logics('r','r')){
                    if(lo2->kind()==kSheme){
                        continue;
                    }
                    if((lo2!=lo)&&(lo2->to==lo->to)){
                        ex=true;
                        break;
                    }
                }
            }else if(lo->kindS()==kLeft){
                foreach(rLogic* lo2,s->nodes.item(lo->to)->logics('l','l')){
                    if((lo2!=lo)&&(lo2->fr==lo->fr)){
                        ex=true;
                        break;
                    }
                }
            }
        }
        if(ex){
            lo->ignoreInLogList=true;
        }
        lo->prodSeq=false;
        lo->prodSeqList.clear();
    }
    foreach(rLogic* lo,s->logics.values()){
        if(lo->ignoreInLogList){
            if(lo->kindS()!=kLeft){
                foreach(rLogic* l2,s->nodes.item(lo->fr)->logics('r','r')){
                    if(l2->to==lo->to){
                        l2->prodSeq=true;
                        l2->prodSeqList.append(lo);
                    }
                }
            }else{
                foreach(rLogic* l2,s->nodes.item(lo->to)->logics('l','l')){
                    if(l2->fr==lo->fr){
                        l2->prodSeq=true;
                        l2->prodSeqList.append(lo);
                    }
                }
            }
        }
    }
    vis_iteration.clear();
    foreach(int no_id,s->nodes.keys()){
        genLoOfNo_no_map.insert(no_id,-1);
    }
    iteration=0;
    do{
#ifdef mdebug
        qDebug()<<"step:"<<iteration;
#endif
        if(!simple){
            out=!no_output;
            if(iteration==0){
                out=false;
            }
        }
        vis.clear();
        parents.clear();
        st.clear();
        sl.clear();
        genLoOfNo_no_map_used=false;
        seqGenLoOfNo_cache.clear();
        seqNextNoOfLo_cache.clear();
        seqNext_cache[0].clear();
        seqNext_cache[1].clear();
        inv_seqNext_cache.clear();
        lastLogPath_cache.clear();
        inv_lastLogPath_cache.clear();
        if(s->startNode()->logics('r','r').count()){
            proc(s->startNode()->logics('r','r')[0],"","-1");
        }
        vis_iteration.append(vis);
        iteration++;
    }while((iteration<=(simple?0:1))||((vis.count()<s->logics.count())
                                       &&(genLoOfNo_no_map_used)&&(iteration<99999)));
    sl.append(label+id_end+":");st.append("lab "+id_end);
    if(out){
        bool cleaned;
        do{
            cleaned=false;
            for(int i=0;i<sl.count();i++){
                QString ist=st[i];
                if(ist.section(' ',0,0)=="got"){
                    QString ito=ist.section(' ',2,2);
                    if((i+1<st.count())&&
                            (st[i+1].section(' ',0,0)=="lab")&&(st[i+1].section(' ',1,1)==ito)){
                        sl.removeAt(i);
                        st.removeAt(i);
                        cleaned=true;
                        i--;
                    }else{
                        int p=i+1;
                        while(p<st.length()){
                            if(((st[p].section(' ',0,0)=="lab")&&(st[p].section(' ',1,1)==ito))||
                                    ((st[p].section(' ',0,0)=="got")&&(st[p].section(' ',2,2)==ito))){
                                sl.removeAt(i);
                                st.removeAt(i);
                                cleaned=true;
                                i--;
                                break;
                            }else{
                                if((st[p].section(' ',0,0)=="els")){
                                    QString bi=st[p].section(' ',1,1);
                                    while(!(((st[p].section(' ',0,0)=="end")
                                             &&(loById(st[p].section(' ',1,1))->kindS()!=kSheme)
                                             &&(loById(st[p].section(' ',1,1))->kindS()!=kSpecial)
                                             )
                                            &&(st[p].section(' ',1,1)==bi))){
                                        p++;
                                    }
                                    p++;
                                }else if((sl[p].section('\t',-1,-1)==" ")||(sl[p].section('\t',-1,-1)=="")||
                                         ((st[p].section(' ',0,0)=="end")
                                          &&(loById(st[p].section(' ',1,1))->kindS()!=kSheme)//!!
                                          &&(loById(st[p].section(' ',1,1))->kindS()!=kSpecial)
                                          )||(st[p].section(' ',0,0)=="lab")){
                                    p++;
                                }else break;
                            }
                        }
                    }


                }
            }
        }while (cleaned);
        QHash<QString,int> us_lab;
        foreach(QString s,st){
            if(s.section(' ',0,0)=="got"){
                QString li=s.section(' ',2,2);
                if(!us_lab.contains(li))
                    us_lab.insert(li,1);
            }
        }
        for(int i=0;i<sl.count();i++){
            if(st[i].section(' ',0,0)=="lab"){
                if(!us_lab.contains(st[i].section(' ',1,1))){
                    sl.removeAt(i);
                    st.removeAt(i);
                    i--;
                }
            }
        }
        for(int i=0;i<sl.count()-1;i++){
            if((st[i].section(' ',0,0)=="els")&&(st[i+1].section(' ',0,0)=="end")){
                sl.removeAt(i);
                st.removeAt(i);
                i--;
            }
        }
    }
    QVector<rSheme*> nupd;
    foreach(rLogic* lo,s->logics.values()){
        bool layit=false;
        if(!vis.contains(lo->traId)){
            lo->markr=true;
            layit=true;
            if(integrate){
                doOut(lo->UpText,"","","lup"+lo->traId);
                doOut(lo->DownText,"","","lup"+lo->traId);
            }
        }else{
            if(lo->markr){
                lo->markr=false;
                layit=true;
            }
        }
        if(layit) if(!nupd.contains(lo->sheme)) nupd.append(lo->sheme);
    }
    foreach(rSheme* s,nupd){
        s->llayout();
    }
    result+=sl;
    deb_info+=st;
    static int ccc;
    qDebug()<<"Translated :"<<ccc++<<"in"<<myTimer.elapsed()<< "steps:"<<iteration;
}

void rTranslator::translateScene(rScene *sce,QStringList* rresult,QStringList* rd_info)
{
    scene=sce;
    findRepls();
    QHash<int,int> HLabs;
    QHash<rSheme*,QStringList> h_result,h_d_info;
    QHash<rLogic*,rSheme*> loTextSheme;
    bool intg=integrate;
    foreach(rSheme* she,sce->shemes.values()){
        QStringList result,d_info;
        translateSheme(she,HLabs,result,d_info,0);
        h_result.insert(she,result);
        h_d_info.insert(she,d_info);
        foreach(rLogic* lo,she->logics.values()){
            loTextSheme.insert(lo,lo->sheme);
        }
    }
    foreach(rSheme* she,sce->shemes.values()){
        if((she->loreplace)&&(h_result.keys().contains(she))){
            rSheme* insShe=loTextSheme[she->loreplace];
            if((insShe!=she)&&(h_result.contains(insShe))){
                foreach(rLogic* lo,she->logics.values()){
                    loTextSheme[lo]=insShe;
                }
                int posb=0;
                while((posb<h_d_info[insShe].count())&&(h_d_info[insShe][posb]!=
                                                        (she->loreplace->shreplaceUp?"lup ":"ldo ")+she->loreplace->traId)){
                    posb++;
                }
                int pose=posb+1;
                while((pose<h_d_info[insShe].count())&&(h_d_info[insShe][pose]!="end "+she->loreplace->traId)) pose++;
                if(she->header()){
                    if(intg){
                        int c=0;
                        while(h_d_info[she][c].indexOf(she->firstLogic->traId)==4) c++;
                        if(c){
                            h_d_info[she].prepend("comb");
                            h_result[she].prepend("/*");
                            h_d_info[she].insert(c+1,"come");
                            h_result[she].insert(c+1,"*/");
                            h_result[she].last().prepend("//");
                        }
                    }else{
                        while(h_d_info[she][0].indexOf(she->firstLogic->traId)==4){
                            h_d_info[she].removeFirst();
                            h_result[she].removeFirst();
                        }
                        if(h_d_info[she].last()=="end "+she->firstLogic->traId){
                            h_d_info[she].removeLast();
                            h_result[she].removeLast();
                        }
                    }
                }
                int p=pose+1;
                int dTab=tabCount(h_result[insShe][posb]);
                int sTab=tabCount(h_result[she][0]);
                for(int i=0;i<h_d_info[she].count();i++){
                    h_d_info[insShe].insert(p,h_d_info[she][i]);
                    for(int k=0;k<sTab;k++){
                        remTab(h_result[she][i]);
                    }
                    for(int k=0;k<dTab;k++){
                        addTab(h_result[she][i]);
                    }
                    h_result[insShe].insert(p,h_result[she][i]);
                    p++;
                }
                h_result.remove(she);
                h_d_info.remove(she);
                if(intg){
                    h_d_info[insShe].insert(pose+1,"come");
                    h_result[insShe].insert(pose+1,"*/");
                    h_d_info[insShe].insert(posb,"comb");
                    h_result[insShe].insert(posb,"/*");
                }else{
                    for(int k=0;k<pose-posb+1;k++){
                        h_result[insShe].removeAt(posb);
                        h_d_info[insShe].removeAt(posb);
                    }
                }
            }
        }
    }
    QStringList result,d_info;
    foreach(rSheme* she,sce->sortedShemes()){
        if(h_result.keys().contains(she)){
            result.append(h_result[she]);
            d_info.append(h_d_info[she]);
        }
    }
    if(rresult){
        *rresult=result;
    }
    if(rd_info){
        *rd_info=d_info;
    }
}

void rTranslator:: revTranslateSheme(rScene* sce){
    return;
}

bool rTranslator::isSimpleBlockGen(rLogic* lo){
    QStringList cand=Options::instance()->cppBlockgen;
    if(((lo->kindS()==kLeft)||(lo->kindS()==kRight))&&
            (lo->sheme->nodes.item(lo->sour())->logics(lo->dir(),lo->dir()).last()==lo)){
        foreach(QString s,cand){
            if((s.length())&&(lo->UpText->text().indexOf(s)==0)&&
                    ((lo->UpText->text()[s.length()]==' '))){
                return true;
            }
        }
    }
    return false;
}

bool rTranslator::isSimpleSeqGen(rLogic* lo){
    QStringList cand=Options::instance()->cppSeqgen;
    if(((lo->kindS()==kLeft)||(lo->kindS()==kRight))&&
            (lo->sheme->nodes.item(lo->sour())->logics(lo->dir(),lo->dir()).last()==lo)){
        foreach(QString s,cand){
            if((s.length())&&(lo->UpText->text().indexOf(s)==0)&&
                    ((lo->UpText->text()[s.length()]==' ')||
                     (lo->UpText->text()[s.length()]==':'))){
                return true;
            }
        }
    }
    return false;
}
void rTranslator::findRepls(){
    foreach(rSheme* shem,scene->shemes.values()){
        shem->loreplace=NULL;
    }
    foreach(rSheme* shem,scene->shemes.values()){
        foreach(rLogic* lo,shem->logics.values()){
            lo->shreplace=NULL;
            if((lo->kindS()==kRight)||(lo->kindS()==kLeft)){
                QVector<rLogic*> fr=shem->nodes.item(lo->sour())->logics(lo->dir(),lo->dir());
                if(fr.last()==lo){
                    foreach(rSheme* sher,scene->shemes.values()){
                        if((shem!=sher)&&(sher->header())&&(!lo->UpText->isEmpty())&&
                                ((lo->UpText->text()==sher->header()->text())||//up=sheme
                                 ((lo->UpText->isEmpty())&&(lo->DownText->text()==sher->header()->text())))//down=sheme
                                ){
                            lo->shreplaceUp=!lo->UpText->isEmpty();
                            lo->shreplace=sher;
                            sher->loreplace=lo;
                        }
                    }
                }
            }
        }
    }
}

void rTranslator::simplify(rScene* s){
    scene=s;
    QTextStream bStream;
    QString bString;
    bStream.setString(&bString);
    bool onepoc;
    QTextStream selSave;
    QString s2;
    selSave.setString(&s2);
    scene->getSel()->saveToStream(selSave);
    do{
        onepoc=false;
        //scene->clearElipseSel();
        //scene->clearSelection();
        findRepls();
        foreach(rSheme* she,scene->shemes.values()){
            if(she->loreplace){
                scene->getSel()->placePoint1(she->loreplace->getSelPoint(she->loreplace->dir()));
                QList<rLogic*> lc=she->logics.values();
                if(she->header())
                    lc.removeFirst();
                bString.clear();
                foreach(rLogic* l,lc)
                    l->setSelected(true);
                if(scene->elemCopy(bStream)){
                    bStream.seek(0);
                    scene->elemPaste(bStream,NULL,false,false,false);
                    foreach(rLogic* l,she->logics.values())
                        l->setSelected(true);
                    she->loreplace->setSelected(true);
                    scene->elemDelete(NULL,false);
                    onepoc=true;
                    break;
                }
            }
        }
    }while(onepoc);
    selSave.seek(0);
    scene->getSel()->loadFromStream(selSave);
    scene->changed();
}
