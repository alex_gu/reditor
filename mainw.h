#ifndef MAINW_H
#define MAINW_H
#define CACHE_M ItemCoordinateCache

#include <qtsingleapplication.h>
#include <QMainWindow>
#include <QTranslator>
#include <QPrinter>
#include <QPrintDialog>
#include <QMenu>
#include <QTimer>
#include <QDir>
#include "documentwindow.h"
#include "ui_mainw.h"

#ifdef DEFINTEG
#define DocumentWindow QMainWindow
#endif

class rLogic;
class rNode;
class rScene;
class rDocument;
namespace Ui {
class MainW;
}

class MainW : public DocumentWindow
{
    Q_OBJECT
public:
    explicit MainW(QWidget *parent = 0,bool create_doc=true);
    ~MainW();
    void setIntgScene(rScene* sc);
    Ui::MainW* gui(){return ui;}
    bool eventFilter(QObject *obj, QEvent *event);
    void showToolBar(bool b);
private slots:
    void sceneShanged();
    void checkToolBar(bool r=true);
    void focusChanged(QWidget *old, QWidget *now);
    void retranslate(QString id, QString qid);
    void on_actionCopy_triggered(bool* ok=NULL);
    void on_actionDelete_triggered();
    void on_actionInsert_triggered();
    void on_actionInsertLogic_triggered();
    void on_actionUndo_2_triggered();
    void on_actionRedo_2_triggered();
    void on_tabWidget_tabCloseRequested(int index);
    void on_treeWidget_itemChanged(QTreeWidgetItem *item, int column);
    void on_tabWidget_currentChanged(int index);
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_tabWidget_currentChanged(QWidget *arg1);
    void on_actionClose_triggered();
    void on_actionNew_triggered();
    void on_actionNew_Folder_triggered();
    void on_actionNew_Document_triggered();
    void on_actionSave_All_triggered();
    void on_actionClose_All_triggered();
    void on_treeWidget_customContextMenuRequested(const QPoint &pos);
    void on_actionHide_text_triggered();
    void on_actionCopy_2_triggered();
    void on_actionDelete_2_triggered();
    void on_actionPaste_triggered();
    void on_actionChange_Color_triggered();
    void on_actionClipboard_triggered();
    void on_actionPrint_triggered();
    void on_actionSvg_triggered();
    void on_actionPng_triggered();
    void on_actionPdf_triggered();
    void on_actionRussian_triggered();
    void on_actionUkrainian_triggered();
    void on_actionEnglish_triggered();
    void on_actionExit_triggered();
    void on_actionFont_triggered();
    void on_actionSheme_from_Text_triggered();
    void on_actionTranslate_triggered();
    void on_actionCut_2_triggered();
    void on_actionRevTranslate_triggered();
    void on_actionRemove_Defaults_triggered();
    void treeWidgetUpdate();
    void on_actionAdv_triggered();
    void on_actionSimplify_triggered();
    void on_actionOptions_triggered();
    void on_actionNope_triggered();
    void on_actionExport_as_image_to_triggered();

private:
    Ui::MainW *ui;
    QString wdir;
    QString bdir;
    QTreeWidgetItem * mwi;
    QTranslator translator;
    QTranslator translator2;
    rScene* scene();
    bool noOnTreeWidgetItemChanged;
    bool treeWidgetLoded;
    bool onActionSaveAS;
    rScene* intgScene;
    bool RemoveDirectory(QDir aDir);
    void treewidgetRen(QString path,QTreeWidgetItem* it);
    void treewidgetClean(QTreeWidgetItem* it);
    void tabwidgetShowDoc(QString path );
    rDocument* tabwidgetGetopened(QString path);
    QString projectOpen(QString path);
    void projectSave(QString path,QString as);
    void treewidgetSelectOne(QList<QString> paths,bool clean=true);
    QTreeWidgetItem* treewidgetItemByPath(QTreeWidgetItem* par,QString &path);
    void closeEvent(QCloseEvent *);
    void doPaint(QPainter &painter, rScene *scene, bool border=false);
    void doPrint(QPrinter &printer, int min = 0, int max = 0, bool border = false);
    QList<rDocument*> rdocs();
    QString textFromFile(QString nname);
    QTimer tcheckToolBar;
    bool toolbv;
    bool fresh;
public slots:
    void handleMessage(const QString& message);
signals:
    void needToShow();
    void retranslated();
};

#endif // MAINW_H
