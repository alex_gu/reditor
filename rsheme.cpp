#include "rsheme.h"
#include "rscene.h"
#include "rlogic.h"
#include "rnode.h"
#include "rctexedit.h"
#include "options.h"

rSheme::rSheme(rScene* cscene) :
    QGraphicsObject(0)
{
    layoutEnable = false;
    loreplace=0;
    scene=cscene;
    show_text=true;
    setFlag(QGraphicsItem::ItemIsSelectable);
    this->setHandlesChildEvents(false);
    //logics.clear();
    //nodes.clear();
    //qreal max_by=0;
    //foreach(rSheme* sh,scene->shemes()){
    //    max_by=qMax(max_by,sh->scenePos().y()+sh->childrenBoundingRect().bottom());
    //}
    //setPos(0,max_by);
    setCacheMode(CACHE_M);
    scene->addItem(this);
    scene->shemes.add(this);
}

rSheme::~rSheme(){
    scene->shemes.remove(this);
    foreach (rLogic* l, logics.values())
        delete l;
    foreach (rNode* n, nodes.values())
        delete n;
}
void rSheme::applyall_pos(){
    prepareGeometryChange();
    foreach(rLogic* lo,logics.values()){
        lo->setPos(lo->lx,lo->ly);
        QPen p=lo->pen();
        if(lo->markr){
            p.setColor(Qt::red);
        }else{
            p.setColor(lo->color);
        }
        lo->setPen(p);
        lo->setWid(lo->lw);
        lo->update();
    }
    foreach(rNode* no,nodes.values()){
        QPen p=no->pen();
        nKind k=kCircle;
        if(no->rl.count()){
            p.setColor(no->rl[0]->lncolor);
            k=(nKind)no->rl[0]->lnKind;
        }else if(no->ll.count()){
            p.setColor(no->ll[0]->rncolor);
            k=(nKind)no->ll[0]->rnKind;
        }
        if(no->isStart){
            if(no->rl.count()>1){
                no->ly=no->rl[1]->ly;
            }
        }
        if(no->isEnd){
            if(no->ll.count()>1){
                no->ly=no->ll[1]->ly;
            }
        }
        no->setPen(p);
        no->setPos(no->lx,no->ly);
        no->setVert();
        no->setKind(k,false);
    }
    if(logics.count()==1){
        startNode()->setColor(Qt::lightGray);
        endNode()->setColor(Qt::lightGray);
        startNode()->setPos(startNode()->pos()+QPointF(0,10));
        endNode()->setPos(endNode()->pos()+QPointF(0,10));
    }
}
int rSheme::fixwpos(rNode* p,qreal px,QList<rLogic*>* newLogics,bool init){
    if(init){
        foreach(rLogic* lo,logics.values()){
            lo->visited=false;
        }
        foreach(rNode* no,nodes.values()){
            no->visited=false;
            no->ll.clear();
        }
    }
    if(p==NULL){
        return 0;
    }
    if(p->visited){
        return 0;
    }
    p->lx=px;
    p->visited=true;
    for(int k=0;k<p->rl.count();k++){
        if(p->rl[k]->visited==false){
            p->rl[k]->visited=true;
            int rigthnodenum=p->rl[k]->to;
            int destx=nodes.item(rigthnodenum)->lx;
            int drawx=px+p->rl[k]->lw;
            if((nodes.item(rigthnodenum)->visited)&&(destx!=drawx)){
                if(destx<drawx) {
                    nodes.item(rigthnodenum)->ll[0]->lw+=(drawx-destx);
                    return 1;
                }else if (destx>drawx){
                    p->rl[k]->lw+=destx-drawx;
                }
            }
            p->rl[k]->lx=px;
            nodes.item(p->rl[k]->to)->ll.append(p->rl[k]);
            p->rl[k]->top=nodes.item(p->rl[k]->to)->ll.count()-1;
            if(newLogics){
                newLogics->append(p->rl[k]);
            }
            if((p->rl[k]->lgen)||(newLogics==0)){
                if(fixwpos(nodes.item(p->rl[k]->to),px+p->rl[k]->lw,newLogics,false)){
                    return 1;
                }
            }
        }
    }
    return 0;
}
int rSheme::buildRs(QList<rLogic*>& logics,rNode* p,qreal px,qreal py,rLogicMap::TKey fromlog,bool init){
    if(init){
        foreach(rLogic* lo,logics){
            lo->visited=false;
        }
        foreach(rNode* no,nodes.values()){
            no->visited=false;
            no->ll.clear();
        }
    }
    if (p->visited){
        return 0;
    }
    p->ly=py;
    p->lx=px;
    p->visited=true;
    for(int k=0;k<p->rl.count();k++){
        if (p->rl[k]->visited==false) {
            p->rl[k]->visited=true;
            if (((k==0)&&(fromlog==-1))||(k!=0)){
                fromlog=logics.indexOf(p->rl[k]);
            }
            p->rl[k]->lmainl=fromlog;
            p->rl[k]->ly=py;
            p->rl[k]->lx=px;
            nodes.item(p->rl[k]->to)->ll.append(p->rl[k]);
            p->rl[k]->top=nodes.item(p->rl[k]->to)->ll.count()-1;
            if (p->rl[k]->lgen){
                buildRs(logics,nodes.item(p->rl[k]->to),px+p->rl[k]->lw,py,fromlog,false);
            }
            py+=p->rl[k]->lih;
            if ((k<p->rl.count()-1)){
                py+=p->rl[k+1]->lbh;
            }
        }
    }
    return 0;
}

bool rSheme::perekrx(qreal l1,qreal r1,qreal l2,qreal r2){
    if(r1<=l2){
        return false;
    }
    if(r2<=l1){
        return false;
    }
    return true;
}

void rSheme::llayout(bool silent){
    if(logics.count()==0 || !layoutEnable)
        return;
    //clean
    QList<rNode*> rem=nodes.values();
    foreach(rLogic* lo,logics.values()){
        rem.removeOne(nodes.item(lo->to));
        rem.removeOne(nodes.item(lo->fr));
        if(!nodes.item(lo->to))
            new rNode(this,lo->to);
        if(!nodes.item(lo->fr))
            new rNode(this,lo->fr);
    }
    foreach (rNode* n, rem)
        delete n;
    QList<rNode*> nodeList = nodes.values();
    QList<rLogic*> logicList = logics.values();
    //find start/end
    foreach (rNode* n,nodeList){
        n->visited=false;
        n->isStart=true;
        n->isEnd=true;
    }
    foreach(rLogic* l,logicList){
        nodes.item(l->to)->isStart=false;
        nodes.item(l->fr)->isEnd=false;
    }
    foreach (rNode* n,nodeList){
        if(n->isStart){
            m_startn=n;
        }
        if(n->isEnd){
            m_endn=n;
        }
    }
    //fill rl
    foreach(rNode* no,nodeList){
        no->rl.clear();
    }
    foreach(rLogic* lo,logicList){
        lo->lw=lo->liw;
        lo->lbh=lo->libh;
        QVector<rLogic*>& rl = nodes.item(lo->fr)->rl;
        rl.resize(qMax(rl.size(),lo->frp+1));
        nodes.item(lo->fr)->rl[lo->frp]=lo;
    }
    foreach(rNode* n,nodeList){
        qint32 misscount=0;
        foreach(rLogic* l,n->rl){
            if(l==NULL){
                misscount++;
            }else{
                l->frp-=misscount;
            }
        }
    }
    foreach(rNode* no,nodeList){
        no->rl.clear();
    }
    foreach(rLogic* lo,logicList){
        lo->lw=lo->liw;
        lo->lbh=lo->libh;
        QVector<rLogic*>& rl = nodes.item(lo->fr)->rl;
        rl.resize(qMax(rl.size(),lo->frp+1));
        nodes.item(lo->fr)->rl[lo->frp]=lo;
    }
    //x align
    do{
    }while (fixwpos(startNode(),10,NULL));
    qreal starty=startNode()->rl[0]->libh;
    qreal startx=5;
    //first logics line
    firstLogic = NULL;
    if(startNode())
        firstLogic = startNode()->rl[0];
    rLogic* sea=firstLogic;
    while (nodes.item(sea->to)->rl.count()){
        sea=nodes.item(sea->to)->rl[0];
        starty=qMax(starty,sea->libh);
    }
    //get ordered
    logicList.clear();
    fixwpos(startNode(),startx,&logicList);
    buildRs(logicList,startNode(),startx,starty,-1);
    for (int m = 0; m < logicList.count(); m++){
        qint32 searcma=m;
        searcma=logicList[m]->lmainl;
        qreal maxy=0;
        for (qint32 l=0;l<m;l++) {
            if (perekrx(logicList[m]->lx,logicList[m]->lx+logicList[m]->lw,
                        logicList[l]->lx,logicList[l]->lx+logicList[l]->lw)) {
                maxy=qMax(maxy,logicList[l]->ly+logicList[l]->lih);
            }
        }
        logicList[searcma]->lbh=qMax(logicList[searcma]->lbh,
            maxy-logicList[searcma]->ly+logicList[searcma]->lbh+logicList[m]->libh);
        buildRs(logicList,startNode(),startx,starty,-1);
    }
    //bug fix
    qreal max_rx=0;
    bool bugP=false;
    int icycle=0;
    do{
        bugP=false;
        foreach(rNode* p,nodeList){
            qreal px=p->lx;
            max_rx=0;
            foreach(rLogic* lo,p->ll){
                max_rx=qMax(max_rx,lo->lx+lo->liw);
            }
            foreach(rLogic* lo,p->ll){
                lo->lw=max_rx-lo->lx;
            }
            if((p->ll.count())&&(max_rx<px)){
                bugP=p;
            }
        }
        if(bugP){
            buildRs(logicList,startNode(),startx,starty,-1);
        }
    }while ((bugP)&&(++icycle<std::numeric_limits<int>::max()));
    foreach(rLogic* lo,logicList){
        if(lo->ly>=nodes.item(lo->to)->ly) lo->lgen=true;
    }
    if(!silent){
        applyall_pos();
        updateKeyboardGrid();
        scene->shemeRedrawed(this);
    }

}
QRectF rSheme::boundingRect() const
{
    return childrenBoundingRect();
}

QVariant rSheme::itemChange(GraphicsItemChange change, const QVariant &value)
{
    return QGraphicsObject::itemChange(change, value);
}


void rSheme::putToStream(QTextStream &out, QList<rLogic*>* logicsList){
    out << 0<<'\n';
    out << ""<<'\n';
    out << pos().x()<<' '<<pos().y()<<'\n';
    //
    out << (logicsList?logicsList->count():logics.count())<<'\n';
    foreach( rLogic* l,logicsList?*logicsList:logics.values()){
        out << l->stringKey() <<"\n";
        l->putToStream(out);
    }
}
bool rSheme::getFromStream(QTextStream &out, bool readId){
    char c;
    qint32 len=0;
    out >> len>>c;
    QString s=out.read(len);//unused name
    qreal x,y;
    out >> x>> y>>c;
    setPos(x,y);
    qint32 lolen=0;
    out>>lolen;
    //logics load
    if(scene->fileVersion==0)
        logics.clear();
    QList<rLogic*> lorem = logics.values();
    for(int l=0;l<lolen;++l){
        rLogicMap::TKey lokey=logics.freeKey();
        if(scene->fileVersion>=1)
            out>>lokey>>c;
        rLogic* lo=logics.item(lokey);
        lorem.removeOne(lo);
        lo=lo?lo:new rLogic(this);
        lo->getFromStream(out,readId);
    }
    foreach (rLogic* l, lorem)
        delete l;
    return lolen>0;
}

rNode *rSheme::startNode()
{
    return m_startn;
}

rNode *rSheme::endNode()
{
    return m_endn;
}

int rSheme::startKey()
{
    return nodes.key(m_startn);
}

int rSheme::endKey()
{
    return nodes.key(m_endn);
}

bool rSheme::LeftToRightConnected(rNode* l,rNode* r,bool init){
    if (init){
        foreach(rLogic* lo,logics.values()){
            lo->visited=false;
        }
    }
    foreach (rLogic* lo,l->rl){
        if(lo->visited){
            break;
        }
        lo->visited=true;
        if(nodes.item(lo->to)==r){
            return true;
        } else if (LeftToRightConnected(nodes.item(lo->to),r)) {
            return true;
        }
    }
    return false;
}

void rSheme::changed(bool trans, bool byedit){
    scene->changed(trans,byedit);
}

rLogic *rSheme::getFirstLogic()
{
    return firstLogic;
}

void rSheme::mousePressEvent(QGraphicsSceneMouseEvent *event){
    event->ignore();
}

void rSheme::setFont(const QFont &font){
    QFont font1=font;
    bool oldbold=font1.bold();
    foreach(rLogic* l,logics.values()){
        if(l->kindS()==kSheme){
            font1.setBold(1);
        }
        l->UpText->setFont(font1);
        font1.setBold(oldbold);
        l->DownText->setFont(font1);
    }
}

rTextEdit* rSheme::header(){
    if((logics.count())&&firstLogic&&(firstLogic->kindS()==kSheme)){
        return firstLogic->UpText;
    }
    return NULL;
}
rTextEdit* rSheme::body(){
    if((logics.count())&&(firstLogic->kindS()==kSheme)){
        return firstLogic->DownText;
    }
    return NULL;
}

void rSheme::prepareConnection(rSheme* master){
    int delta=0;//todo search for free key
    QList<int> curkeys=master->nodes.keys();
    foreach(int k,curkeys){
        delta=qMax(delta,k+1);
    }
    foreach(rLogic* nl,logics.values()){
        nl->fr+=delta;
        nl->to+=delta;
    }
    /*
      QList<int> curkeys=master->nodes.keys();
    QHash<int,int> freekeys;
    foreach(int k,curkeys){//todo opt
        for(int i=1; i<std::numeric_limits<rNodeMap::TKey>::max();++i){
            if(!master->nodes.item(i)&&!freekeys.contains(i)){
                freekeys.insert(k,i);
                break;
            }
        }

    }
    foreach(rLogic* nl,logics.values()){
        nl->fr=freekeys[nl->fr];
        nl->to=freekeys[nl->to];
    }
}
     */
}

void rSheme::updateKeyboardGrid(){
    mKeyboardGridNode.clear();
    mKeyboardGridLogic.clear();
    mKeyboardGridVertical.clear();
    bool bnodes =Options::instance()->keditNodes;
    bool blogics=Options::instance()->keditLogics;
    bool bverts =Options::instance()->keditVerticals;
    foreach (rNode* no,nodes.values()){
       if(bnodes)
           mKeyboardGridNode.append(no->scenePos());
       if(bverts){
          // bool next=true;
          // int idx=
           rLogic* pl = 0;
           foreach(rLogic* lo, no->ll){
               if(pl)
                   mKeyboardGridVertical.append(QPointF(no->scenePos().x()-rNode::radius(),(pl->scenePos().y()+lo->scenePos().y())/2));
               else
                   if(lo->kindS()!=kSheme)
                       mKeyboardGridVertical.append(QPointF(no->scenePos().x()-rNode::radius(),(lo->scenePos().y()-rNode::radius())));
               pl = lo;
           }
           pl =0;
           foreach(rLogic* lo, no->rl){
               if(pl)
                   mKeyboardGridVertical.append(QPointF(no->scenePos().x()+rNode::radius(),(pl->scenePos().y()+lo->scenePos().y())/2));
               else
                   if(lo->kindS()!=kSheme)
                       mKeyboardGridVertical.append(QPointF(no->scenePos().x()+rNode::radius(),(lo->scenePos().y()-rNode::radius())));
               pl = lo;
           }
       }
    }
    if(blogics)
        foreach (rLogic* lo,logics.values()){
           if(lo->kindS()!=kSheme){
               mKeyboardGridLogic.append(lo->getSelPoint('l'));
               mKeyboardGridLogic.append(lo->getSelPoint('r'));
           }
        }
}

QString rSheme::stringKey(){
    if(scene)
        return QString::number(scene->shemes.key(this));
     return "0";
}
