#ifndef RITEXTEDIT_H
#define RITEXTEDIT_H

#include <QGraphicsRectItem>
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/documentmanager.h>
#include <coreplugin/vcsmanager.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/ieditor.h>
#include <texteditor/autocompleter.h>
#include <texteditor/texteditorplugin.h>
#include <texteditor/basetexteditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/icore.h>
#include <coreplugin/minisplitter.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/openeditorsview.h>
#include <coreplugin/editormanager/openeditorswindow.h>
#include <texteditor/plaintexteditor.h>
#include <aggregation/aggregate.h>

class rSheme;
class rLogic;
class rScene;
class rIDocument;
class rTextEdit : public QGraphicsObject
{
    Q_OBJECT
public:
    QString id;
    int m_tb_beg;
    int m_tb_end;
    int m_tb_beg_pos;
    int m_tb_end_pos;
    int oldw;
    int oldh;
    QString rtext;
    rLogic* logic;
    int loffset;
    QTimer updatetimer;
    QString m_text;
    explicit rTextEdit(QString tx,rScene *scene=0,QGraphicsItem *parent=0,rLogic* log=0);
    ~rTextEdit();
    void textEdited();
    void setId(QString nid);
    inline QString text(){ return m_text;}
    void setText(QString s, bool any=false);
    inline bool isEmpty(){return m_text.length()==0;} 
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;
    void updateLater(bool sim=true);
    inline QRect getEdRect(){return QRect(_x,_y,_w,_h);}
    bool setEdRect(int flx, int fly, int flw, int flh, int x, int y, int w, int h, int dw);
    QString selectedText();
    void setFont(const QFont &font);
    QFont font();
    void setDefaultTextColor(QColor);
    void setVisible(bool visible);
    void my_clearFocus();
    void my_setFocus(Qt::FocusReason focusReason = Qt::OtherFocusReason, bool man=true);
    int curPos();
    bool setCurPos(int p,bool act=true);
private:
    bool m_focus;
    rIDocument* m_view;
    rScene* m_scene;
    QPointer<QPlainTextEdit> m_par_ed;
    QPointer<QTextDocument> m_doc;
    QPixmap* buf;
    bool upbufer;
    QString lasttext;
    int _flx;
    int _fly;
    int _flw;
    int _flh;
    int _x;
    int _y;
    int _w;
    int _h;
    int _dw;
private slots:
    void updatetimer_out();
    void redrawbuf();
    void resizebuf(int nw,int nh);
};

#endif // RITEXTEDIT_H
