#include "rscene.h"
#include "rnode.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "rtransparentgraphicsellipseitem.h"
#include "options.h"
#include "rdocument.h"
#include <QScrollBar>
#include <qfileinfo.h>
#include "redSelection.h"

rScene::rScene(QObject *parent) :
     QGraphicsScene(parent)
    ,sel(new redSelection(this))
    ,mouseDown(false)
{
    //todo move to init. list
    fileVersion=1;
    translateEn=0;
    extEn=0;
    lvissible=0;
    setFont(QFont("Arial",8));
    bufferStream.setString(&bufferString);
    moveSheme=NULL;
    path="";
    timeoutEditTimer=new QTimer(this);
    timeoutEditTimer->setSingleShot(true);
    connect(timeoutEditTimer,SIGNAL(timeout()),this,SLOT(privateSlChanged()));
    fRecordUndo=true;
    unRePos=-1;
    tFRecordUndoTrue.setSingleShot(true);
    tFRecordUndoTrue.setInterval(1000);
    connect(&tFRecordUndoTrue,SIGNAL(timeout()),this,SLOT(fRecordUndoTrue()));
    tAutoSave.setSingleShot(true);
    tAutoSave.setInterval(1000);
    connect(&tAutoSave,SIGNAL(timeout()),this,SLOT(sAutoSave()));
    tMouseReleaseEvent.setSingleShot(true);
    tMouseReleaseEventWait.setSingleShot(true);
    connect(&tMouseReleaseEvent,SIGNAL(timeout()),this,SLOT(slMouseReleaseEvent()));
    connect(Options::instance(),SIGNAL(changed()),this,SLOT(readSettings()));
    readSettings();
    tCenter.setSingleShot(true);
    tCenter.setInterval(100);
    connect(&tCenter,SIGNAL(timeout()),this,SLOT(sAutoScroll()));
    scrollTimer.setInterval(25);
    connect(&scrollTimer,SIGNAL(timeout()),this,SLOT(slScroll()));
    shiftAccept=false;
}

rScene::~rScene()
{
    foreach (rSheme* sh,shemes.values())
        delete sh;
}

QPointF rScene::posToGrid(QPointF p){
    return QPointF(qint64(((p).x()))/10*10+5,
                               qint64(((p).y()))/10*10+5);
}

void rScene::wheelEvent(QGraphicsSceneWheelEvent *wheelEvent)
{
    if (wheelEvent->modifiers()){
        qreal factor = wheelEvent->delta() > 0 ? 1.1 : (1/1.1);
        foreach (QGraphicsView * view, views()) {
        }
    }
    QGraphicsScene::wheelEvent(wheelEvent);
}

void rScene::mouseMoveEvent ( QGraphicsSceneMouseEvent * event ){
    QPointF ps=mouseMovePos=event->scenePos();
    if (event->modifiers()==Qt::NoModifier){
        if(moveSheme){
            QPointF p = moveShemeinitp + ps - lastDownPos;
            moveSheme->setPos(posToGrid(p));
            shemeRedrawed(moveSheme);
        }else if(mouseDown&&sel->getNode1()&&sel->getPoint1MousePos()==lastDownPos)//todo out of
            sel->placePoint2(ps);
    }
    QGraphicsScene::mouseMoveEvent(event);
}

void rScene::mousePressEvent ( QGraphicsSceneMouseEvent * event )  {
    QPointF ps=event->scenePos();
    mouseDown=true;
    lastDownPos = ps;
    if (event->modifiers()==Qt::NoModifier){
        rLogic* lo = dynamic_cast<rLogic*>(ritemAt(ps,true));
        if (event->button() == Qt::LeftButton){
            if(lo && lo->kindS()==kSheme){
                moveSheme = lo->sheme;
                moveShemeinitp = moveSheme->pos();
            }else{
                sel->placePoint1(ps);
            }
        }else if (event->button() == Qt::RightButton){
            if (lo){
                event->setButton(Qt::LeftButton);//select item
            }
        }
    }
    scrollTimer.start();
    QGraphicsScene::mousePressEvent(event);
}


void rScene::mouseReleaseEvent ( QGraphicsSceneMouseEvent * event ) {
    QPointF ps=event->scenePos();
    mouseDown=false;
    butMouseReleaseEvent = event->button();
    posMouseReleaseEvent = ps;
    if (event->modifiers()==Qt::NoModifier){
        rLogic* lo = dynamic_cast<rLogic*>(ritemAt(ps,true));
        rNode*  no = dynamic_cast<rNode* >(ritemAt(ps,true));
        if(moveSheme){
            moveSheme->changed();
            moveSheme = NULL;
        }else{
            if(!curEd.length()&&(no||lo)&&!tMouseReleaseEventWait.isActive()&&
               ((sel->selected()&&(!lo||lo->kindS()!=kSheme))||event->button() == Qt::RightButton)&&(sel->getNode2() || (lastDownPos-ps).manhattanLength()<=rNode::radius()*2)){
                tMouseReleaseEvent.start();
            }
            else
            {
                tMouseReleaseEvent.stop();
            }
        tMouseReleaseEventWait.start();
        }
    }
    scrollTimer.stop();
    QGraphicsScene::mouseReleaseEvent(event);
}

void rScene::slMouseReleaseEvent(bool forceEmptySel){
    QPointF ps=posMouseReleaseEvent;
    rLogic* lo = dynamic_cast<rLogic*>(ritemAt(ps, true));
    rNode*  no = dynamic_cast<rNode* >(ritemAt(ps, true));
    if (butMouseReleaseEvent == Qt::LeftButton){
        if(forceEmptySel||lo||no)
            elemPaste(ps,forceEmptySel);
    }
    else if (butMouseReleaseEvent == Qt::RightButton){
        if (lo){
            elemDelete();
        }
    }
}

void rScene::mouseDoubleClickEvent (QGraphicsSceneMouseEvent *event)
{
    QPointF ps=event->scenePos();
    QGraphicsItem* it = ritemAt(ps, true);
    if (!it)
        slMouseReleaseEvent(true);
    tMouseReleaseEvent.stop();
    QGraphicsScene::mouseDoubleClickEvent(event);
}

bool rScene::elemCopy(QTextStream & out){
    fRecordUndo=false;
    bool one_sheme_ex=false;
    QList<rLogic*> sellogs=selectedLogics();
    QHash<rSheme*,int> shemes;
    foreach (rLogic* lo, sellogs)
        shemes.insert(lo->sheme,1);
    //
    out<<shemes.keys().count()<<'\n';
    foreach(rSheme* sh,shemes.keys()){
        bool sel_is_sheme=true;
        QHash<int,int> nto;
        QHash<int,int> nfr;
        foreach(rLogic* lo,sellogs){
            if(sh->logics.key(lo)){
                int fr=lo->fr;
                int to=lo->to;
                if(nto.contains(to)){
                    nto.insert(to,nto.value(to)+1);
                }else{
                    nto.insert(to,1);
                }
                if(nfr.contains(fr)){
                    nfr.insert(fr,nfr.value(fr)+1);
                }else{
                    nfr.insert(fr,1);
                }
            }
        }
        int begins_c=0;
        int ends_c=0;
        foreach(int k,nto.keys()){
            if ((!nfr.contains(k))){ends_c++;}
        }
        foreach(int k,nfr.keys()){
            if ((!nto.contains(k))){begins_c++;}
        }
        if ((begins_c!=1)||(ends_c!=1)){
            sel_is_sheme=false;
        }
        if (sel_is_sheme){
            QList<rLogic*> logs;
            foreach( rLogic* l,sh->logics.values()){
                if (sellogs.contains(l)){logs.append(l);}
            }
            sh->putToStream(out,&logs);
        }
        if(sel_is_sheme){
            one_sheme_ex=true;
        }
    }
    fRecordUndo=true;
    return one_sheme_ex;
}

QGraphicsItem *rScene::ritemAt(QPointF ps, bool edit) const{
    QGraphicsItem* ret=0;
    qreal len = 0;
    foreach(QGraphicsItem* i,items(ps)){
        if((dynamic_cast<rLogic*>(i))||(dynamic_cast<rNode*>(i))||((dynamic_cast<rTextEdit*>(i)))){
            if((!edit)&&(dynamic_cast<rTextEdit*>(i))){
                return 0;
            }
            qreal l = (i->scenePos() - ps).manhattanLength();
            if(ret==0||l<=len){
                len = l;
                ret = i;
            }
        }
    }
    return ret;
}

void rScene::fixShemeInSheme(rSheme* nsh){
    foreach(rLogic* lo,nsh->logics.values()){
        if(lo!=nsh->firstLogic){
            if(lo->kindS()==kSheme){
                lo->setKind(kSpecial);
            }
        }
    }
}

void rScene::setEns(bool tra, bool ext)
{
    translateEn=tra;
    extEn=ext;
}

void rScene::setAdv(bool v)
{
}

rSheme* rScene::elemPaste(QTextStream & out, QPointF *poi, bool forceEmptySel, bool toBuff, bool ch_act){
    //lgen is forced
    if(toBuff){
        bufferString=out.readAll();
        return NULL;
    }
    setEns(false,false);
    bool scCh=false;
    rSheme* retSheme=NULL;
    rLogic* ensvis=0;
    fRecordUndo=false;
    bool first=true;
    qint32 shlen=0;
    out>>shlen;
    QList<rSheme*>nshs;
    for(qint32 i=0;i<shlen;i++){
        rSheme* nsh=new rSheme(this);
        if(nsh->getFromStream(out,false)){
            nsh->layoutEnable = true;
            nsh->llayout(true);
            nshs.append(nsh);
        }else{
            delete nsh;
        }
    }
    //
    if(!sel->selected()||forceEmptySel){
        foreach(rSheme*nsh,nshs){
            bool exist=false;
            foreach(rLogic* lo,nsh->endNode()->ll){
                if(lo->lgen)exist=true;
            }
            if(!exist){
                nsh->endNode()->ll[0]->lgen=true;
            }
            bool need_repos=true;
            if(first){
               need_repos=false;
               first=false;
            }
            if (need_repos||!poi){
                qreal max_by=0;
                foreach(rSheme* sh,shemes.values()){
                    if(nsh!=sh){
                        max_by=qMax(max_by,sh->scenePos().y()+sh->childrenBoundingRect().bottom());
                    }
                }
                nsh->setPos(posToGrid(QPointF(0,max_by)));
            }else{
                nsh->setPos(posToGrid(*poi));
            }
            if(!nsh->header()){//insert header
                nsh->layoutEnable=false;
                foreach(rLogic* l,nsh->startNode()->rl){
                    l->frp+=1;
                }
                rLogic* nl=new rLogic(nsh,NULL,NULL);
                nl->setKind(kSheme);
                nl->UpText->setText(tr("R-scheme"));
                nl->DownText->setText("");
                nl->fr=nsh->startKey();
                nl->frp=0;
                nl->to=nsh->endKey();
                nsh->layoutEnable=true;
            }
            scCh=true;
            retSheme=nsh;
            nshs.removeOne(nsh);
            nsh->llayout();
            fixShemeInSheme(nsh);
            ensvis=nsh->firstLogic;
        }
    }else if ((sel->getNode1())&&(sel->getNode2())&&(sel->getNode1()!=sel->getNode2())){
        bool swap=sel->reversed();
        if(swap)
            sel->swap();
        rSheme* nsh=(nshs.size())?(nshs.at(0)):(NULL);
        rSheme* cursh=shemes.item(sel->getSheme());
        rNode* insStart=cursh->nodes.item(sel->getNode1());
        rNode* insEnd=cursh->nodes.item(sel->getNode2());
        if (nsh && insStart && insEnd && !cursh->LeftToRightConnected(insEnd,insStart,true)){
            int insStartIdx=(sel->point1OnNode())?insStart->rl.count():sel->getNode1IdxL();
            int insEndIdx  =(sel->point2OnNode())?insEnd->ll.count():sel->getNode2IdxL();
            foreach(rLogic* lo,nsh->endNode()->ll)
                lo->lgen=true;
            nsh->prepareConnection(cursh);
            nsh->llayout(true);
            cursh->layoutEnable = false;
            //
            for(int i=insStartIdx;i<insStart->rl.count();++i)
                insStart->rl[i]->frp+=nsh->startNode()->rl.count();
            foreach (rLogic* lo, nsh->startNode()->rl){
                lo->fr=sel->getNode1();
                lo->frp+=insStartIdx;
            }
            for(int i=insEndIdx;i<insEnd->ll.count();++i)
                insEnd->ll[i]->top+=nsh->endNode()->ll.count();
            foreach (rLogic* lo, nsh->endNode()->ll){
                lo->to=sel->getNode2();
                lo->top+=insEndIdx;
            }
            sel->shiftNode1Idx(nsh->startNode()->rl.count());
            sel->shiftNode2Idx(nsh->endNode()->ll.count());
            //
            foreach(rLogic* lo,nsh->logics.values()){
                QString stmp;
                QTextStream tmp(&stmp);
                lo->putToStream(tmp);
                rLogic* nl=new rLogic(cursh);
                tmp.seek(0);
                nl->getFromStream(tmp,false);
                ensvis=nl;
            }
            cursh->layoutEnable = true;
            cursh->llayout();
            fixShemeInSheme(cursh);
            scCh=true;
        }
        if(swap)
            sel->swap();
    }else if(sel->getNode1()){
        rSheme* nsh=(nshs.size())?(nshs.at(0)):(NULL);
        rSheme* cursh=shemes.item(sel->getSheme());
        rNode* insStart=cursh->nodes.item(sel->getNode1());
        if (nsh && insStart){
            int insStartIdx=sel->getNode1IdxL();
            char insSide = sel->getNode1Side();
            //fix invalid locs
            if(insStart->isStart&&insSide!='r')
            {
                insSide='r';
                insStartIdx=1;//skip sheme logic
            }
            else
            if(insStart->isEnd&&insSide!='l')
            {
                insSide='l';
                insStartIdx=1;
            }
            if(insStartIdx<=0)
                insSide=' ';
            if(insSide=='r'&&insStartIdx>=insStart->rl.count()){
               if(!insStart->isStart)
                   insSide=' ';
               else
                   insStartIdx=1;
            }
            if(insSide=='l'&&insStartIdx>=insStart->ll.count()){
                if(!insStart->isEnd)
                    insSide=' ';
                else
                    insStartIdx=1;
             }
            foreach(rLogic* lo,nsh->endNode()->ll)
                lo->lgen=true;
            nsh->prepareConnection(cursh);
            nsh->llayout(true);
            cursh->layoutEnable = false;
            //
            if(insSide=='r'){
                for(int i=insStartIdx;i<insStart->rl.count();++i)
                    insStart->rl[i]->fr=nsh->endKey();
                foreach(rLogic* lo,nsh->startNode()->rl){
                    lo->fr=sel->getNode1();
                    lo->frp+=insStartIdx;
                }
                //sel->setNode1(nsh->endKey());//end node?
            }else
            if(insSide=='l'){
                for(int i=insStartIdx;i<insStart->ll.count();++i)
                    insStart->ll[i]->to=nsh->startKey();
                foreach(rLogic* lo,nsh->endNode()->ll){
                    lo->to=sel->getNode1();
                    lo->top+=insStartIdx;
                }
                //sel->setNode1(insStart->isEnd?cursh->endKey():nsh->startKey());//end node?
            }
            if(insSide==' '){
                foreach (rLogic* lo, insStart->ll)
                    lo->to=nsh->startKey();
                foreach (rLogic* lo, insStart->rl)
                    lo->fr=nsh->endKey();
                sel->setNode1(((nsh->logics.count()>1||nsh->startNode()->rl[0]->dir()=='r')?nsh->endKey():nsh->startKey()));
            }
            //
            foreach(rLogic* lo,nsh->logics.values()){
                QString stmp;
                QTextStream tmp(&stmp);
                lo->putToStream(tmp);
                rLogic* nl=new rLogic(cursh);
                tmp.seek(0);
                nl->getFromStream(tmp,false);
                ensvis=nl;
            }
            cursh->layoutEnable = true;
            cursh->llayout();
            fixShemeInSheme(cursh);
            scCh=true;
        }
    }else if(sel->getLogic()){//todo restore sele
        rSheme*nsh=(nshs.size())?(nshs.at(0)):(NULL);
        rSheme* cursh=shemes.item(sel->getSheme());
        rLogic* ins=cursh->logics.item(sel->getLogic());
        if (nsh && ins){
            foreach(rLogic* lo,nsh->endNode()->ll)
                lo->lgen=true;
            nsh->prepareConnection(cursh);
            nsh->llayout(true);
            cursh->layoutEnable = false;
            rLogic* inslo=ins;
            if(sel->getLogicSide()=='r'){
                foreach (rLogic* lo, nsh->endNode()->ll) {
                    lo->to=inslo->to;
                    lo->lgen=inslo->lgen;
                }
                inslo->to=nsh->startKey();
                inslo->lgen=true;
            }else{
                for(qint32 i=inslo->frp+1;i<cursh->nodes.item(inslo->fr)->rl.count();i++){
                    cursh->nodes.item(inslo->fr)->rl[i]->frp+=nsh->startNode()->rl.count()-1;
                }
                for(qint32 i=0;i<nsh->startNode()->rl.count();i++){
                    nsh->startNode()->rl[i]->fr=inslo->fr;
                    nsh->startNode()->rl[i]->frp=inslo->frp+i;
                }
                inslo->frp=0;
                inslo->fr=nsh->endKey();
            }
            foreach(rLogic* lo,nsh->logics.values()){
                QString stmp;
                QTextStream tmp(&stmp);
                lo->putToStream(tmp);
                rLogic* nl=new rLogic(cursh);
                tmp.seek(0);
                nl->getFromStream(tmp,false);
                ensvis=nl;
            }
            cursh->layoutEnable = true;
            cursh->llayout();
            fixShemeInSheme(cursh);
            scCh=true;
        }
    }
    //
    foreach (rSheme* sh, nshs) {
        delete sh;
    }
    fRecordUndo=true;
    setEns(true,true);
    if(ensvis){
        lvissible=ensvis;
        svissible();
    }
    if(scCh&&ch_act){
        changed();
    }
    return retSheme;
}

QList<rSheme*> rScene::sortedShemes(){//todo cache
    QList<rSheme*> ret=shemes.values();
    QList<rSheme*> ret1;
    while(ret.count()){
        rSheme* min=ret.first();
        foreach(rSheme* sh,ret){
            if((sh->scenePos().y()<min->scenePos().y())||
                    ((sh->scenePos().y()==min->scenePos().y())&&((sh->scenePos().x()<min->scenePos().x())))){
                min=sh;
            }
        }
        ret1.append(min);
        ret.removeOne(min);
    }
    ret=ret1;
    return ret;
}

void rScene::removeLog(rSheme* sheme,rLogic* l,bool silent){
    if(sheme->logics.count()>1) {
        bool exist=false;
        if (!((sheme->nodes.item(l->fr)->rl.count()>1)&&(sheme->nodes.item(l->to)->ll.count()>1))){// !other way
            if(sheme->nodes.item(l->to)->ll.count()==1){
                for (int i=l->frp+1;i<sheme->nodes.item(l->fr)->rl.count();i++){
                    sheme->nodes.item(l->fr)->rl[i]->frp+=sheme->nodes.item(l->to)->rl.count()-1;
                }
                for (int i=0;i<sheme->nodes.item(l->to)->rl.count();i++){
                    sheme->nodes.item(l->to)->rl[i]->fr=l->fr;
                    sheme->nodes.item(l->to)->rl[i]->frp=i+l->frp;
                }
            }else{
                foreach(rLogic* lo,sheme->nodes.item(l->fr)->ll){
                    lo->to=l->to;
                    if(l->lgen==false){
                        lo->lgen=false;
                    }
                    if(lo->lgen){
                        exist=true;
                    }
                }
            }
        }
        qint32 toi=l->to;
        foreach(rLogic* lo,sheme->nodes.item(toi)->ll){
            if((lo!=l)&&(lo->lgen)){
                exist=true;
            }
        }
        if(!exist){
            int newgen=l->top-1;
            if (newgen<0){
                newgen=0;
            }
            for(int i=newgen;i<sheme->nodes.item(toi)->ll.count();i++){
                sheme->nodes.item(toi)->ll[i]->lgen=true;
            }
        }
    }
    //fix sel
    rNode* node = NULL;
    if(node=sheme->nodes.item(sel->getNode1())){
        const QVector<rLogic*> &logs = sel->getNode1Side()=='r'?node->rl:node->ll;
        if(logs.contains(l)&&logs.count()!=1&&logs.indexOf(l)<sel->getNode1IdxL())
            sel->shiftNode1Idx(-1);
    }
    if(node=sheme->nodes.item(sel->getNode2())){
        const QVector<rLogic*> &logs = sel->getNode2Side()=='r'?node->rl:node->ll;
        if(logs.contains(l)&&logs.count()!=1&&logs.indexOf(l)<sel->getNode2IdxL())
            sel->shiftNode2Idx(-1);
    }
    if(sel->getLogic()==sheme->logics.key(l))
       sel->clear();
    int to=l->to;
    int fr=l->fr;
    //
    delete l;
    if(sheme->logics.count())
        sheme->llayout(silent);
    //fix sel
    if(sel->getNode1()==to&&sheme->nodes.item(to)==rNodeMap::notFoundValue())
        sel->setNode1(fr);
    if(sel->getNode1()==fr&&sheme->nodes.item(fr)==rNodeMap::notFoundValue())
        sel->setNode1(to);
    if(sel->getNode2()==to&&sheme->nodes.item(to)==rNodeMap::notFoundValue())
        sel->setNode2(fr);
    if(sel->getNode2()==fr&&sheme->nodes.item(fr)==rNodeMap::notFoundValue())
        sel->setNode2(to);
    if(sel->getNode1()==sel->getNode2())
        sel->clearPoint2();
    if(sheme->nodes.item(sel->getNode1())==rNodeMap::notFoundValue())
        sel->clear();
}

void rScene::elemDelete(QList<rLogic*> *m_sellogs, bool ch_act){
    fRecordUndo=false;
    setEns(false,false);
    bool scCh=false;
    QList<rLogic*> sellogs=selectedLogics(m_sellogs);
    foreach(rSheme* sh,shemes.values()){
        if(shemes.key(sh)==rShemeMap::notFoundKey())
            continue;
        foreach(rLogic* lo,sellogs)
            if(lo->sheme == sh)
            {
                if(lo==lvissible)
                    lvissible=NULL;
                removeLog(sh,lo,true);
                sellogs.removeOne(lo);
                scCh=true;
            }
        if((sh->logics.count())&&((sh->firstLogic->kindS()==kSheme))){
            sh->firstLogic->TextResized(true);
        }
        if((sh->logics.values().count()==0)){
            if(sel->getSheme()==shemes.key(sh))
                sel->clear();
            delete sh;
        }
    }
    fRecordUndo=true;
    setEns(true,true);
    if(scCh&&ch_act){
        changed();
    }
}

void rScene::createLogic(QTextStream& out, char dir, rNode* insNode1, rNode* insNode2)
{
    setEns(false,false);
    rSheme* nsh=new rSheme(this);
    rLogic* nl=new rLogic(nsh);
    nl->fr=1; nl->frp=0; nl->to=2; nl->setKind(dir=='r'?(kRight):(kLeft));
    if(insNode1){
        nl->lncolor=(insNode1->ll.count())?insNode1->ll[0]->rncolor:insNode1->rl[0]->rncolor;
    }
    if(insNode2){
        nl->rncolor=(insNode2->ll.count())?insNode2->ll[0]->rncolor:insNode2->rl[0]->rncolor;
    }
    out << 1<<'\n';
    nsh->putToStream(out);
    out.seek(0);
    delete nsh;
    setEns(true,true);
}

rSheme* rScene::logicPaste(QPointF *poi, bool forceEmptySel, bool toBuff, bool ch_act){
    fRecordUndo=false;
    bool dirRight=!sel->reversed();
    rSheme* cursh=shemes.item(sel->getSheme());
    rNode*  insNode1=cursh?cursh->nodes.item(sel->getNode1()):NULL;
    rNode*  insNode2=cursh?cursh->nodes.item(sel->getNode2()):NULL;
    rLogic* insLogic=cursh?cursh->logics.item(sel->getLogic()):NULL;
    if(insLogic){
        dirRight=(insLogic->kindS()!=kLeft);
    }
    if((insNode1)&&(!insNode2)){
        qreal sr=0;
        foreach(rLogic* lo,insNode1->ll+insNode1->rl){
            sr+=(lo->kindS()==kLeft)?-1:1;
        }
        dirRight=sr>=0;
    }
    QString ts;
    QTextStream out(&ts);
    createLogic(out,dirRight?'r':'l',insNode1,insNode2);
    fRecordUndo=true;
    rSheme* ret=elemPaste(out,poi,forceEmptySel,toBuff,ch_act);
    return ret;
}

int rScene::saveToStream(QTextStream &out){
    out << "version\n1\n";
    sel->saveToStream(out);
    out << shemes.count()<<'\n';
    foreach(rSheme* sh,shemes.values()){
        out << sh->stringKey() <<"\n";
        sh->putToStream(out);
    }
    out<<curEd.length()<<'\n';
    out<<curEd<<'\n';
    int pos=0;
    if(curEd.length()){
        pos=edits[editsId.indexOf(curEd)]->curPos();
    }
    out<<pos<<'\n';
    QPointF c=views()[0]->mapToScene(QPoint());//todo fix
    out<<c.x()<<'\n';
    out<<c.y()<<'\n';
    return 0;
}

int rScene::loadFromStream(QTextStream &out, bool ch_call, bool readId){
    char c;
    out.seek(0);
    int oldVersion = fileVersion;
    fileVersion = 0;
    QString key;
    out>>key;
    if(key=="version")
        out>>fileVersion>>c;
    else
        out.seek(0);
    if(fileVersion>=1)
        sel->loadFromStream(out);
    qint32 shlen;
    out>>shlen;
    bool scCh=false;
    setEns(false,false);
    if(fileVersion==0)
        shemes.clear();
    QList<rSheme*> rem = shemes.values();
    for(int i=0;i<shlen;++i){
        rShemeMap::TKey key=shemes.freeKey();
        if(fileVersion>=1)
            out>>key>>c;
        rSheme* nsh=shemes.item(key);
        rem.removeOne(nsh);
        nsh=nsh?nsh:new rSheme(this);
        nsh->layoutEnable = false;
        if(nsh->getFromStream(out, readId)){
            nsh->layoutEnable = true;
            nsh->llayout();
            scCh=true;
        }else
            delete nsh;
    }
    foreach (rSheme* s, rem)
        delete s;
    setEns(true,true);
    if(scCh&&ch_call) changed();
    int n=0;
    out>>n>>c;
    QString ce=out.read(n);
    out>>c;
    int p=0;
    out>>p>>c;
    if(editsId.contains(ce)){
        edits[editsId.indexOf(ce)]->setFocus();
        edits[editsId.indexOf(ce)]->setCurPos(p);
    }
    qreal x,y;
    out>>x>>c;
    out>>y;
    savedLT=QPointF(x,y);
    tCenter.start();
    fileVersion = oldVersion;
    return 0;
}

void rScene::changed(bool trans, bool byedit){
    if(!translateEn) {
        trans=false;
    }
#ifndef DEFINTEG
    timeoutEditTimer->start(500);
#endif
    emit siChanged(trans,byedit);
}

void rScene::privateSlChanged(bool record){
    if(!record) return;
    if(fRecordUndo){
        unRePos++;
        undoRedo.resize(unRePos+1);
        undoRedo.last().clear();
        QTextStream s(&undoRedo.last());
        saveToStream(s);
    }
    tAutoSave.start();
}

void rScene::fRecordUndoTrue()
{
    fRecordUndo=true;
}

void rScene::svissible()
{
    if((lvissible)) {
        if((views().count())&&(views()[0])){
            QRectF r=lvissible->mapToScene(lvissible->childrenBoundingRect()).boundingRect();
            //todo zoom
            if(views()[0]->horizontalScrollBar()->minimum()>r.left()){
               views()[0]->horizontalScrollBar()->setMinimum(r.left());
            }
            if(views()[0]->horizontalScrollBar()->maximum()<r.right()){
               views()[0]->horizontalScrollBar()->setMaximum(r.right());
            }
            if(views()[0]->verticalScrollBar()->minimum()>r.top()){
               views()[0]->verticalScrollBar()->setMinimum(r.top());
            }
            if(views()[0]->verticalScrollBar()->maximum()<r.bottom()){
               views()[0]->verticalScrollBar()->setMaximum(r.bottom());
            }
            views()[0]->ensureVisible(r,20,20);//todo nodesize
        }
        lvissible=0;
    }
}

void rScene::sAutoScroll()
{
    views()[0]->ensureVisible(QRectF(savedLT,views()[0]->size()),0,0);
}

void rScene::sAutoSave()
{
    save(path);
}

void rScene::elemPaste(QPointF poi, bool forceEmptySel)
{
    if(bufferString.length()){
        bufferStream.seek(0);
        elemPaste(bufferStream,&poi,forceEmptySel);
        bufferString.clear();
    }else{
        logicPaste(&poi,forceEmptySel);
    }
}

void rScene::readSettings()
{
    tMouseReleaseEvent.setInterval(Options::instance()->uiSceneMousebuttondelay);
    tMouseReleaseEventWait.setInterval(Options::instance()->uiSceneMousebuttondelay);
}

void rScene::doUndo(){
    if((unRePos>0)&&(unRePos<undoRedo.size())){
        unRePos--;
        fRecordUndo=false;
        QTextStream s(&undoRedo[unRePos]);
        loadFromStream(s);
        tAutoSave.start();
        tFRecordUndoTrue.start();
    }
}

void rScene::doRedo(){
    if(unRePos+1<undoRedo.size()){
        unRePos++;
        fRecordUndo=false;
        QTextStream s(&undoRedo[unRePos]);
        loadFromStream(s);
        tAutoSave.start();
        tFRecordUndoTrue.start();
    }
}

void rScene::save(QString path){
    if(!path.length()){
        return;
    }
    QString stdir=path.section( '/', 0,-2 );
    QDir dir(stdir);
    foreach (QFileInfo fi, dir.entryInfoList(QDir::Files)){
        if(fi.suffix()=="rsns")
            QFile(fi.absoluteFilePath()).remove();
    }
    foreach (rSheme* sh, shemes.values()) {
        if(sh->header()){
            QString show_name=sh->header()->text();
            QFile f1(stdir+"/"+sh->stringKey()+".rsns");
            f1.open(QFile::WriteOnly);
            QTextStream out1(&f1);
            out1<<show_name;
            f1.close();
        }
    }
    emit si_tree_up();
    QFile file(path);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream str(&file);
    saveToStream(str);
    file.close();
    setPath(path);
}

int rScene::open(QString path){
    setPath(path);
    QFile file(path);
    QTextStream str(&file);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    loadFromStream(str);
    file.close();
    return 0;
}

void rScene::setPath(QString p){
    path=p;
    rDocument* doc=dynamic_cast<rDocument*>(views()[0]);
    if(doc==NULL){
        return;
    }
    if(doc->tabWidget){
        QTabWidget * tw=doc->tabWidget;
        QFile f1(QFileInfo(path).dir().absolutePath()+"/name");
        f1.open(QFile::ReadOnly);
        QTextStream out1(&f1);
        QString Label=out1.readAll();
        f1.close();
        caption=Label;
        if (tw){
            tw->setTabText(tw->indexOf(doc),Label);
        }
    }
}

void rScene::hideText(){
    setEns(false,false);
    fRecordUndo=false;
    QList<rLogic*> sellogs;
    QList<rSheme*> needLL;
    QList<QGraphicsItem *> sels=selectedItems();
    foreach(QGraphicsItem* se,sels){
        rLogic* lo=dynamic_cast<rLogic*>(se);
        if((lo)&&(lo->metaObject()->className()==QString("rLogic"))){
            sellogs.append(lo);
        }
    }
    if((shemes.count())&&(sellogs.count()==0)){
        foreach(rSheme* sh,shemes.values()){
            sellogs.append(sh->logics.values());
        }
    }
    bool show_s=sellogs.first()->showText;
    show_s=!show_s;
    foreach(rLogic* lo,sellogs){
        lo->showText=show_s;
        lo->TextResized();
        if(!needLL.contains(lo->sheme)){
            needLL.append(lo->sheme);
        }
    }
    foreach(rSheme* sh,needLL){
        sh->llayout();
    }
    fRecordUndo=true;
    setEns(true,true);
    changed(false);
}

void rScene::setColor(QColor colo){
    setEns(false,false);
    QList<rSheme*> mod_shemes;
    foreach(QGraphicsItem* se,selectedItems()){
        QObject* lo=dynamic_cast<QObject*>(se);
        if(dynamic_cast<rNode*>(lo)){
            rNode* no=dynamic_cast<rNode*>(lo);
            foreach(rLogic* l,no->ll){
                l->rncolor=colo.rgb();
            }
            foreach(rLogic* l,no->rl){
                l->lncolor=colo.rgb();
            }
            if(!mod_shemes.contains(no->sheme)){
                mod_shemes.append(no->sheme);
            }
        }else if((lo)&&(lo->metaObject()->className()==QString("rLogic"))){
            dynamic_cast<rLogic*>(lo)->color=colo.rgb();
            if(!mod_shemes.contains(dynamic_cast<rLogic*>(lo)->sheme)){
                mod_shemes.append(dynamic_cast<rLogic*>(lo)->sheme);
            }
        }
    }
    foreach(rSheme *sh,mod_shemes){
        sh->llayout();
    }
    setEns(true,true);
    changed(false);
}

void rScene::setFont(const QFont &font){
    QList<rSheme*> sellogs;
    QList<QGraphicsItem *> sels=selectedItems();
    foreach(QGraphicsItem* se,sels){
        rSheme* lo=dynamic_cast<rSheme*>(se);
        if((lo)&&(lo->metaObject()->className()==QString("rLogic"))){
            sellogs.append(lo);
        }
    }
    if(sellogs.count()==0) sellogs.append(shemes.values());
    foreach(rSheme* sh,sellogs){
        sh->setFont(font);
        sh->llayout();
    }
    QGraphicsScene::setFont(font);
}

void rScene::createShemeFromSelText(){
    if(!editsId.contains(curEd)){
        return;
    }
    setEns(false,false);
    rTextEdit* focIt=edits[editsId.indexOf(curEd)];
    if((focIt)&&(focIt->selectedText().length())){
        rSheme* sh=logicPaste(NULL,false,false);
        if(sh){
            setEns(false,false);
            rLogic* lo=sh->firstLogic;
            lo->UpText->setText(focIt->selectedText());
            lo->DownText->setText(tr(""));
            lo->setKind(kSheme);
        }
        setEns(true,true);
        changed();
    }
    setEns(true,true);
}

QString rScene::_tr(const char *s){
    return tr(s);
}

void rScene::removeDefaults(){
    setEns(false,false);
    bool s=false;
    foreach(rSheme* sh,shemes.values()){
        foreach(rLogic* lo,sh->logics.values()){
            rLogic* par=lo;
            if(par->UpText->text()==par->tr("Condition")){
                par->UpText->setText("");
                s=true;
            }
            if(par->DownText->text()==par->tr("Actions")){
                par->DownText->setText("");
                s=true;
            }
            if(par->UpText->text()==par->sheme->scene->_tr("R-scheme")){
                par->UpText->setText("");
                s=true;
            }
        }
    }
    setEns(true,true);
    if(s)
        changed();
}

void rScene::slScroll()
{
    if(mouseDown){
        views()[0]->ensureVisible(QRectF(mouseMovePos,QSizeF(1,1)),5,5);
    }
}

void rScene::focusInEvent(QFocusEvent *event){

}

QSharedPointer<redSelection> rScene::getSel() const
{
    return sel;
}

QList<rLogic*> rScene::selectedLogics(QList<rLogic*>* m_sellogs, bool shemeLogs){
    QList<rLogic*> sellogs;
    if(m_sellogs==NULL){
        QList<QGraphicsItem *> sels=selectedItems();
        foreach(QGraphicsItem* se,sels){
            rLogic* lo=dynamic_cast<rLogic*>(se);
            if((lo)&&(lo->metaObject()->className()==QString("rLogic"))){
                sellogs.append(lo);
            }
        }
    }else{
        sellogs=*m_sellogs;
    }
    if(shemeLogs)
        foreach(rLogic* lo,sellogs){
            if(lo->kindS()==kSheme){
                foreach(rLogic* l,lo->sheme->logics.values()){
                    if(!sellogs.contains(l)){
                        sellogs.append(l);
                    }
                }
            }
        }
    return sellogs;
}

void rScene::shemeRedrawed(rSheme* sheme)
{//todo ignore other prev. sheme
    sel->update();
}

QPointF rScene::findNextPoint(const QList<QPointF>& mKeyboardGrid,QPointF current,char dir)
{
    QPointF res(current);
    float len = 0;
    bool first = true;
    foreach(QPointF p, mKeyboardGrid){
        QPointF d = p - current;
        float lx = qAbs(d.x());
        float ly = qAbs(d.y());
        bool m = false;
        if(dir =='r' && p.x()>current.x() && qAbs(p.x() - current.x())>=qAbs(p.y()-current.y())){
            m = true;
            //ly*=10;
        }
        if(dir =='l' && p.x()<current.x() && qAbs(p.x() - current.x())>=qAbs(p.y()-current.y())){
            m = true;
            //ly*=10;
        }
        if(dir =='u' && p.y()<current.y() && qAbs(p.x() - current.x())<=qAbs(p.y()-current.y())){
            m = true;
            //lx*=10;
        }
        if(dir =='d' && p.y()>current.y() && qAbs(p.x() - current.x())<=qAbs(p.y()-current.y())){
            m = true;
            //lx*=10;
        }
        float l = lx*lx + ly*ly;
        if(m && (first || l<len)){
            len=l;
            res = p;
            first = false;
         }
    }
    return res;
}

QPointF rScene::getNextKeyboardGridPoint(QPointF current, char dir, bool secondPoint, QPointF firstPos)
{
    QList<QPointF> mKeyboardGrid;
    foreach (rSheme* sh, shemes.values()){
        mKeyboardGrid.append(sh->mKeyboardGridNode);
        if(!secondPoint)
            mKeyboardGrid.append(sh->mKeyboardGridLogic);
        mKeyboardGrid.append(sh->mKeyboardGridVertical);
    }
    if(secondPoint)
    {
       QList<QPointF> tmp;
        foreach (QPointF p, mKeyboardGrid)
            if(ritemAt(p,false)!=ritemAt(firstPos,false))
                tmp.append(p);
        mKeyboardGrid=tmp;
    }
    if(mKeyboardGrid.count()==0)
        return QPointF(0,0);
    QPointF res=findNextPoint(mKeyboardGrid,current,dir);
    return res;
}

void rScene::focusOnNextEdit(char dir)
{
    if(!curEd.length())
        return;
    QList<QPointF> points;
    QList<rTextEdit*> vissible_edits;
    foreach (rTextEdit* ed, edits)
    {
        if (!ed->isVisible())
            continue;
        vissible_edits.append(ed);
        points.append(ed->sceneBoundingRect().center());

    }
    if(vissible_edits.count()==0)
        return;
    QPointF cur=edits[editsId.indexOf(curEd)]->sceneBoundingRect().center();
    rTextEdit* ed = vissible_edits[points.indexOf(findNextPoint(points,cur,dir))];
    ed->setFocus();
    views()[0]->ensureVisible(ed);//todo cursor pos
}

void rScene::keyPressEvent(QKeyEvent *event){
    if(curEd==""){
        char d = 0;
        switch(event->key()){
            case Qt::Key_Right: d='r'; break;
            case Qt::Key_Left:  d='l'; break;
            case Qt::Key_Up:    d='u'; break;
            case Qt::Key_Down:  d='d'; break;
        }
        if(d){
            shiftAccept=true;
            if(sel->getNode1()&&event->modifiers()==Qt::ShiftModifier)
                sel->placePoint2(getNextKeyboardGridPoint(sel->getNode2()?sel->point2Pos():sel->point1Pos(),d,true,sel->point1Pos()));
            else
                if(sel->point1OnNode()&&event->modifiers()==Qt::ControlModifier&&(d=='r'||d=='l')){
                    QString ts;
                    QTextStream out(&ts);
                    rSheme* cursh=shemes.item(sel->getSheme());
                    rNode* insStart=cursh->nodes.item(sel->getNode1());
                    createLogic(out,d,insStart,insStart);
                    elemPaste(out);
                }
                else
                    sel->placePoint1(getNextKeyboardGridPoint(sel->point1Pos(),d));
            return;
        }
        if(event->key()==Qt::Key_Return){
            if(sel->selected())
            {
                elemPaste(sel->point1Pos());
                return;
            }
        }
        rLogic* last=0;
        if(event->key()==Qt::Key_Delete || event->key()==Qt::Key_Backspace){
            if(sel->getNode1())
            {            
                //QString side="";
                if(!sel->getNode2()){
                    if(event->key()==Qt::Key_Backspace){
                        foreach (rLogic* lo, shemes.item(sel->getSheme())->nodes.item(sel->getNode1())->ll)
                        {
                            if(lo->kindS()==kSheme)
                                continue;
                            last=lo;
                        }
                    }
                    if(event->key()==Qt::Key_Delete)
                    {
                        foreach (rLogic* lo, shemes.item(sel->getSheme())->nodes.item(sel->getNode1())->rl)
                        {
                            if(lo->kindS()==kSheme)
                                continue;
                            //lrestore = last;
                            //side="l";
                            last=lo;
                        }
                    }
                }else{
                    //side="lr";
                    foreach (rLogic* lo, shemes.item(sel->getSheme())->nodes.item(sel->getNode1())->ll) {
                        if(lo->kindS()==kSheme)
                            continue;
                        if(shemes.item(sel->getSheme())->nodes.item(sel->getNode2())->rl.contains(lo))
                        {
                            //lrestore=last;
                            last=lo;
                        }
                    }
                    foreach (rLogic* lo, shemes.item(sel->getSheme())->nodes.item(sel->getNode1())->rl) {
                        if(lo->kindS()==kSheme)
                            continue;
                        if(shemes.item(sel->getSheme())->nodes.item(sel->getNode2())->ll.contains(lo))
                        {
                            //lrestore=last;
                            last=lo;
                        }
                    }

                }
            }else if(sel->getLogic())
                last=shemes.item(sel->getSheme())->logics.item(sel->getLogic());
            QList<rLogic*> ls;
            if(last)
                ls.append(last);
            elemDelete(&ls);
            return;
        }
    }
    shiftAccept=false;
    QGraphicsScene::keyPressEvent(event);
}

void rScene::keyReleaseEvent(QKeyEvent *event){
    if(curEd==""){
        if(event->key()==Qt::Key_Shift){
            if(sel->getNode1()&&sel->getNode2()&&shiftAccept)
            {
                elemPaste(sel->point2Pos());
                return;
            }
        }
    }
    QGraphicsScene::keyReleaseEvent(event);
}
