#ifndef DEBUGDIALOG_H
#define DEBUGDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QPlainTextEdit>


class rScene;
namespace Ui {
class DebugDialog;
}

class DebugDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DebugDialog(QWidget *parent = 0);
    ~DebugDialog();
    void setDebug(rScene* s, QStringList d, QStringList t);
private slots:
    void on_plainTextEdit_cursorPositionChanged();
public:
    Ui::DebugDialog *ui;
private:
    rScene* scene;
    QStringList dInfo;
};

#endif // DEBUGDIALOG_H
