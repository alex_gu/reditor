#include "rnode.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "options.h"

rNode::rNode(rSheme* she, rNodeMap::TKey key) :
    QGraphicsObject(she)
{
    sheme=she;
    setFlag(QGraphicsItem::ItemIsSelectable);
    this->setHandlesChildEvents(false);
    setCacheMode(CACHE_M);
    lx=0;
    ly=0;
    isStart=false;
    isEnd=false;
    sheme->nodes.add(this,key);
}

rNode::~rNode()
{
    sheme->nodes.remove(this);
}

void rNode::setKind(nKind k, bool changedcall){
    if(k==m_Kind){
        return;
    }
    m_Kind=k;
    foreach (rLogic* lo,ll){
        lo->rnKind=k;
    }
    foreach (rLogic* lo,rl){
        lo->lnKind=k;
    }
    prepareGeometryChange();
    if(changedcall){
        changed();
    }
    update();
}

void rNode::setWidthF(int w){
    if (m_WidthF==w){
        return;
    }
    QPen q=pen();
    q.setWidthF(w);
    q.setJoinStyle(Qt::RoundJoin);
    setPen(q);
}

void rNode::setVert(){
    if (ll.count()){
        m_vline_l_x1=-5;
        m_vline_l_y1=ll.first()->ly-ly;
        if((isEnd)&&(ll.count()>1)){
            m_vline_l_y1=ll[1]->ly-ly;
        }
        m_vline_l_x2=-5;
        m_vline_l_y2=ll.last()->ly-ly;
    } else{
        m_vline_l_x1=-5;
        m_vline_l_y1=0;
        m_vline_l_x2=-5;
        m_vline_l_y2=0;
    }
    if (rl.count()){
        m_vline_r_x1=5;
        m_vline_r_y1=rl.first()->ly-ly;
        if((isStart)&&(rl.count()>1)){
            m_vline_r_y1=rl[1]->ly-ly;
        }
        m_vline_r_x2=5;
        m_vline_r_y2=rl.last()->ly-ly;
    } else{
        m_vline_r_x1=5;
        m_vline_r_y1=0;
        m_vline_r_x2=5;
        m_vline_r_y2=0;
    }
    update();
}

QRectF rNode::boundingRect() const
{
    return QRectF(QPointF(-9,qMin(m_vline_l_y1,m_vline_r_y1)-15),
                  QPointF( 9,qMax(m_vline_l_y2,m_vline_r_y2)+10));
}


QVariant rNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemSelectedChange:
        setWidthF(value.toBool() ? 3 : 1);
        break;
    }
    return QGraphicsObject::itemChange(change, value);
}
void rNode::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    switch (kind()){
        case kCircle:setKind(kSquare);break;
        case kSquare:setKind(kDiamond);break;
        case kDiamond:setKind(kRightNo);break;
        case kRightNo:setKind(kLeftNo);break;
        case kLeftNo:setKind(kPara);break;
        case kPara:setKind(kVBox);break;
        default:
        case kVBox:setKind(kCircle);break;
    }
}
void rNode::setColor(QColor colo){
    QPen p=pen();
    p.setColor(colo);
    setPen(p);
}

void rNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                  QWidget *widget){
    Q_UNUSED(widget);
    Q_UNUSED(option);
    QPolygon pg;
    if(m_vline_r_y1<m_vline_r_y2){
        painter->drawLine(m_vline_r_x1,m_vline_r_y1,m_vline_r_x2,m_vline_r_y2);
    }
    if(m_vline_l_y1<m_vline_l_y2){
        painter->drawLine(m_vline_l_x1,m_vline_l_y1,m_vline_l_x2,m_vline_l_y2);
    }
    painter->setPen(pen());
    switch (m_Kind){
    case kCircle:
        painter->drawEllipse(-5,-5,10,10);
        break;
    case kSquare:
        painter->drawRect(-5,-5,10,10);
        break;
    case kDiamond:
        pg.append(QPoint(-5,0));
        pg.append(QPoint(0,-8));
        pg.append(QPoint(5,0));
        pg.append(QPoint(0,8));
        painter->drawPolygon(pg);
        break;
    case kVBox:
        painter->drawRect(-5,-8,10,16);
        break;
    case kRightNo:
        pg.append(QPoint(-5,-5));
        pg.append(QPoint(5,0));
        pg.append(QPoint(-5,5));
        painter->drawPolygon(pg);
        break;
    case kLeftNo:
        pg.append(QPoint(5,-5));
        pg.append(QPoint(-5,0));
        pg.append(QPoint(5,5));
        painter->drawPolygon(pg);
        break;
    case kPara:
        pg.append(QPoint(10,-5));
        pg.append(QPoint(0,5));
        pg.append(QPoint(-10,5));
        pg.append(QPoint(0,-5));
        pg.append(QPoint(10,-15));
        pg.append(QPoint(0,-5));
        painter->drawPolygon(pg);
        painter->setBrush(pen().brush());
        pg.clear();
        pg.append(QPoint(10,-15));
        pg.append(QPoint(7,-8));
        pg.append(QPoint(3,-12));
        painter->drawPolygon(pg);
        break;
    }
#ifdef mdebug
    painter->setFont(QFont("Arial",6));
    painter->drawText(-2,3, stringKey());
#endif
}

void rNode::changed()
{
    sheme->changed();
}

QVector<rLogic*> rNode::logics(char side,char dir,bool gen_only,bool no_ing){
    QVector<rLogic*> los=(side=='l')?ll:rl;
    QVector<rLogic*> ret;
    foreach(rLogic* lo,los){
        if(((!gen_only)||(lo->lgen))&&(lo->dir()==dir)&&((!lo->ignoreInLogList)||(no_ing))){
            ret.append(lo);
        }
    }
    return ret;
}

QVector<rLogic*> rNode::logics(char side,lKind k,bool gen_only,bool no_ing){
    QVector<rLogic*> los=(side=='l')?ll:rl;
    QVector<rLogic*> ret;
    foreach(rLogic* lo,los){
        if(((!gen_only)||(lo->lgen))&&(lo->kind()==k)&&((!lo->ignoreInLogList)||(no_ing))){
            ret.append(lo);
        }
    }
    return ret;
}

QString rNode::stringKey(){
    if(sheme)
        return QString::number(sheme->nodes.key(this));
     return "0";
}

