#ifndef RSCENE_H
#define RSCENE_H

#define CACHE_M ItemCoordinateCache
#include <QtWidgets>
#include <QGraphicsScene>
#include <QtAlgorithms>
#include "idcontainer.h"

class rLogic;
class rNode;
class rSheme;
class rTextEdit;
class rTransparentGraphicsEllipseItem;
struct redSelection;
class rScene : public QGraphicsScene
{
    friend class redSelection;
    friend class rTextEdit;
    Q_OBJECT
public:
    static QString mimeT(){return "application/rsheme";}
    explicit rScene(QObject *parent = 0);
    ~rScene();
    rShemeMap shemes;
    QString path;
    QString caption;
    QString curEd;
    int fileVersion;
//todo to private
    int saveToStream(QTextStream & out);
    int loadFromStream(QTextStream &out,bool ch_call=true,bool readId=true);
    void doUndo();
    void doRedo();
    void setPath(QString p);
    void hideText();
    void setColor(QColor colo);
    void setFont(const QFont &font);
    void save(QString path);
    int open(QString path);
    bool elemCopy(QTextStream & out);
    rSheme* elemPaste(QTextStream & out, QPointF* poi=NULL, bool forceEmptySel=false, bool toBuff=false, bool ch_act=true);
    void elemDelete(QList<rLogic*> *m_sellogs=NULL,bool ch_act=true);
    rSheme* logicPaste(QPointF* poi=NULL,bool forceEmptySel=false,bool toBuff=false,bool ch_act=true);
    void clearElipseSel();
    void clearSecondElipseSel();
    void updateElipseSel();
    bool isElipseSel();
    void placeFirstElipseSel(QPointF ps);
    void placeSecondElipseSel(QPointF ps);
    void wheelEvent(QGraphicsSceneWheelEvent *wheelEvent);
    void mouseMoveEvent ( QGraphicsSceneMouseEvent * event );
    void mousePressEvent ( QGraphicsSceneMouseEvent * event );
    void mouseReleaseEvent ( QGraphicsSceneMouseEvent * event );
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent * event );
    void createShemeFromSelText();
    QString _tr(const char *s);
    void removeDefaults();
    QGraphicsItem* ritemAt(QPointF ps,bool edit=true) const;
    void fixShemeInSheme(rSheme *nsh);
    void setEns(bool tra,bool ext);
    void setAdv(bool v);
    void keyPressEvent(QKeyEvent *event);
    QList<rSheme *> sortedShemes();
    QList<rLogic *> selectedLogics(QList<rLogic *> *m_sellogs = NULL,bool shemeLogs=true);
    rNode *selShNode(rNodeMap::TKey key);
    void placeFirstRedSelection(QPointF ps);
    void shemeRedrawed(rSheme *sheme);
    void removeLog(rSheme *sheme, rLogic *l, bool silent);
    QSharedPointer<redSelection> getSel() const;
    void focusOnNextEdit(char dir);
signals:
    void si_tree_up();
    void siChanged(bool trans,bool byedit);
public slots:
    void changed(bool trans=true,bool byedit=false);
    void privateSlChanged(bool record=true);
    void fRecordUndoTrue();
    void svissible();
    void sAutoScroll();
    void sAutoSave();
    void elemPaste(QPointF poi,bool forceEmptySel=false);
    void readSettings();
    void slMouseReleaseEvent(bool forceEmptySel=false);
    void slScroll();
    void focusInEvent(QFocusEvent *event);
private:
    rSheme* moveSheme;
    QPointF moveShemeinitp;
    bool translateEn,extEn;
    QList<rTextEdit*> edits;
    QList<QString> editsId;
    QSharedPointer<redSelection> sel;
    QVector<QString> undoRedo;
    bool mouseDown;
    bool fRecordUndo;
    qint32 unRePos;
    QTimer *timeoutEditTimer;
    QTimer tAutoSave;
    QTimer tFRecordUndoTrue;
    rLogic* lvissible;
    QTimer tMouseReleaseEvent;
    QTimer tMouseReleaseEventWait;
    QTextStream bufferStream;
    QString bufferString;
    QPointF bufferpoi;
    QPointF lastDownPos;
    QPointF savedLT;
    QTimer tCenter;
    Qt::MouseButton butMouseReleaseEvent;
    QPointF posMouseReleaseEvent;
    QPointF posToGrid(QPointF p);
    QPointF getNextKeyboardGridPoint(QPointF current, char dir, bool secondPoint = false, QPointF firstPos = QPointF(0,0));
    QTimer scrollTimer;
    QPointF mouseMovePos;
    bool shiftAccept;
    void keyReleaseEvent(QKeyEvent *event);
    void findNodePositions(rNode *no, QPointF &ps, char &side, int &posL);
    QPointF findNextPoint(const QList<QPointF> &mKeyboardGrid, QPointF current, char dir);
    void createLogic(QTextStream &out, char dir, rNode *insNode1, rNode *insNode2);
};

#endif // RSCENE_H
