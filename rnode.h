#ifndef RNODE_H
#define RNODE_H

#include <QGraphicsEllipseItem>
#include <QAbstractGraphicsShapeItem>
#include <QPen>
#include <QPolygonF>
#include "rlogic.h"
#include "idcontainer.h"

enum nKind { kCircle, kSquare,kDiamond,kVBox,kNOn,kLeftNo,kRightNo,kPara };

class rLogic;
class rSheme;

class rNode : public QGraphicsObject
{
    Q_OBJECT
    friend class rScene;
    friend class rLogic;
    friend class rSheme;
    friend class rTranslator;
    friend class redSelection;
    qreal lx,ly;
    bool isStart;
    bool isEnd;
    QVector<rLogic*> ll,rl;
    bool visited;
    rSheme* sheme;
    rNode(rSheme* she, rNodeMap::TKey key);
    void setKind(nKind k,bool changedcall=true);
    void setColor(QColor colo);
    QRectF boundingRect() const;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void setVert();
    inline QPen pen(){return mpen;}
    inline void setPen(QPen p){mpen=p;}
    inline nKind kind(){return m_Kind;}
    static inline float radius(){return 5.0;}
    ~rNode();
    QString stringKey();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void changed();
    void setWidthF(int w);
    nKind m_Kind;
    int m_WidthF;
    QPen mpen;
    qreal  m_vline_r_x1,
    m_vline_r_y1,
    m_vline_r_x2,
    m_vline_r_y2,
    m_vline_l_x1,
    m_vline_l_y1,
    m_vline_l_x2,
    m_vline_l_y2;
    QVector<rLogic*> logics(char side,char dir,bool gen_only=false,bool no_ing=false);
    QVector<rLogic*> logics(char side,lKind k,bool gen_only=false,bool no_ing=false);
};

#endif // RNODE_H
