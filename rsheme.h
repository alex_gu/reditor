#ifndef RSHEME_H
#define RSHEME_H

#include <QtWidgets>
#include <QAbstractGraphicsShapeItem>
#include <QPen>
#include <QtCore/QObject>
#include <QGraphicsSceneMouseEvent>
#include "idcontainer.h"

class rScene;
class rTextEdit;

class rSheme : public QGraphicsObject
{
    Q_OBJECT
    friend class rScene;
    friend class rNode;
    friend class rLogic;
    friend class redSelection;
    friend class rTranslator;
    rLogic* firstLogic;
    rLogic* loreplace;
    rScene* scene;
    bool layoutEnable;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0){}
    void setColor(QColor colo);
    explicit rSheme(rScene* cscene);
    ~rSheme();
    QRectF boundingRect() const;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void llayout(bool silent=false);
    void applyall_pos();
    int fixwpos(rNode* node,qreal px,QList<rLogic*>* newLogics,bool init=true);
    int buildRs(QList<rLogic*>& logics, rNode* p, qreal px, qreal py, rLogicMap::TKey fromlog, bool init=true);
    bool perekrx(qreal l1,qreal r1,qreal l2,qreal r2);
    void removeLog(rLogic* l, bool silent=false);
    void putToStream(QTextStream &out, QList<rLogic *> *logicsList=NULL);
    bool getFromStream(QTextStream &out, bool readId);
    rNode* startNode();
    rNode* endNode();
    bool LeftToRightConnected(rNode* l,rNode* r,bool init=false);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void setFont(const QFont &font);
    QList<QPointF> mKeyboardGridNode;
    QList<QPointF> mKeyboardGridLogic;
    QList<QPointF> mKeyboardGridVertical;
    int startKey();
    int endKey();
    void prepareConnection(rSheme *master);
    QString stringKey();
    bool m_save_is_valid;
    rNode *m_startn,*m_endn;
    bool show_text;
    void updateKeyboardGrid();
    void changed(bool trans=true,bool byedit=false);
public:
    rLogic* getFirstLogic();
    rLogicMap logics;
    rNodeMap nodes;
    rTextEdit* header();
    rTextEdit* body();
};

#endif // RSHEME_H
