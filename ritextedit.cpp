#include "ritextedit.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "rtranslator.h"
#include "ridocument.h"
#include "options.h"

rTextEdit::rTextEdit(QString tx,rScene *scene,QGraphicsItem *parent,rLogic* log) :
    QGraphicsObject(parent)
{
    m_focus=false;
    buf=0;
    resizebuf(10,10);
    upbufer=true;
    m_scene=scene;
    logic=log;
    oldw=0;
    oldh=0;
    loffset=0;
    m_tb_beg=-1;
    m_tb_end=-2;
    _x=0;
    _y=0;
    _w=0;
    _h=0;
    _flx=0;
    _fly=0;
    _flw=0;
    _flh=0;
    _dw=0;
    if(!m_scene){
        m_scene=dynamic_cast<rScene*>(parent->scene());
    }
    m_view=(rIDocument*)(m_scene->views()[0]);
    m_par_ed=(TextEditor::BaseTextEditorWidget*)m_view->m_ed;
    m_doc=m_par_ed->document();
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges,true);
    setFlag(QGraphicsItem::ItemIsFocusable,true);
    setAcceptHoverEvents(true);
    setAcceptTouchEvents(true);
    if(scene&&!parent){
        scene->addItem(this);
    }
    setText(tx);
    int i=0;
    while(m_scene->editsId.contains(QString::number(i))){
        i++;
    }
    id=QString::number(i);
    m_scene->edits.append(this);
    m_scene->editsId.append(id);
    setCacheMode(CACHE_M);
    updatetimer.setInterval(500);
    updatetimer.setSingleShot(true);
    updatetimer.stop();
    connect(&updatetimer,SIGNAL(timeout()),this,SLOT(updatetimer_out()));
}

rTextEdit::~rTextEdit()
{    
    m_scene->editsId.removeOne(id);
    m_scene->edits.removeOne(this);
    delete buf;
}

void rTextEdit::setId(QString nid)
{
    id=nid;
    m_scene->editsId[m_scene->edits.indexOf(this)]=nid;
}

void rTextEdit::updateLater(bool sim)
{
    if(sim){
        updatetimer_out();
    }else{
        updatetimer.start();
    }
}

void rTextEdit::setText(QString s,bool any){
    if(((logic)&&(!logic->sheme->scene->extEn))){
        m_text=s;
        lasttext=s;
        _dw=s.length()*m_view->fcharw+m_view->extW();//todo poss bug
        _h=m_view->fcharh;
        return;
    }
    if(m_text==s){
        return;
    }
    if(m_tb_beg>0){
        QTextCursor c(m_doc);
        c.setPosition(m_doc->findBlockByLineNumber(m_tb_beg+1).position()+loffset);
        c.setPosition(m_doc->findBlockByLineNumber(m_tb_end).position()-1,QTextCursor::KeepAnchor);
        c.insertText(s);
    }else{
        if(m_text!=s){
            rtext=s;
            m_text=s;
            textEdited();
        }
    }
}

void rTextEdit::textEdited(){
    if(lasttext==m_text){
        return;
    }
    bool needUndo=false;
    bool forcetrans=false;
    if((m_view->trans)&&(logic->UpText==this)){
        forcetrans=m_view->trans->needTrans(lasttext,m_text,logic,needUndo);
    }
    lasttext=m_text;
    if(forcetrans){
        logic->changed(true,needUndo);
    }
}

void rTextEdit::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    if(upbufer){
        redrawbuf();
    }
    painter->drawPixmap(QRect(0,0,_dw,_h),*buf,QRect(0,0,_dw,_h));
    if(m_focus){
        QPen p(Qt::black);
        p.setStyle(Qt::DotLine);
        p.setWidthF(1);
        painter->setPen(p);
        painter->drawRect(boundingRect());
    }
}

void rTextEdit::my_setFocus(Qt::FocusReason focusReason, bool man)
{
    rLogic* par=logic;
    if((man)&&(par)&&
            (((par->UpText==this)&&(text()==par->tr("Condition")))||
             ((par->DownText==this)&&(text()==par->tr("Actions")))||
             ((par->UpText==this)&&(par->kindS())==kSheme)&&
             (text()==m_scene->_tr("R-scheme")))){
        QTextCursor c(m_doc);
        c.setPosition(m_tb_beg_pos);
        c.setPosition(m_tb_end_pos,QTextCursor::KeepAnchor);
        m_par_ed->setTextCursor(c);
    }
    m_focus=true;
    update();
}

QRectF rTextEdit::boundingRect() const {
    if((logic)&&(!logic->showText)){
        return QRectF();
    }
    return QRectF(QPointF(0,0),QSize(_dw,_h));
}

void rTextEdit::setFont(const QFont &font){
    return;
}

QString rTextEdit::selectedText(){
    QTextCursor c=m_par_ed->textCursor();
    if(c.hasSelection()){
        int curpos=c.position();
        if((m_tb_beg_pos<=curpos)&&(curpos<=m_tb_end_pos)){
            return c.selectedText();
        }
    }
    return 0;
}

QFont rTextEdit::font(){
    return QFont();
}

void rTextEdit::setDefaultTextColor(QColor){
    return;
}

void rTextEdit::setVisible(bool visible)
{
    QGraphicsObject::setVisible(visible);
}

bool rTextEdit::setEdRect(int flx,int fly,int flw,int flh,int x,int y,int w,int h,int dw)
{
    if(m_text.length()<1){
        flh/=2;
        h/=2;
        w+=30;
        flw+=30;
    }
    _flx=flx;
    _fly=fly;
    _flw=flw;
    _flh=flh;
    _x=x;
    _y=y;
    if(/*(flx!=_flx)||(fly!=_fly)||(flw!=_flw)||(flh!=_flw)||(x!=_x)||(y!=_y)||*/(w!=_w)||(h!=_h)||(dw!=_dw)){
        _w=w;
        _h=h;
        _dw=dw;
        resizebuf(w+m_view->fcharw*loffset,h);
        return true;
    }
    return false;
}

void rTextEdit::my_clearFocus()
{
    m_focus=false;
    update();
}

void rTextEdit::updatetimer_out()
{
    upbufer=true;
    update();
}

void rTextEdit::redrawbuf()
{
    if(!buf){
        return;
    }
    if(m_tb_beg<0){
        return;
    }
    if(_x>m_par_ed->width()){
        return;
    }
    if(_y>m_par_ed->height()){
        return;
    }
    QPainter p(buf);
    m_par_ed->render(&p,QPoint(),QRegion(_x,_y,_w,_h));
    if(loffset>0){
        p.drawPixmap(m_view->extraw,0,*buf,_flx,0,_w,_flh);
    }
    upbufer=false;
}

void rTextEdit::resizebuf(int nw, int nh)
{
    int di=50;
    int ww=nw;
    int wh=nh;
    ww=(ww/di+1)*di;
    wh=(wh/di+1)*di;
    if((!buf)||(buf->size()!=QSize(ww,wh))){
        QPixmap* sbuf=buf;
        buf=new QPixmap(ww,wh);
        QPainter p(buf);
        p.fillRect(buf->rect(),Qt::white);
        if(sbuf){
            p.drawPixmap(QPointF(),*sbuf,sbuf->rect());
            delete sbuf;
        }
    }
}

int rTextEdit::curPos()
{
    int cpos=m_par_ed->textCursor().position();
    if((m_tb_beg_pos<=cpos)&&(cpos<=m_tb_end_pos)){
        return cpos-m_tb_beg_pos;
    }else{
        return 0;
    }
}

bool rTextEdit::setCurPos(int p,bool act)
{
    QTextCursor c=m_par_ed->textCursor();
    c.setPosition(m_tb_beg_pos+p);
    m_view->ResetCurBlock();
    m_par_ed->setTextCursor(c);
    m_par_ed->setFocus();
    return true;
}
