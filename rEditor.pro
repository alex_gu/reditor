#-------------------------------------------------
#
# Project created by QtCreator 2011-11-20T19:17:44
#
#-------------------------------------------------



QT       += core gui widgets printsupport

TARGET = rEditor
TEMPLATE = app
#DEFINES += mdebug
TRANSLATIONS += ru.ts ua.ts

#LIBS+= advapi32.lib

SOURCES += main.cpp\
        mainw.cpp \
    rscene.cpp \
    rsheme.cpp \
    rlogic.cpp \
    rnode.cpp \
    rhighlighter.cpp \
    debugdialog.cpp \
    rtranslator.cpp \
    documentwindow.cpp \
    rdocument.cpp \
    rtextedit.cpp \
    options.cpp \
    redselection.cpp

HEADERS  += mainw.h \
    rscene.h \
    rsheme.h \
    rlogic.h \
    rnode.h \
    rhighlighter.h \
    debugdialog.h \
    rtranslator.h \
    documentwindow.h \
    rtransparentgraphicsellipseitem.h \
    rdocument.h \
    rtextedit.h \
    options.h \
    qzipwriter_p.h \
    qzipreader_p.h \
    idcontainer.h \
    redselection.h

INCLUDEPATH += $(QTDIR)/src/gui/text

FORMS    += \
    debugdialog.ui \
    options.ui \
    mainw.ui

RESOURCES += \
    icons.qrc \
    translate.qrc

RC_FILE = myapp.rc

OTHER_FILES += \
    icons/wrench_orange.png \
    icons/wand.png \
    icons/rainbow.png \
    icons/printer.png \
    icons/paste_plain.png \
    icons/page_white_csharp.png \
    icons/page_white_cplusplus.png \
    icons/page_white_camera.png \
    icons/page_copy.png \
    icons/page_add.png \
    icons/page.png \
    icons/link_add.png \
    icons/folder_delete.png \
    icons/folder_add.png \
    icons/folder.png \
    icons/disk_multiple.png \
    icons/disk.png \
    icons/delete.png \
    icons/cancel.png \
    icons/camera_go.png \
    icons/camera.png \
    icons/arrow_undo.png \
    icons/arrow_redo.png \
    icons/add.png \
    icons/accept.png



include(src/qtsingleapplication.pri)









































