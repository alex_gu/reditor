#include "rlogic.h"
#include "rscene.h"
#include "rsheme.h"
#include "rnode.h"
#include "rctexedit.h"
#include "rtranslator.h"
#include "options.h"

rLogic::rLogic(rSheme* she,QString* uText,QString* dText) :
    QGraphicsObject(she)
{
    shreplace=NULL;
    liw=0;
    lih=0;
    libh=0;
    markr=false;
    UpText=0;
    DownText=0;
    ignoreInLogList=false;
    showText=true;
    sheme=she;
    UpText=new rTextEdit((uText==NULL)?tr("Condition"):*uText,she->scene,this,this);
    DownText=new rTextEdit((dText==NULL)?tr("Actions"):*dText,she->scene,this,this);
    setFlag(QGraphicsItem::ItemIsSelectable);
    this->setHandlesChildEvents(false);
    setPos(0,0);
    TextResized();
    mKind=kNO;
    color=Qt::black;
    lncolor=Qt::black;
    rncolor=Qt::black;
    rnKind=kCircle;
    lnKind=kCircle;
    mWid=0;
    lx=0;
    ly=0;
    lgen=true;
    setCacheMode(CACHE_M);
    sheme->logics.add(this);
}

void rLogic::TextResized(bool forcelay)
{
    prepareGeometryChange();
    if((!UpText)||(!DownText)) return;
    UpText->setVisible(showText);
    DownText->setVisible(showText);
    if((showText)&&(sheme->logics.count()>1)&&(mKind==kSheme)&&(DownText->isEmpty())){
        DownText->setVisible(false);
    }
    int log_d=5;
    int log_sp=2;
    qreal nliw=(qMax(UpText->boundingRect().width(),DownText->boundingRect().width()));
    qreal nlih=DownText->boundingRect().height();
    qreal nlibh=UpText->boundingRect().height();
    if(!showText){
        nlih=0;
        nlibh=0;
        nliw=0;
    }
    nlih+=log_d+log_sp;
    if(mKind==kSheme){
        nliw=DownText->boundingRect().width();
        log_d=1;
        if(DownText->isEmpty()){
            nlih=0;
        }
        nlibh=qMax(16.0,nlibh);
    }
    nlibh+=log_d+log_sp;
    nliw=qMax(25.0,nliw);
    nliw+=18;
    bool needlayout=false;
    if(nliw!=liw){
        liw=nliw;
        needlayout=true;
    }
    if(nlih!=lih){
        lih=nlih;
        needlayout=true;
    }
    if(nlibh!=libh){
        libh=nlibh;
        needlayout=true;
    }
    setTextPos();
    if((forcelay)||(needlayout)){
        sheme->llayout();
    }
}

void rLogic::setKind(lKind k)
{
    if(k==mKind){
        return;
    }
    mKind=k;
    switch(k){
    case kRight:
    case kRightD:
        mKindS=kRight;
        mdir='r';
        mdirO='l';
        break;
    case kLeft:
    case kLeftD:
        mKindS=kLeft;
        mdir='l';
        mdirO='r';
        break;
    case kSpecial:
    case kSpecialD:
        mKindS=kSpecial;
        mdir='r';
        mdirO='l';
        break;
    case kSheme:
        mKindS=kSheme;
        mdir='r';
        mdirO='r';
        break;
    default:
        mKindS=kRight;
        mdir='r';
        mdirO='l';
    }
    if(kindS()==kSheme){
        UpText->setDefaultTextColor(Qt::black);
        DownText->setDefaultTextColor(Qt::darkBlue);
        QFont fr=UpText->font();
        fr.setBold(true);
        UpText->setFont(fr);
    }else{
        UpText->setDefaultTextColor(Qt::darkRed);
        DownText->setDefaultTextColor(Qt::darkBlue);
        QFont fr=UpText->font();
        fr.setBold(false);
        UpText->setFont(fr);
    }
    QFont frd=DownText->font();
    frd.setBold(false);
    DownText->setFont(frd);
    TextResized();
    changed();
    update();
}

int rLogic::setWid(qreal w)
{
    if (w==mWid) return 0;
    mWid=w;
    setTextPos();
    update();
    return 1;
}
void rLogic::setWidthF(qint16 w)
{
    if (mWidthF==w) return;
    mWidthF=w;
    QPen q=pen();
    q.setWidthF(w);
    q.setJoinStyle(Qt::RoundJoin);
    setPen(q);
}

QRectF rLogic::boundingRect() const
{
    if(mKind==kSheme){
        return QRectF(0,-libh,20,libh);
    }else{
        return QRectF(7,-5,mWid-14,5+5);
    }
}

QVariant rLogic::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch(change){
    case ItemSelectedChange:
        setWidthF(value.toBool() ? 3 : 1);
        break;
    }
    return QGraphicsObject::itemChange(change, value);
}
void rLogic::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button()==Qt::LeftButton){
        if(kind()!=kSheme){
            bool llay=true;
            switch (kind()){
            case kLeft:
                setKind(kLeftD);
                break;
            case kLeftD:
                setKind(kRight);
                break;
            case kRight:
                setKind(kRightD);
                break;
            case kRightD:
                setKind(kSpecial);
                break;
            case kSpecial:
                setKind(kLeft);
                break;
            }
            if(llay) sheme->llayout();
        }
    }
    if(event->button()==Qt::RightButton){
        if(kind()==kSheme){
            ignoreUp=true;
            foreach(rLogic* lo,sheme->logics.values()){
                lo->setSelected(1);
            }
        }else{
            ignoreUp=true;
            getAllChilds(true);
        }
    }
}

void rLogic::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    if(ignoreUp){
        ignoreUp=false;
    }else{
        QGraphicsObject::mouseReleaseEvent(event);
    }
}

void rLogic::putToStream(QTextStream &out)
{
    if(!UpText){
        return;
    }
    out << UpText->text().length()<<'\n';
    out << DownText->text().length()<<'\n';
    out << UpText->id.length()<<'\n';//ver2
    out << DownText->id.length()<<'\n';//ver2
    out << UpText->text()<<'\n';
    out << DownText->text()<<'\n';
    out << UpText->id<<'\n';///ver2
    out << DownText->id<<'\n';//ver2
    out << fr<<' '<<frp<<' '<<to<<' '<<lgen<<' '<<mKind<<'\n';
    out << lncolor<<' '<<rncolor<<' '<<color<<'\n';
    out << lnKind<<' '<<rnKind<<'\n';
    if(sheme->scene->fileVersion>=1)
        out << showText<<'\n';
}

void rLogic::getFromStream(QTextStream &out, bool readId)
{
    if(!UpText) {
        return;
    }
    char c;
    qint32 len1=0;
    qint32 len2=0;
    qint32 leni1=0;
    qint32 leni2=0;
    out >> len1>>c;
    out >> len2>>c;
    out >> leni1>>c;
    out >> leni2>>c;
    QString s1,s2,si1,si2;
    s1=out.read(len1);
    out>>c;
    s2=out.read(len2);
    out>>c;
    si1=out.read(leni1);
    out>>c;
    si2=out.read(leni2);
    out>>c;
    int newK;
    out >> fr >> frp >> to >>lgen>>newK>>c;
    out >> lncolor >> rncolor >> color>>c;
    out >> lnKind >> rnKind>>c;
    if(sheme->scene->fileVersion>=1)
    {   int i=0;
        out >> i >> c;
        showText = (i!=0);
    }
    setKind((lKind)newK);
    if(readId){
        UpText->setId(si1);
    }
    UpText->setText(s1,true);
    if(readId){
        DownText->setId(si2);
    }
    DownText->setText(s2,true);
}

void rLogic::changed(bool trans, bool byedit)
{
    sheme->changed(trans,byedit);
}

rLogic::~rLogic()
{
    delete UpText;
    delete DownText;
    sheme->logics.remove(this);
}
void rLogic:: setTextPos()
{
    qreal xpu=9;
    qreal xpd=9;
    qreal dy_u=5;
    qreal dy_d=5;
    if(mKind==kSheme){
        xpu=15;
        xpd=5;
        dy_u=1;
        dy_d=1;
    }
    UpText->setPos(xpu-UpText->boundingRect().left(),-UpText->boundingRect().bottom()-dy_u);
    DownText->setPos(xpd-DownText->boundingRect().left(),-DownText->boundingRect().top()+dy_d);
}

void rLogic::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    Q_UNUSED(widget);  Q_UNUSED(option);
    qreal pw = painter->pen().widthF();
    painter->setPen(pen());
    painter->setBrush(pen().brush());
    qreal W=mWid-8;
    qreal B=0;
    bool dies=false;
    if((mKind==kLeftD)||(mKind==kRightD)||(mKind==kSpecialD)){
        dies=true;
        W-=14;
        if(mKind==kLeftD){
            B+=14;
        }
        qreal pos=(B>0)?4+7:4+W+7;
        painter->drawLine(pos-2,-5,pos-2,5);
        painter->drawLine(pos+2,-5,pos+2,5);
        painter->drawLine(pos-5,-2,pos+5,-2);
        painter->drawLine(pos-5,2,pos+5,2);

    }
    switch (mKind){
    case kSpecial:
    case kSpecialD:
    {
        painter->drawLine(4,-3,W+4/*-11*/,-3);
        painter->drawLine(4,3,W+4/*-11*/,3);
        break;
    }
    case kSheme:
    {
        QBrush br(Qt::yellow);
        painter->setBrush(br);
        painter->drawEllipse(0,qMin(-libh+6,-13.0),13,10);//!!!
        break;
    }
    case kRight:
    case kRightD:
    case kLeft:
    case kLeftD:{
        QPolygonF m_ArrowHead;
        qreal dir = ((mKind ==kLeft)||(mKind==kLeftD)) ? 1 : -1;
        m_ArrowHead.append(QPointF(0, 0));
        m_ArrowHead.append(QPointF(dir * 11, 8 / 2 - pw / 2));
        m_ArrowHead.append(QPointF(dir * 11, - 8 / 2 + pw / 2));
        m_ArrowHead.translate(4+B,0);
        if (dir<1){
            m_ArrowHead.translate(W, 0);
        }
        painter->drawPolygon(m_ArrowHead);
        painter->drawLine(4+B,0,B+W+4,0);
        break;
    }
    }
#ifdef mdebug
    QPen p;
    p.setColor(Qt::darkRed);
            QBrush b;
    painter->setPen(p);
    QFont frnt;
    frnt.setFamily("Arial");
    frnt.setPixelSize(8);
    painter->setFont(frnt);
    painter->drawText(10,2,infoString());
#endif
}

QString rLogic::infoString(){
    return stringKey()+"!"+ QString::number(fr)+"@"+QString::number(frp)+"<->"+QString::number(to)+"@"+QString::number(top)
            +" tr:"+traId+"@"+parentTraId;
}

QList<rLogic*> rLogic::getAllChilds(bool select,bool translated){
    QList<rLogic*> ret;
    rTranslator trans;
    QStringList result,d_info;
    rSheme* she=sheme;
    if (!translated){
        QHash<int,int> hl;
        trans.translateSheme(she,hl,result,d_info,0,true);
    }
    ret.append(this);//!!?
    foreach(rLogic* lo,she->logics.values()){
        if(lo->parentTraId==traId){
            ret.append(lo->getAllChilds(select,true));
        }
    }
    if(select) {
        foreach(rLogic* lo,ret){
            lo->setSelected(true);
        }
    }
    return ret;
}


QString rLogic::_tr(const char *s){
    return tr(s);
}


QString rLogic::stringKey(){
    if(sheme)
        return QString::number(sheme->logics.key(this));
     return "0";
}

QPointF rLogic::getSelPoint(char side){
    return scenePos()+QPointF(width()/3*(side=='l'?1:2),0);
}

