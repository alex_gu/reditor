#ifndef IDCONTAINER_H
#define IDCONTAINER_H

#include <QObject>
#include <QtWidgets>
#include <limits>
class rNode;
class rLogic;
class rSheme;

template <class TValue>
class idContainer
{
public:
    typedef int TKey;
private:
    QMap<TKey, TValue> mMap;
public:

    explicit idContainer(){}
    TKey freeKey() const
    {
        for(TKey i=1; i<std::numeric_limits<TKey>::max();++i){
            if(!mMap.contains(i))
                return i;
        }
        return notFoundKey();
    }

    static TKey notFoundKey()
    {
        return 0;
    }

    static TValue notFoundValue()
    {
        return NULL;
    }

    TValue add(TValue item, TKey key)
    {
        mMap.insert(key, item);
        return item;
    }

    TValue add(TValue item)
    {
        return add(item, freeKey());
    }

    void remove(TKey key)
    {
        if(mMap.contains(key))
        {
            mMap.remove(key);
        }
    }

    void remove(TValue item){
        remove(mMap.key(item, notFoundKey()));
    }

    TValue item(const TKey& key){
        if(mMap.contains(key))
            return mMap[key];
        else
            return TValue();
    }

    const TValue item(const TKey& key) const{
        if(key && mMap.contains(key))
            return mMap[key];
        else
            return TValue();
    }

    const TKey key(const TValue& item) const{
        return mMap.key(item, notFoundKey());
    }

    QList<TValue> values() const{//todo cache
        return mMap.values();
    }

    QList<TKey> keys() const{
        return mMap.keys();
    }

    int count() const{
        return mMap.count();
    }

    void clear(){
        QList<TKey> kl = keys();
        foreach(TKey k,kl)
            remove(k);
    }

};

typedef idContainer<rLogic*> rLogicMap;
typedef idContainer<rNode*>  rNodeMap;
typedef idContainer<rSheme*> rShemeMap;

#endif // IDCONTAINER_H
