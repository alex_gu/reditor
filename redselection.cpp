#include "redselection.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rnode.h"
#include <QtWidgets>
#include <QGraphicsScene>
#include <QtAlgorithms>

void redSelection::swap(){
    qSwap(redNode1,redNode2);
    qSwap(node1,node2);
    qSwap(node1IdxL,node2IdxL);
    qSwap(node1Side,node2Side);
    qSwap(node1Offset,node2Offset);
}


rNodeMap::TKey redSelection::getNode1() const
{
    return node1;
}

rNodeMap::TKey redSelection::getNode2() const
{
    return node2;
}

rLogicMap::TKey redSelection::getLogic() const
{
    return logic;
}

char redSelection::getLogicSide() const
{
    return logicSide;
}

int redSelection::getNode1IdxL() const
{
    return node1IdxL;
}

int redSelection::getNode2IdxL() const
{
    return node2IdxL;
}

char redSelection::getNode1Side() const
{
    return node1Side;
}

char redSelection::getNode2Side() const
{
    return node2Side;
}

rShemeMap::TKey redSelection::getSheme() const
{
    return sheme;
}

redSelection::redSelection(rScene* scene):
     redNode1(new rTransparentGraphicsEllipseItem(-4,-4,8,8))
    ,redNode2(new rTransparentGraphicsEllipseItem(-4,-4,8,8))
    ,sheme(rShemeMap::notFoundKey())
    ,node1(rNodeMap::notFoundKey())
    ,node2(rNodeMap::notFoundKey())
    ,logic(rLogicMap::notFoundKey())
    ,logicSide(' ')
    ,node1IdxL(0)
    ,node2IdxL(0)
    ,node1Side(' ')
    ,node2Side(' ')
    ,node1Offset(0.0,0.0)
    ,node2Offset(0.0,0.0)
    ,logicOffset(0.0,0.0)
    ,mScene(scene)
{
    scene->addItem(redNode1.data());
    scene->addItem(redNode2.data());
    redNode1->setActive(false);
    redNode2->setActive(false);
    QBrush b(Qt::red);
    redNode1->setBrush(b);
    redNode2->setBrush(b);
    QPen p(Qt::black);
    redNode1->setPen(p);
    redNode2->setPen(p);
    clear();
}

void redSelection::saveToStream(QTextStream &out){
    out<<sheme<<'\n'<<node1<<'\n'<<node2<<'\n'<<logic
    <<'\n'<< logicSide<<'\n'<<  node1IdxL <<'\n'<<  node2IdxL
    <<'\n'<< node1Side <<'\n'<< node2Side
    <<'\n'<<node1Offset.x()<<'\n'<<node1Offset.y()
    <<'\n'<<node2Offset.x()<<'\n'<<node2Offset.y()
    <<'\n'<<logicOffset.x()<<'\n'<<logicOffset.y()<<'\n';
}

void redSelection::loadFromStream(QTextStream &out){
    char ch; float tmp;
    out>>sheme>>node1>>node2>>logic>>ch
    >> logicSide>>ch>>node1IdxL >>ch>>node2IdxL>>ch
    >> node1Side>>ch >>node2Side>>ch
    >>node1Offset.rx()>>node1Offset.ry()
    >>node2Offset.rx()>>node2Offset.ry()
    >>logicOffset.rx()>>logicOffset.ry();
}

void redSelection::clearPoint2(){
    node2=0;
    redNode2->hide();
}

void redSelection::clear(){
    node1=rNodeMap::notFoundKey();
    node2=rNodeMap::notFoundKey();
    logic=rLogicMap::notFoundKey();
    sheme=rShemeMap::notFoundKey();
    redNode1->hide();
    clearPoint2();
}

void redSelection::placePoint1(QPointF ps)
{
    rLogic* lo = dynamic_cast<rLogic*>(mScene->ritemAt(ps));
    rNode*  no = dynamic_cast<rNode* >(mScene->ritemAt(ps));
    if(lo){
        firstPos = ps;
        clear();
        redNode1->setPos(ps.x(),lo->scenePos().y());
        redNode1->show();
        redNode1->setZValue(10);
        logic=lo->sheme->logics.key(lo);
        sheme=mScene->shemes.key(lo->sheme);
        logicSide=(redNode1->scenePos().x()>=lo->scenePos().x()+lo->width()/2)?
                 'r':'l';
        logicOffset=ps-(lo->scenePos()+QPointF(lo->width()/2,0));
        logicOffset.ry()=0;
        logicOffset.rx()/=lo->width();
        redNode1->show();
        redNode1->setZValue(10);
    }else if(no){
        firstPos = ps;
        clear();
        node1=no->sheme->nodes.key(no);
        sheme=mScene->shemes.key(no->sheme);
        findNodePositions(no,ps,node1Side,node1IdxL,node1Offset);
        redNode1->setPos(ps);
        redNode1->show();
        redNode1->setZValue(10);
        mScene->views()[0]->ensureVisible(QRectF(ps,QSizeF(0,0)),rNode::radius(),rNode::radius());
    }
}

void redSelection::placePoint2(QPointF ps)
{
    clearPoint2();
    rNode* no = dynamic_cast<rNode* >(mScene->ritemAt(ps));
    rNode* first = sheme && mScene->shemes.item(sheme) ? mScene->shemes.item(sheme)->nodes.item(node1) : NULL;
    if(first && no && no!=first && no->sheme==first->sheme){
        node2=no->sheme->nodes.key(no);
        findNodePositions(no,ps,node2Side,node2IdxL,node2Offset);
        redNode2->setPos(ps);
        redNode2->show();
        redNode2->setZValue(10);
    }

}

bool redSelection::selected()
{
    return node1 || node2 || logic;
}

void redSelection::setNode1(rNodeMap::TKey val)
{
    node1 = val;
}

void redSelection::setNode2(rNodeMap::TKey val)
{
    node2 = val;
}

void redSelection::update(){
    rNode* pnode1;
    rNode* pnode2;
    rSheme* psheme;
    rLogic* plogic;
    if((psheme=mScene->shemes.item(sheme))&&(pnode1=psheme->nodes.item(node1))){
        if(node1IdxL==-1||node1Side==' ')
            redNode1->setPos(pnode1->scenePos());
        else{
            updateNodePosition(redNode1.data(),pnode1,node1Side,node1IdxL,node1Offset);
        }
        redNode1->setZValue(10);
        redNode1->show();
        if((pnode2=psheme->nodes.item(node2))){
                if(node2IdxL==-1||node2Side==' ')
                    redNode2->setPos(pnode2->scenePos());
                else{
                    updateNodePosition(redNode2.data(),pnode2,node2Side,node2IdxL,node2Offset);
                }
                redNode2->setZValue(10);
                redNode2->show();
        }else
            redNode2->hide();
    }
    else
        if(psheme&&(plogic=psheme->logics.item(logic))){
            redNode1->setPos(plogic->scenePos()+QPointF(plogic->width()/2,0)+logicOffset*plogic->width());
            redNode1->setZValue(10);
            redNode1->show();
        }
        else
            hide();
}

void redSelection::shiftNode1Idx(int offset)
{
    int prev = node1IdxL;
    node1IdxL+=offset;
    if(prev==0||node1IdxL==0)
    {
        node1Offset.ry()=(node1IdxL==0)?-1.0f:0.5f;
    }
}

void redSelection::shiftNode2Idx(int offset)
{
    int prev = node2IdxL;
    node2IdxL+=offset;
    if(prev==0||node2IdxL==0)
    {
        node2Offset.ry()=(node2IdxL==0)?-1.0f:0.5f;
    }
}

bool redSelection::reversed()
{
    rSheme* psheme=mScene->shemes.item(sheme);
    if(psheme){
        rNode*  pnode1=psheme->nodes.item(node1);
        rNode*  pnode2=psheme->nodes.item(node2);
        if(&pnode1&&pnode2){
            if(redNode1->scenePos().x()>=pnode1->scenePos().x()){
                if(mScene->shemes.item(getSheme())->LeftToRightConnected(pnode2,pnode1,true)){//imp
                    return true;
                }
            }else{
                if(redNode2->scenePos().x()>=pnode2->scenePos().x()){//simplified
                    return true;
                }else{
                    return true;
                }
            }
        }
    }
    return false;
}

void redSelection::updateNodePosition(visual* redNode,rNode* pnode,char nodeSide,int nodeIdxL,QPointF nodeOffset)
{
    QVector<rLogic*> logs = nodeSide=='l'?pnode->ll:pnode->rl;
    if(nodeIdxL==0)
        redNode->setPos(pnode->scenePos()+nodeOffset*rNode::radius());
    else
    if(nodeIdxL==logs.count())
        redNode->setPos(QPointF(pnode->scenePos().x(),logs.last()->scenePos().y())+nodeOffset*rNode::radius());
    else
    if(nodeIdxL>0&nodeIdxL<logs.count())
        redNode->setPos(pnode->scenePos().x()+nodeOffset.x()*rNode::radius(),
                        logs[nodeIdxL-1]->scenePos().y()*(1.f-nodeOffset.y())+logs[nodeIdxL]->scenePos().y()*nodeOffset.y());
}


QPointF redSelection::getPoint1MousePos() const
{
    return firstPos;
}

QPointF redSelection::point1Pos() const
{
    return redNode1->scenePos();
}

QPointF redSelection::point2Pos() const
{
    return redNode2->scenePos();
}

void redSelection::findNodePositions(rNode* no,QPointF& ps,char& side,int& posL,QPointF& offset)
{
    if(no->isStart || no->isEnd)
        ps.setY(qMax(no->scenePos().y()-no->radius(),ps.y()));
    if(qAbs(ps.y()-no->scenePos().y())>=no->radius()){
        ps=QPointF(no->scenePos().x()+no->radius()*((ps.x()<no->scenePos().x())?-1:1),ps.y());
    }else{
        ps=no->scenePos();
    }
    side=' ';//on node
    posL =-1;
    offset=(ps-no->scenePos())/rNode::radius();
    const QVector<rLogic*>* logs=NULL;
    if(ps.x()>no->scenePos().x()){
        side='r';
        posL =0;
        logs = &no->rl;
    }else
    if(ps.x()<no->scenePos().x()){
        side='l';
        posL =0;
        logs = &no->ll;
    }
    if(logs)
        for(int i=0;i<logs->count();i++){
            rLogic* curLo=(*logs)[i];
            rLogic* nextLo=(i+1<logs->count())?(*logs)[i+1]:NULL;
            if(curLo->scenePos().y()<ps.y())
            {
                posL=i+1;

                if(nextLo)
                    offset.ry()=(ps.y()-curLo->scenePos().y())/(nextLo->scenePos().y()-curLo->scenePos().y());
                else
                    offset.ry()=(ps.y()-curLo->scenePos().y())/rNode::radius();
            }
        }
}

bool redSelection::point1OnNode()//todo ren  node1
{
    return node1&&((node1IdxL==-1||node1Side==' '||
           (mScene->shemes.item(sheme)&& mScene->shemes.item(sheme)->nodes.item(node1)->isStart&&node1Side=='l')));
}

bool redSelection::point2OnNode()
{
    return node2&&((node2IdxL==-1||node2Side==' '||
           (mScene->shemes.item(sheme)&& mScene->shemes.item(sheme)->nodes.item(node2)->isEnd&&node2Side=='r')));
}

void redSelection::hide()
{
    redNode1->hide();
    redNode2->hide();
}
