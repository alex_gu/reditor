#ifndef RTRANSPARENTGRAPHICSELLIPSEITEM_H
#define RTRANSPARENTGRAPHICSELLIPSEITEM_H

#include <QGraphicsEllipseItem>

class rTransparentGraphicsEllipseItem : public QGraphicsEllipseItem
{
public:
    explicit rTransparentGraphicsEllipseItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent = 0):
        QGraphicsEllipseItem( x,  y,  w,  h, parent ){}
    virtual QPainterPath shape()const {return QPainterPath();}
};

#endif // RTRANSPARENTGRAPHICSELLIPSEITEM_H
