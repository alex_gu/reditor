#include "rtextedit.h"
#include "rscene.h"
#include "rlogic.h"
#include "rsheme.h"

rTextEdit::rTextEdit(QString tx,rScene *cscene,QGraphicsItem *parent,rLogic* log) :
    QGraphicsTextItem(parent)
{
    scene=cscene;
    logic=log;
    m_text="";
    if(!scene) scene=dynamic_cast<rScene*>(parent->scene());
    connect(document(),SIGNAL(contentsChanged()),this,SLOT(textEdited()));
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges,true);
    setTextInteractionFlags(Qt::TextEditorInteraction);
    document()->setUndoRedoEnabled(false);
    QTextOption to=document()->defaultTextOption();
    to.setTabStop(25);
    document()->setDefaultTextOption(to);
    if(scene){
        scene->addItem(this);
    }
    setText(tx);
    int i=0;
    while(scene->editsId.contains(QString::number(i))){
        i++;
    }
    id=QString::number(i);
    scene->edits.append(this);
    scene->editsId.append(id);
    setCacheMode(CACHE_M);
    ignore_next_m_up=false;
    setFont(scene->font());
}

rTextEdit::~rTextEdit()
{    
    scene->editsId.removeOne(id);
    scene->edits.removeOne(this);
}

void rTextEdit::setId(QString nid)
{
    id=nid;
    scene->editsId[scene->edits.indexOf(this)]=nid;
}

void rTextEdit::setText(QString s,bool any){
    QGraphicsTextItem::setPlainText(s);
    textEdited(any);
}

void rTextEdit::textEdited(bool any){
    m_text=QGraphicsTextItem::toPlainText();
    if((!any)&&(lasttext==m_text)) return;
    int l=m_text.length();
    if (l < 1) {
        setTextWidth(20);
    } else {
        setTextWidth(-1);
    }
    setFrame();
    logic->TextResized();
    logic->changed();
    update();
    lasttext=m_text;
}

QString rTextEdit::selectedText(){
    QTextCursor c=textCursor();
    if(c.hasSelection()){
        return c.selectedText();
    }
    return 0;
}

int rTextEdit::curPos()
{
    return textCursor().position();
}

bool rTextEdit::setCurPos(int p, bool act)
{
    QTextCursor c=textCursor();
    c.setPosition(p);
    setTextCursor(c);
    scene->setFocusItem(this);
    return true;
}

QRectF rTextEdit::boundingRect() const{
    if((logic)&&(!logic->showText)) return QRectF();
    QRectF ret=QGraphicsTextItem::boundingRect();
    ret.setTopLeft(ret.topLeft()+QPointF(3,5));
    ret.setBottomRight(ret.bottomRight()-QPointF(4,3));
    if(m_text.length()<1) ret.setHeight(ret.height()/2);
    return ret;
}

void rTextEdit::focusInEvent ( QFocusEvent * event )  {
    scene->curEd=id;
    QGraphicsTextItem::focusInEvent(event);
    rLogic* par=logic;
    if((par)&&
            (((par->UpText==this)&&(toPlainText()==par->tr("Condition")))||
             ((par->DownText==this)&&(toPlainText()==par->tr("Actions")))||
             ((par->UpText==this)&&(par->kindS()==kSheme))&&
             (toPlainText()==scene->_tr("R-scheme")))){
        QTextCursor cursor(document());
        cursor.setPosition(0);
        cursor.setPosition(toPlainText().length(), QTextCursor::KeepAnchor);
        setTextCursor(cursor);
        ignore_next_m_up=true;
    }
    update();
}

void rTextEdit::focusOutEvent ( QFocusEvent * event )  {
    scene->curEd="";
    QTextCursor cursor(document());
    cursor.setPosition(!textCursor().selection().isEmpty()?textCursor().selectionStart():textCursor().position());
    setTextCursor(cursor);
    QGraphicsTextItem::focusOutEvent(event);
    update();
}

void rTextEdit::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    if(ignore_next_m_up){
        ignore_next_m_up=false;
    }else{
        QGraphicsTextItem::mouseReleaseEvent(event);
    }
}

void rTextEdit::setFrame(bool any){
    QColor co=(m_text.length()<1)?QColor(230,230,230,150):Qt::transparent;
    if((any)||(document()->frameAt(0)->frameFormat().background().color()!=co)){
        QTextFrameFormat frf=document()->frameAt(0)->frameFormat();
        frf.setBackground(QBrush(co));;
        document()->frameAt(0)->setFrameFormat(frf);
    }
}

void rTextEdit::setFont(const QFont &font){
    QGraphicsTextItem::setFont(font);
    textEdited(true);
}

void rTextEdit::keyPressEvent(QKeyEvent *event)
{
    char d = 0;
    switch(event->key()){
        case Qt::Key_Right: d='r'; break;
        case Qt::Key_Left:  d='l'; break;
        case Qt::Key_Up:    d='u'; break;
        case Qt::Key_Down:  d='d'; break;
    }
    if(event->modifiers()==Qt::ControlModifier&&d)
        scene->focusOnNextEdit(d);
    else
        QGraphicsTextItem::keyPressEvent(event);
}

