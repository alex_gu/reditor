#ifndef REDSELECTION_H
#define REDSELECTION_H

#include "rtransparentgraphicsellipseitem.h"
#include <QSharedPointer>
#include "idcontainer.h"

class rScene;
class redSelection
{
    typedef rTransparentGraphicsEllipseItem visual;
    QSharedPointer<visual> redNode1;
    QSharedPointer<visual> redNode2;
    rShemeMap::TKey sheme;
    rNodeMap::TKey node1;
    rNodeMap::TKey node2;
    rLogicMap::TKey logic;
    char logicSide;
    int  node1IdxL;
    int  node2IdxL;
    char node1Side;
    char node2Side;
    QPointF node1Offset;
    QPointF node2Offset;
    QPointF logicOffset;
    QPointF firstPos;
    const rScene* mScene;
    void findNodePositions(rNode *no, QPointF &ps, char &side, int &posL, QPointF &offset);
    void updateNodePosition(visual *redNode1, rNode *pnode1, char node1Side, int node1IdxL, QPointF node1Offset);
public:
    redSelection(rScene *scene);
    void swap();
    void saveToStream(QTextStream &out);
    void loadFromStream(QTextStream &out);
    void clear();
    void clearPoint2();
    void placePoint1(QPointF ps);
    void placePoint2(QPointF ps);
    bool selected();
    void update();
    bool point1OnNode();
    bool point2OnNode();
    void hide();
    rNodeMap::TKey getNode1() const;
    rNodeMap::TKey getNode2() const;
    rLogicMap::TKey getLogic() const;
    char getLogicSide() const;
    int getNode1IdxL() const;
    int getNode2IdxL() const;
    char getNode1Side() const;
    char getNode2Side() const;
    rShemeMap::TKey getSheme() const;
    void setNode1(rNodeMap::TKey val);
    void setNode2(rNodeMap::TKey val);
    void shiftNode1Idx(int offset);
    void shiftNode2Idx(int offset);
    bool reversed();
    QPointF getPoint1MousePos() const;
    QPointF point1Pos() const;
    QPointF point2Pos() const;
};

#endif // REDSELECTION_H
