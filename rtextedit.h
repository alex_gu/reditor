#ifndef RITEXTEDIT_H
#define RITEXTEDIT_H

#include <QGraphicsRectItem>
#include <QTextEdit>
class rScene;
class rLogic;
class rTextEdit : public QGraphicsTextItem
{
    Q_OBJECT
public:
    QString id;
    explicit rTextEdit(QString tx,rScene *cscene=0,QGraphicsItem *parent=0,rLogic* log=0);
    ~rTextEdit();
    void setId(QString nid);
    inline QString text(){ return m_text;}
    void setText(QString s, bool any=false);
    inline bool isEmpty(){return m_text.length()==0;}
    QString selectedText();
    QRectF boundingRect() const;
    void focusInEvent(QFocusEvent *event);
    void focusOutEvent(QFocusEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void setFrame(bool any=false);
    void setFont(const QFont &font);
    void keyPressEvent(QKeyEvent *event);
private:
    QString m_text;
    bool ignore_next_m_up;
    QString lasttext;
    rScene* scene;
    rLogic* logic;
public slots:
    void textEdited(bool any=false);
    int curPos();
    bool setCurPos(int p,bool act=true);
};

#endif // RITEXTEDIT_H
