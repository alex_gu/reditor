#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <QSettings>
#include <QDebug>
namespace Ui {
class Options;
}

class Options : public QDialog
{
    Q_OBJECT
public:
    QStringList cppSeqgen;
    QStringList cppBlockgen;
    int uiSceneMousebuttondelay;
    static Options* instance();
    void show(bool s=true);
    void save(bool lang=true);
    void forceLang(int i);
    void setLI(int i);
    bool showCaptions;
    bool showShorts;
    bool startMax;
    bool keditLogics;
    bool keditNodes;
    bool keditVerticals;
    bool compact;
signals:
    void changed();
    void retranslate(QString id,QString qid);
private slots:
    void on_buttonBox_accepted();
private:
    Ui::Options *ui;
    QSettings* set;
    int curLang;
protected:
    explicit Options(QWidget *parent = 0);
    ~Options();
};

#endif // OPTIONS_H
