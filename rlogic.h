#ifndef RLOGIC_H
#define RLOGIC_H

#include <QtWidgets>
#include <QAbstractGraphicsShapeItem>
#include <QPen>
#include <QPolygonF>
#include "idcontainer.h"

enum lKind { kRight, kLeft, kSpecial,kSheme,kNO,kRightD, kLeftD, kSpecialD};

class rSheme;
class rTextEdit;
class rNode;
class rLogic :public QGraphicsObject
{
    Q_OBJECT
    friend class rScene;
    friend class rNode;
    friend class rSheme;
    friend class rTextEdit;
    friend class rTranslator;
    friend class redSelection;
    rSheme* shreplace;
    bool shreplaceUp;
    bool markr;
    qreal liw,lw,lih,libh,lx,ly,lbh;
    bool showText;
    rNodeMap::TKey fr;
    qint32 frp;
    rNodeMap::TKey to;
    qint32 top;
    QRgb lncolor,rncolor,color;
    qint32 lnKind,rnKind;
    bool visited;
    qint32 lgen;
    rLogicMap::TKey lmainl;
    rSheme* sheme;
    rTextEdit *UpText;
    rTextEdit *DownText;
    QString parentTraId;
    QString next;
    bool ignoreInLogList;
    bool prodSeq;
    QList<rLogic*> prodSeqList;
    rLogic(rSheme* she,QString* uText=NULL,QString* dText=NULL);
    ~rLogic();
    inline lKind kind(){return mKind;}
    inline lKind kindS(){return mKindS;}
    inline char dir(){return mdir;}
    inline char dirO(){return mdirO;}
    inline int dest(){return (mdir=='r')?to:fr;}
    inline int sour(){return (mdir=='r')?fr:to;}
    inline qreal width(){return mWid;}
    void setKind(lKind k);
    int setWid(qreal w);
    void setWidthF(qint16 w);
    QRectF boundingRect() const;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void putToStream(QTextStream &out);
    void getFromStream(QTextStream &out,bool readId);
    void setColor(QColor colo);
    QList<rLogic*> getAllChilds(bool select=false,bool translated=false);
    QString _tr(const char *s);
    void changed(bool trans=true,bool byedit=false);
    void setTextPos();
    QString stringKey();
    QPointF getSelPoint(char side);
    QString infoString();
    qreal mWid;
    qint16 mWidthF;
    bool ignoreUp;
    QPen mpen;
    lKind mKind;
    lKind mKindS;
    char mdir;
    char mdirO;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void TextResized(bool forcelay=false);
    inline QPen pen(){return mpen;}
    inline void setPen(QPen p){mpen=p;}
public:
    QString traId;//for translator
    rTextEdit* getUpText(){return UpText;}
    rTextEdit* getDownText(){return DownText;}
};

#endif // RLOGIC_H
