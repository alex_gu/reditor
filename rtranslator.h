#ifndef RTRANSLATOR_H
#define RTRANSLATOR_H

#include <QObject>
#include "options.h"

class rSheme;
class rLogic;
class rNode;
class rScene;
class rTextEdit;
class rTranslator : public QObject
{
    Q_OBJECT
public:
    explicit rTranslator(QObject *parent = 0,bool sim=false,bool inte=false);
    ~rTranslator();
    void translateSheme(rSheme* she,QHash<int,int> &HLogs,QStringList &result,QStringList &deb_info,int lang,bool no_output=false);
    void translateScene(rScene* sce, QStringList* rresult=NULL,QStringList* rd_info=NULL);
    void revTranslateSheme(rScene* sce);
    bool recurLast(rLogic *call, rLogic *lmain, int *end);
    bool needTrans(QString& sOld,QString& sNew,rLogic* lo,bool& needUndo);
    void findRepls();
    int tabCount(QString &s);
    void addTab(QString &s);
    void remTab(QString &s);
    void simplify(rScene* s);
private:
    bool genLoOfNo_no_map_used;
    QHash<QString,rLogic*> inv_seqNext_cache;
    QHash<rLogic*,QString> seqNext_cache[2];
    QHash<rLogic*,int> seqNextNoOfLo_cache;
    QHash<int,QString> seqGenLoOfNo_cache;
    QHash<rLogic*,QList<int> > lastLogPath_cache;
    QHash<int,rLogic*> inv_lastLogPath_cache;
    QHash<int,int> genLoOfNo_no_map;
    rSheme* s;
    QString label;
    QList<QString> vis;
    QStringList st;
    QStringList sl;
    QList<QString> parents;
    QString id_start,id_end,id_no_way;
    bool out;
    int iteration;
    QList< QList<QString> > vis_iteration;
    QString tabs;
    bool simple,integrate;
    rScene* scene;
    bool isSimpleBlockGen(rLogic *lo);
    bool isSimpleSeqGen(rLogic *lo);
    QString clearTabs(QString s);
    bool is_comment_at_and(QString s);
    void CleanTabSpace(QStringList &ut);
    int proc(rLogic* lo,QString tab,QString par_b,bool full=true);
    int doOut(rTextEdit* ed,QString ifemp,QString tab,QString id, QString bef="", QString aft="");
    rLogic* loById(QString id);
    bool  is_real_thr(rLogic* lo);
    QVector<QString> getLoPars(rLogic* lo,bool blockOnly=true,bool firstOnly=false);
    QString getLoParBl(rLogic* lo);
    QString mainLoOfNo(int no_id,char call_dir, bool cond, rLogic* call=NULL,QList<QString> *parents=NULL,bool dies=false);
    bool firstLogPath(rLogic *call, int endnode);
    bool lastLogPath(rLogic *call,int* node=NULL,QList<QString> *parents=NULL);
    rLogic* invLastlogpath(int node);
    QString seqGenLoOfNo(int no_id, bool combine=false);
    int seqNextNoOfLo(rLogic *call);
    QString seqNext(rLogic *call, QList<QString> *parents=NULL,int cachen=0);
    rLogic *invSeqnext(QString next);
    char noDir(int no_id);
};

#endif // RTRANSLATOR_H
