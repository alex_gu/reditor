#include <QApplication>
#include "mainw.h"

int main(int argc, char *argv[])
{
    QtSingleApplication instance("REditor SingleApplication", argc, argv);


    QString message;
    for (int a = 1; a < argc; ++a) {
    message += instance.arguments().at(a);
    if (a < argc-1)
        message += " ";
    }


    instance.sendMessage(message);
    if(instance.isRunning())
    return 0;

    MainW mw(0,message.length()==0);
    mw.handleMessage(message);
    mw.show();

    QObject::connect(&instance, SIGNAL(messageReceived(const QString&)),
             &mw, SLOT(handleMessage(const QString&)));

    instance.setActivationWindow(&mw, false);
    QObject::connect(&mw, SIGNAL(needToShow()), &instance, SLOT(activateWindow()));

    return instance.exec();


}

