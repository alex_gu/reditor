#ifndef RDOCUMENT_H
#define RDOCUMENT_H

#include <QWidget>
#include <QtGui>
#include <QGraphicsView>
#include <QTabWidget>

class rScene;
class rDocument : public QGraphicsView//all in rScene
{
    Q_OBJECT
public:   
    rScene* pscene;
    QTabWidget* tabWidget;
    explicit rDocument(QWidget *parent = 0,QTabWidget* tabW=0);
private:
    bool mouseDown;
    QPointF sceneDownPos;
    QPoint mouseMovePos;
    QTimer scrollTimer;
    void keyPressEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
};
#endif // RDOCUMENT_H
