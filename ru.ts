<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>DebugDialog</name>
    <message>
        <location filename="debugdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainW</name>
    <message>
        <location filename="mainw.ui" line="208"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Export to</source>
        <translation type="obsolete">Экспорт в</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">Язык</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="225"/>
        <source>Tree</source>
        <translation>Дерево</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="obsolete">Документ</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="331"/>
        <source>Insert</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="343"/>
        <location filename="mainw.ui" line="540"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="355"/>
        <location filename="mainw.ui" line="530"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Действия</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="14"/>
        <source>REditor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="171"/>
        <location filename="mainw.ui" line="176"/>
        <source>Tab 1</source>
        <translation></translation>
    </message>
    <message>
        <source>Options...</source>
        <translation type="obsolete">Настройки...</translation>
    </message>
    <message>
        <source>Schemes</source>
        <translation type="obsolete">Схемы</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="711"/>
        <source>Export as image to</source>
        <translation>Экспорт как изображение в</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="367"/>
        <source>Insert Logic</source>
        <translation>Втавить дугу</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="370"/>
        <source>Alt+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="379"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="394"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Новый</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="412"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="obsolete">Открыть...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="423"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="438"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="obsolete">Сохратить как...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="455"/>
        <source>Save All</source>
        <translation>Сохранить всё</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="458"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="406"/>
        <location filename="mainw.ui" line="409"/>
        <source>New Project</source>
        <translation>Новый проект</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="417"/>
        <source>Open Project...</source>
        <translation>Открыть проект...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="420"/>
        <source>Open Project</source>
        <translation>Открыть проект</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="432"/>
        <location filename="mainw.ui" line="435"/>
        <source>Save Project</source>
        <translation>Сохранить проект</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="443"/>
        <source>Save Project As...</source>
        <translation>Сохранить проект как...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="446"/>
        <source>Save Project As</source>
        <translation>Сохранить проект как</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="467"/>
        <location filename="mainw.ui" line="470"/>
        <source>Close Project</source>
        <translation>Закрыть проект</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="473"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="482"/>
        <source>Print...</source>
        <oldsource>Print</oldsource>
        <translation>Печать...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="485"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="494"/>
        <source>New Folder</source>
        <translation>Новый каталог</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="503"/>
        <source>New Document</source>
        <translation>Новый документ</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="512"/>
        <source>Close All</source>
        <translation>Закрыть все</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="517"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="520"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="525"/>
        <location filename="mainw.ui" line="638"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="535"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="549"/>
        <source>Hide Text</source>
        <translation>Скрыть текст</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="558"/>
        <source>Change Color...</source>
        <oldsource>Change Color</oldsource>
        <translation>Изменить цвет...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="563"/>
        <source>Clipboard</source>
        <translation>Буффер обмена</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="568"/>
        <source>Png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="573"/>
        <source>Svg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="578"/>
        <source>Print to Pdf</source>
        <translation>Печать в Pdf</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="586"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="594"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="602"/>
        <source>Ukrainian</source>
        <translation>Украинский</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="611"/>
        <source>Font...</source>
        <translation>Шрифт...</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="620"/>
        <source>Scheme from Text</source>
        <oldsource>Sheme from Text</oldsource>
        <translation>Схема из Текста</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="629"/>
        <source>Translate</source>
        <translation>Транслировать</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="646"/>
        <source>revTranslate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="649"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainw.ui" line="658"/>
        <source>Remove Defaults</source>
        <translation>Удалить стандартные</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="680"/>
        <location filename="mainw.ui" line="683"/>
        <source>Advanced Mode</source>
        <translation>Расширенный режим</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="692"/>
        <source>Fold</source>
        <translation>Вложить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="702"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simplify</source>
        <translation type="obsolete">Упростить</translation>
    </message>
    <message>
        <location filename="mainw.ui" line="697"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="211"/>
        <source>Project Tree:</source>
        <oldsource>Projects:</oldsource>
        <translation>Дерево проектов:</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="491"/>
        <source>Open R Project</source>
        <translation>Открыть Р проект</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="491"/>
        <location filename="mainw.cpp" line="526"/>
        <source>R Projects</source>
        <translation>Р проект</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="524"/>
        <source>Save R Project &quot;</source>
        <translation>Сохранить Р проект</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="723"/>
        <location filename="mainw.cpp" line="747"/>
        <location filename="mainw.cpp" line="801"/>
        <source>Unnamed</source>
        <translation>Безымянный</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1089"/>
        <source>Print Documents</source>
        <translation>Печатать документы</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1239"/>
        <source>Other C++ symbols</source>
        <translation>Другие символы C++</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1240"/>
        <source>Shemes symbols</source>
        <translation>Cимволы схем</translation>
    </message>
    <message>
        <source>Export to Svg Image</source>
        <translation type="vanished">Экспорт в  Svg изображение</translation>
    </message>
    <message>
        <source>Svg images (*.svg)</source>
        <translation type="vanished">Изображения Svg (*.svg)</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1133"/>
        <location filename="mainw.cpp" line="1159"/>
        <source>untitled</source>
        <translation>Безымянный</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1130"/>
        <source>Export to Png Image</source>
        <translation>Экспорт в  Png изображение</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1156"/>
        <source>Export to Pdf document</source>
        <translation>Экспорт в документ Pdf</translation>
    </message>
    <message>
        <source>Export to Svg File</source>
        <translation type="obsolete">Экспорт в  Svg файл</translation>
    </message>
    <message>
        <source>Export to Image</source>
        <translation type="obsolete">Экспорт в изображение</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1132"/>
        <source>Png images (*.png)</source>
        <translation>Изображения Png (*.png)</translation>
    </message>
    <message>
        <source>Export to PDF</source>
        <translation type="obsolete">Экспорт в Pdf</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="1158"/>
        <source>Pdf documents (*.pdf)</source>
        <translation>Документы Pdf (*.pdf)</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="options.ui" line="17"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="options.ui" line="32"/>
        <source>Environment</source>
        <translation>Среда</translation>
    </message>
    <message>
        <location filename="options.ui" line="70"/>
        <source>Wait for double click:</source>
        <translation>Ожидать двойное нажатие:</translation>
    </message>
    <message>
        <location filename="options.ui" line="83"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="options.ui" line="100"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="options.ui" line="105"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="options.ui" line="110"/>
        <source>Ukrainian</source>
        <translation>Украинский</translation>
    </message>
    <message>
        <location filename="options.ui" line="124"/>
        <source>Show captions in toolbar</source>
        <translation>Показывать подписи в панели инструментов</translation>
    </message>
    <message>
        <location filename="options.ui" line="137"/>
        <source>Show shortcuts in toolbar</source>
        <translation>Показывать сочетания клавишь в панели инструментов</translation>
    </message>
    <message>
        <location filename="options.ui" line="150"/>
        <source>Start maximized</source>
        <translation>Запускать развернуто</translation>
    </message>
    <message>
        <location filename="options.ui" line="163"/>
        <source>Display points for keyboard editing on:</source>
        <translation type="unfinished">Показывать точки для клавиатурного редактирования на:</translation>
    </message>
    <message>
        <location filename="options.ui" line="176"/>
        <source>Arcs</source>
        <translation type="unfinished">Дугах</translation>
    </message>
    <message>
        <location filename="options.ui" line="189"/>
        <source>Nodes</source>
        <translation type="unfinished">Вершинах</translation>
    </message>
    <message>
        <location filename="options.ui" line="202"/>
        <source>Vertical arcs</source>
        <translation type="unfinished">Вертикальных дугах</translation>
    </message>
    <message>
        <location filename="options.ui" line="208"/>
        <source>C++</source>
        <translation></translation>
    </message>
    <message>
        <location filename="options.ui" line="220"/>
        <source>Serial keywords</source>
        <translation>Последовательные ключ. слова</translation>
    </message>
    <message>
        <location filename="options.ui" line="238"/>
        <source>Block keywords</source>
        <translation>Блочные ключ. слова</translation>
    </message>
    <message>
        <source>Mouse button delay</source>
        <translation type="obsolete">Задержка кнопки мыши</translation>
    </message>
    <message>
        <location filename="options.ui" line="44"/>
        <source>300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="options.ui" line="57"/>
        <source>ms</source>
        <translatorcomment>мс</translatorcomment>
        <translation></translation>
    </message>
</context>
<context>
    <name>rLogic</name>
    <message>
        <source>Predicate</source>
        <translation type="obsolete">Условие</translation>
    </message>
    <message>
        <location filename="rlogic.cpp" line="22"/>
        <source>Condition</source>
        <translation>Условие</translation>
    </message>
    <message>
        <location filename="rlogic.cpp" line="23"/>
        <source>Actions</source>
        <oldsource>Action</oldsource>
        <translation>Действия</translation>
    </message>
</context>
<context>
    <name>rPlugin2::Internal::rPlugin2Plugin</name>
    <message>
        <source>Assign Editor</source>
        <translation type="obsolete">Привязать редактор</translation>
    </message>
    <message>
        <source>Advanced Mode</source>
        <translation type="obsolete">Расширенный режим</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Опции</translation>
    </message>
    <message>
        <source>Hide Text</source>
        <translation type="obsolete">Скрыть текст</translation>
    </message>
    <message>
        <source>Change Color</source>
        <translation type="obsolete">Изменить цвет</translation>
    </message>
    <message>
        <source>Sheme from Text</source>
        <translation type="obsolete">Схема из Текста</translation>
    </message>
    <message>
        <source>Remove Defaults</source>
        <translation type="obsolete">Удалить стандартные</translation>
    </message>
    <message>
        <source>Simplify</source>
        <translation type="obsolete">Упростить</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="obsolete">Вставить</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Копировать</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Вырезать</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Удалить</translation>
    </message>
</context>
<context>
    <name>rScene</name>
    <message>
        <source>Scheme</source>
        <translation type="obsolete">Схема</translation>
    </message>
    <message>
        <location filename="rscene.cpp" line="312"/>
        <source>R-scheme</source>
        <translation>Р-схема</translation>
    </message>
    <message>
        <location filename="rscene.cpp" line="1028"/>
        <source>Condition</source>
        <translation>Условие</translation>
    </message>
    <message>
        <source>Predicate</source>
        <translation type="obsolete">Условие</translation>
    </message>
    <message>
        <location filename="rscene.cpp" line="1032"/>
        <source>Actions</source>
        <translation>Действия</translation>
    </message>
</context>
<context>
    <name>rSheme</name>
    <message>
        <source>R-sheme</source>
        <translation type="obsolete">Р-схема</translation>
    </message>
</context>
<context>
    <name>rTextEdit</name>
    <message>
        <source>Predicate</source>
        <translation type="obsolete">Условие</translation>
    </message>
    <message>
        <location filename="ritextedit.cpp" line="141"/>
        <source>Condition</source>
        <translation>Условие</translation>
    </message>
    <message>
        <location filename="ritextedit.cpp" line="142"/>
        <source>Actions</source>
        <translation>Действия</translation>
    </message>
</context>
<context>
    <name>rplugin::Internal::rpluginPlugin</name>
    <message>
        <location filename="rpluginplugin.cpp" line="82"/>
        <source>REditor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="rpluginplugin.cpp" line="97"/>
        <source>Assign Editor</source>
        <translation>Привязать редактор</translation>
    </message>
</context>
</TS>
