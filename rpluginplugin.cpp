#include "rpluginplugin.h"
#include "rpluginconstants.h"

#include <coreplugin/icore.h>
#include <coreplugin/coreplugin.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/documentmanager.h>
#include <coreplugin/vcsmanager.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/ieditor.h>
#include <texteditor/autocompleter.h>
#include <texteditor/texteditorplugin.h>
#include <texteditor/basetexteditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/icore.h>
#include <coreplugin/minisplitter.h>
#include <coreplugin/editortoolbar.h>
#include <texteditor/plaintexteditor.h>
/*#include <cplusplus/OverviewModel.h>
#include <cplusplus/Symbols.h>
#include <cplusplus/Symbol.h>
#include <cplusplus/Scope.h>
#include <cplusplus/TranslationUnit.h>
#include <cplusplus/Literals.h>*/
#include <aggregation/aggregate.h>
#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QtGui/QtEvents>
#include <QComboBox>
#include <QApplication>
#include <QTextEdit>
#include <QtGui/QTextLayout>
#include <QtGui/QTextBlock>
#include <QScrollBar>
#include <QtCore/QtPlugin>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGroupBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QLayout>
#include <QStackedLayout>
#include <QBoxLayout>
#include <QTimer>
#include "options.h"
#include "mainw.h"
#include "ridocument.h"
#include "rscene.h"

using namespace rplugin::Internal;

rpluginPlugin::rpluginPlugin()
{
    focusEd=0;
    bufferStream.setString(&bufferString);
    tautoupdate.start(1000);
    connect(&tautoupdate,SIGNAL(timeout()),this,SLOT(autoupdate()));
}

rpluginPlugin::~rpluginPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool rpluginPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    Core::ActionManager *am = Core::ActionManager::instance();
    Core::ActionContainer *menu = am->createMenu(Constants::MENU_ID);
    QMenu* qmenu=menu->menu();
    menu->menu()->setTitle(tr("REditor"));
    QString ln=Core::ICore::instance()->settings()->value("General/OverrideLanguage").toString();
    if(ln.length()==0){
        QString dL = QLocale::system().name();
        dL.truncate(dL.lastIndexOf('_'));
        ln=dL;
    }
    if(ln=="ru"){
        Options::instance()->forceLang(1);
    }else if(ln=="ua"){
        Options::instance()->forceLang(2);
    }else{
        Options::instance()->forceLang(0);
    }
    mw=new MainW(0,false);
    QAction *action = new QAction(tr("Assign Editor"), this);
    Core::Command *cmd = am->registerAction(action, Constants::ACTION_ID,
                                            Core::Context(Core::Constants::C_GLOBAL));
    connect(action, SIGNAL(triggered()), this, SLOT(forceEditor()));
    menu->addAction(cmd);
    QMenuBar *menubar =
            am->actionContainer(Core::Constants::MENU_BAR)->menuBar();
    menubar->addMenu(menu->menu());
    actionsToolBar1=mw->gui()->mainToolBar;
    actionsToolBar2=mw->gui()->mainToolBar2;
    actionsToolBar3=mw->gui()->mainToolBarBig;
    mw->gui()->mainToolBar->removeAction(mw->gui()->actionUndo_2);
    mw->gui()->mainToolBar->removeAction(mw->gui()->actionRedo_2);
    mw->gui()->mainToolBar2->removeAction(mw->gui()->actionFont);
    qmenu->addAction(mw->gui()->actionOptions);
    connect(mw,SIGNAL(retranslated()),this,SLOT(mainWRetranslated()));
    mainWRetranslated();
    connect(Core::EditorManager::instance(),SIGNAL(currentEditorChanged(Core::IEditor*)),this,SLOT(currentEditorChanged(Core::IEditor*)));
    connect(Core::EditorManager::instance(),SIGNAL(currentEditorStateChanged(Core::IEditor*)),this,SLOT(currentEditorStateChanged(Core::IEditor*)));
    connect(Core::EditorManager::instance(),SIGNAL(editorCreated(Core::IEditor*,QString)),this,SLOT(editorCreated(Core::IEditor*,QString)));
    connect(Core::EditorManager::instance(),SIGNAL(editorOpened(Core::IEditor*)),this,SLOT(editorOpened(Core::IEditor *)));
    connect(Core::EditorManager::instance(),SIGNAL(editorAboutToClose(Core::IEditor*)),this,SLOT(editorAboutToClose(Core::IEditor*)));
    connect(Core::EditorManager::instance(),SIGNAL(editorsClosed(QList<Core::IEditor*>)),this,SLOT(editorsClosed(QList<Core::IEditor *>)));
    return true;
}

void rpluginPlugin::mainWRetranslated(){
    foreach(QAction* act,actionsToolBar1->actions()+actionsToolBar2->actions()+actionsToolBar3->actions()){
        if(!act->shortcut().isEmpty()){
            act->setShortcut(QKeySequence("Shift+"+act->shortcut().toString()));
            qDebug()<<act->shortcut();
        }
    }
}

void rpluginPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // "In the extensionsInitialized method, a plugin can be sure that all
    //  plugins that depend on it are completely initialized."
}

ExtensionSystem::IPlugin::ShutdownFlag rpluginPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

rIDocument *rpluginPlugin::getEditorDoc(Core::IEditor *e)
{
    foreach(rIDocument* d,ridocs()){
        if(d->m_ed->editor()==e){
            return d;
        }
    }
    return 0;
}

rIDocument* rpluginPlugin::curDoc()
{
    return focusEd;
}

rIDocument *rpluginPlugin::findDupDoc(rIDocument *doc)
{
    foreach(Core::IEditor* e,Core::EditorManager::instance()->documentModel()->oneEditorForEachOpenedDocument()){
        if(getEditorDoc(e)==doc){
            return getEditorDoc(e);
        }
    }
    return 0;
}

QList<rIDocument *> rpluginPlugin::ridocs()
{
    QList<rIDocument *> ret;
    rIDocument* d;
    foreach(QObject* o,Core::EditorManager::instance()->children()){
        if(d=dynamic_cast<rIDocument*>(o)){
            ret.append(d);
        }
    }
    return ret;
}

void rpluginPlugin::forceEditor()
{
    editorOpened(Core::EditorManager::instance()->currentEditor(),true);
}
void rpluginPlugin::triggerAction2()
{
    autoupdate();
}

void rpluginPlugin::autoupdate()
{
    if((Core::EditorManager::instance())&&
            (Core::EditorManager::instance()->parentWidget())&&
            (Core::EditorManager::instance()->parentWidget()->parentWidget())&&
            (Core::EditorManager::instance()->parentWidget()->parentWidget()->layout())&&
            (actionsToolBar1)){
        Core::EditorManager::instance()->parentWidget()->parentWidget()->layout()->addWidget(actionsToolBar1);
        Core::EditorManager::instance()->parentWidget()->parentWidget()->layout()->addWidget(actionsToolBar2);
        Core::EditorManager::instance()->parentWidget()->parentWidget()->layout()->addWidget(actionsToolBar3);
        mw->showToolBar(curDoc()&&(curDoc()->isVisible()));
    }
}

void rpluginPlugin::currentEditorChanged(Core::IEditor *editor){
    if(getEditorDoc(editor)){
    }else{
        editorOpened(editor);
    }
    foreach(rIDocument* d,ridocs()){
        d->autoupdate(10);
    }
}

void rpluginPlugin::currentEditorStateChanged(Core::IEditor *editor){
}

void rpluginPlugin::editorCreated(Core::IEditor *editor, const QString &fileName){
    foreach(rIDocument* d,ridocs()){
        d->autoupdate(10);
    }
}

void rpluginPlugin::editorOpened(Core::IEditor *editor, bool nosign){
    if(!editor) return;
    if(editor->metaObject()->className()!=QString("CppEditor::Internal::CPPEditor")) return;
    if((!getEditorDoc(editor))&&((nosign)||(rIDocument::hasSignature(editor)))){
        rIDocument* doc=new rIDocument(editor->widget());
        connect(doc,SIGNAL(si_focusCall()),this,SLOT(focusCall()));
        doc->show();
        doc->doc_contentsChanged();
    }
    foreach(rIDocument* d,ridocs()){
        d->autoupdate(10);
    }
}

void rpluginPlugin::editorAboutToClose(Core::IEditor *editor){
    if(getEditorDoc(editor)){
        delete getEditorDoc(editor);
    }
}

void rpluginPlugin::editorsClosed(QList<Core::IEditor *> editors){
    if(curDoc()){
        curDoc()->autoupdate(10);
    }
}

void rpluginPlugin::focusCall()
{
    focusEd=dynamic_cast<rIDocument*>(QObject::sender());
    if(focusEd){
        focusEd->pscene->caption=focusEd->m_ed->baseTextDocument()->displayName();
        mw->setIntgScene(focusEd->pscene);
    }
}

Q_EXPORT_PLUGIN(rPlugin2::Internal::rPlugin2Plugin)
