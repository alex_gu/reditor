#include "rdocument.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "options.h"

rDocument::rDocument(QWidget *parent,QTabWidget* tabW) :
    QGraphicsView(parent)
{
    tabWidget=tabW;
    pscene=new rScene(this);
    setScene(pscene);
    setRenderHint(QPainter::Antialiasing, true);
    setRenderHint(QPainter::SmoothPixmapTransform, true);
    setScene(pscene);
    setDragMode(QGraphicsView::RubberBandDrag);
    setMinimumSize(50, 50);
    setAlignment(Qt::AlignLeft | Qt::AlignTop);
    setViewportUpdateMode(FullViewportUpdate);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    mouseDown=false;
}

void rDocument::mouseMoveEvent(QMouseEvent *event)
{
    mouseMovePos=event->pos();
    QGraphicsView::mouseMoveEvent(event);
}

void rDocument::mousePressEvent(QMouseEvent *event)
{
    mouseDown=true;
    sceneDownPos=mapToScene(event->pos());
    QGraphicsView::mousePressEvent(event);
}

void rDocument::mouseReleaseEvent(QMouseEvent *event)
{
    mouseDown=false;
    QGraphicsView::mouseReleaseEvent(event);
}

void rDocument::keyPressEvent(QKeyEvent *event)
{//disable arrow scrolling, todo find another way
    if(pscene->curEd.count())
        QGraphicsView::keyPressEvent(event);
    else
    {
        pscene->keyPressEvent(event);
    }
}


