#include "ridocument.h"
#include "rscene.h"
#include "rsheme.h"
#include "rlogic.h"
#include "rctexedit.h"
#include "rtranslator.h"
#include "options.h"

rIDocument::rIDocument(QWidget *parent) :
    rDocument(parent,NULL)
{
    lastAvailableRedoSteps=0;
    lastAvailableUndoSteps=0;
    lastIsUndoAvailable=false;
    lastIsRedoAvailable=false;
    setParent(Core::EditorManager::instance());
    saveOK=false;
    fastUpdateAll=0;
    allowEdit=true;
    nblockcursor=0;
    maxw=10;
    maxh=10;
    tabWidget=0;
    m_ed=(TextEditor::BaseTextEditorWidget*)parent;
    m_doc=m_ed->document();
    connect(m_ed,SIGNAL(cursorPositionChanged()),this,SLOT(ed_cursorPositionChanged()));
    connect(m_ed,SIGNAL(selectionChanged()),this,SLOT(ed_cursorPositionChanged()));
    connect(m_doc,SIGNAL(undoCommandAdded()),this,SLOT(undoCommandAdded()));
    connect(m_doc,SIGNAL(contentsChanged()),this,SLOT(doc_contentsChanged()));
    connect(m_doc,SIGNAL(contentsChange(int,int,int)),this,SLOT(doc_contentsChange(int,int,int)));
    m_ed->installEventFilter(this);
    m_ed->extraArea()->installEventFilter(this);
    m_ed->viewport()->installEventFilter(this);
    viewport()->installEventFilter(this);
    installEventFilter(this);
    setAttribute(Qt::WA_MouseTracking);
    connect(pscene,SIGNAL(si_changed(bool,bool)),this,SLOT(sceneChanged(bool,bool)));
    m_vsp=m_ed->verticalScrollBarPolicy();
    m_hsp=m_ed->horizontalScrollBarPolicy();
    m_ed->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_ed->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_ed->setLineNumbersVisible(false);
    m_ed->setRevisionsVisible(false);
    m_ed->setCodeFoldingSupported(false);
    trans=new rTranslator(this,false,true);
    QTimer* ti=new QTimer(this); ti->setSingleShot(true); ti->setInterval(200);
    connect(m_ed,SIGNAL(requestFontZoom(int)),ti,SLOT(start()));
    connect(ti,SIGNAL(timeout()),this,SLOT(requestZoom()));
    PaintEventProc=true;
    forced_resize=false;
    tResetCurBlock.setSingleShot(true);
    tResetCurBlock.setInterval(100);
    connect(&tResetCurBlock,SIGNAL(timeout()),this,SLOT(ResetCurBlock()));
    tautoupdate.start(100);
    connect(&tautoupdate,SIGNAL(timeout()),this,SLOT(autoupdate()));
    connect(m_ed,SIGNAL(updateRequest(QRect,int)),this,SLOT(updatereq(QRect,int)));//bug: not all
    updatefontsize();
    redraw_all_edits_timer.setSingleShot(true);
    connect(&redraw_all_edits_timer,SIGNAL(timeout()),this,SLOT(redraw_all_edits()));
    block_updatereq.setSingleShot(true);
    tfixcursor.setSingleShot(true);
    tfixcursor.setInterval(1);
    connect(&tfixcursor,SIGNAL(timeout()),this,SLOT(fixcursor()));
    trestore_txp.setSingleShot(true);
    trestore_txp.setInterval(1);
    connect(&trestore_txp,SIGNAL(timeout()),this,SLOT(restore_txp()));
    tprocessNewFunc.setSingleShot(true);
    tprocessNewFunc.setInterval(2000);
    connect(&tprocessNewFunc,SIGNAL(timeout()),this,SLOT(processNewFunc()));
    tensmEdFocus.setSingleShot(true);
    connect(&tensmEdFocus,SIGNAL(timeout()),this,SLOT(ensmEdFocus()));
    tfixherror.setSingleShot(true);
    tfixherror.setInterval(10);
    connect(&tfixherror,SIGNAL(timeout()),this,SLOT(doc_contentsChanged()));
    setGeometry(0,0,1,1);
    raise();
    autoupdate();
    if(!hasSignature(m_ed->editor())){
        createFun("",m_doc->toPlainText(),false);
        m_ed->setPlainText(m_ed->toPlainText());
    }
    pscene->setAdv(false);
}

bool rIDocument::eventFilter(QObject *obj, QEvent *event)
{
    if((!m_ed)||(!m_ed->parentWidget())){
        return true;
    }
    if(event->type()==QEvent::MouseMove){
        QWidget* w=(QWidget*)obj;
        QMouseEvent* ev=(QMouseEvent*)event;
        trackMouseP=w->mapToGlobal(ev->pos())-mapToGlobal(QPoint());
        if(QRect(QPoint(),size()).contains(trackMouseP)){
            processmEdMouse();
        }
    }else if(event->type()==QEvent::MouseButtonPress){
        if(obj==m_ed->viewport()){
            lastMouseDown=QDateTime::currentMSecsSinceEpoch();
        }
    }else if(event->type()==QEvent::FocusOut){
        if((obj==this)||(obj==m_ed)){
            redraw_all_edits(500);
        }
    }else if(event->type()==QEvent::FocusIn){
        if(obj==this){
            hideCursor();//!!
        }
        if((obj==this)||(obj==m_ed)){
            emit si_focusCall();
            redraw_all_edits(500);
            if(obj==this){
                ensmEdFocus(1000);
            }
        }
    }else if (event->type() == QEvent::Resize) {
        QResizeEvent* ev=(QResizeEvent*)event;
        if(obj==m_ed->parentWidget()){
            autoupdate();
        }else if((obj==m_ed)){
            autoupdate();
            if(ev->size()!=m_edSize)
                return true;
        }
    }else if(event->type()==QEvent::Wheel){
        if(obj==m_ed){
            QWheelEvent* ve=(QWheelEvent*)event;
            wheelEvent(ve);
        }
    }else if(event->type()==QEvent::Show){
        if(obj==m_ed){
            show();
            raise();
        }
    }else if(event->type()==QEvent::Hide){
        if(obj==m_ed){
            hide();
        }
    }else if(event->type()==QEvent::KeyPress){
        if((obj==m_ed)&&(inputLocked)){
            return true;
        }
    }else if(event->type()==QEvent::KeyRelease){
        if((obj==m_ed)&&(inputLocked)){
            return true;
        }
    }
    return QObject::eventFilter(obj, event);
}

rTextEdit *rIDocument::getEd(QString id){
    int p=pscene->editsId.indexOf(id);
    if(p>=0){
        return pscene->edits[p];
    }
    return NULL;
}

bool rIDocument::hasSignature(Core::IEditor *e, int *rpos, QString *stamp)
{
    if(!e){
        return false;
    }
    if(e->widget()->metaObject()->className()!=QString("CppEditor::Internal::CPPEditorWidget")){
        return false;
    }
    QTextBlock b=(dynamic_cast<TextEditor::BaseTextEditorWidget*>(e->widget()))->document()->lastBlock();
    while((b.isValid())&&(b.text()=="")){
        b=b.previous();
    }
    if(b.text().right(9)=="rdocument"){
        if(rpos){
            *rpos=b.previous().previous().previous().position();
        }
        if(stamp){
            *stamp=b.previous().text();
        }
        return true;
    }
    if(rpos){
        *rpos=b.position()+b.length();
    }
    return false;
}
rIDocument::~rIDocument()
{
    allowEdit=false;
    foreach(rSheme* she,pscene->shemes.values()){
        foreach(rLogic* lo,she->logics.values()){
            lo->setSelected(true);
        }
    }
    pscene->elemDelete();
    delete trans;
    delete pscene;
}

void rIDocument::editorsClosed(QList<Core::IEditor*> l){
    qDebug()<<"editorsClosed"<<l;
}

void rIDocument::doc_contentsChange(int from, int charsRemoves, int charsAdded){
    block_updatereq.start(400);
    //qDebug()<<"doc_contentsChange";
}

int rIDocument::doc_contentsChanged(){
    //qDebug()<<"doc_contentsChanged";
    if(!allowEdit){
        return 0;
    }
    bool loaded=checkTimeStamp();
    m_ed->verticalScrollBar()->setValue(0);
    m_ed->horizontalScrollBar()->setValue(0);
    if(unusedLines.count()){
        unusedLines.clear();
    }
    static QString lab="//edit";
    static QString lae="//end";
    bool inb=false;
    int ibeg,iend;
    int twmax=0;
    int tc=0;
    int end=m_doc->lastBlock().blockNumber();
    QTextBlock be=m_doc->lastBlock();
    while((be.isValid())&&(be.text()=="")){
        be=be.previous();
    }
    if(be.text().right(9)=="rdocument"){
        end=be.blockNumber()-2;
    }
    QTextBlock bs=m_doc->firstBlock();
    while((tc<end)&&(bs.isValid())){
        twmax=qMax(twmax,bs.text().count());
        bs=bs.next();
        tc++;
    }
    maxw=twmax*fcharw+extraw+10;
    maxh=(tc)*fcharh+3;
    maxdisph=m_doc->lineCount()*fcharh+3;
    m_ed_resize();
    QTextBlock b=m_doc->firstBlock();
    QList<rTextEdit*> &edl=pscene->edits;
    QList<rSheme*> layshemes;
    QList<rTextEdit*> updateEdits;
    int i=0;
    QString newText;
    QString newLine;
    QString newmLine;
    int tw=0;
    int fltw;
    int firstline;
    int lpos=0;
    bool need_up=false;
    int cury=doc_m;
    int fly=0;
    int plh=0;
    int plh2=0;
    int flh=0;
    int lh=0;
    bool herror=false;
    while(b.isValid()){
        if(!b.isVisible()){
            b.setVisible(true);
        }
        newLine=b.text();
        plh2=plh;
        plh=lh;
        lh=b.layout()->boundingRect().height();
        if(!lh){
            lh=fcharh;
            herror=true;
        }
        if(!inb){
            if(newLine.left(6)==lab){
                QString id=newLine.section("",7);
                i=pscene->editsId.indexOf(id);
                if((i>=0)&&(i<edl.count())){
                    ibeg=lpos+1;
                    inb=true;
                    newText.clear();
                    tw=0;
                    fltw=0;
                    firstline=2;
                }
            }else{
                unusedLines.insert(lpos,newLine);
            }
        }else{
            if(newLine.left(5)==lae){
                edl[i]->loffset=newLine.section("",6).toInt();
                iend=lpos-1;
                need_up=false;
                int oh=edl[i]->m_tb_end-edl[i]->m_tb_beg;
                int nh=iend-ibeg;
                edl[i]->m_tb_beg=ibeg;
                edl[i]->m_tb_end=iend;
                if(newText.length()){
                    newText.chop(1);
                }
                if(newText!=edl[i]->rtext){
                    bool fixx=false;
                    if((newText[0]!='\n')||(newText.length()<=1)){
                        fixx=true;
                    }else
                        if((edl[i]->loffset)&&(newText[edl[i]->loffset]!=' ')){
                            fixx=true;
                        }else
                            if(newText[newText.length()-1]!='\n'){
                                fixx=true;
                            }
                    if(fixx){
                        m_ed->undo();
                        return 1;
                    }
                    edl[i]->rtext=newText;
                    newmLine=newText.section("",2+edl[i]->loffset,-3);
                    if(edl[i]->m_text!=newmLine){
                        edl[i]->m_text=newmLine;
                        updateEdits.append(edl[i]);
                    }
                    need_up=true;
                }
                edl[i]->m_tb_beg_pos=m_doc->findBlockByNumber(ibeg+1).position()+edl[i]->loffset;
                edl[i]->m_tb_end_pos=m_doc->findBlockByNumber(iend).position()-1;
                ////////////

                if(edl[i]->setEdRect(extraw+edl[i]->loffset*fcharw,fly,fltw*fcharw,flh,
                                     0,fly,qMax(tw,fltw)*fcharw+extW(),cury+lh-plh-plh2-fly,
                                     qMax(tw,fltw-edl[i]->loffset)*fcharw+extW()
                                     )){
                    edl[i]->logic->sheme->setLayEn(false);
                    if(!layshemes.contains(edl[i]->logic->sheme)){
                        layshemes.append(edl[i]->logic->sheme);
                    }
                    edl[i]->logic->TextResized();
                    need_up=true;
                }
                if((need_up)&&(edl[i]->id==pscene->curEd)){
                    edl[i]->updateLater(true);
                }
                inb=false;
            }else{
                if(firstline){
                    fltw=newLine.length();
                    firstline--;
                    fly=cury;
                    flh=lh;
                }else{
                    tw=qMax(tw,newLine.length());
                }
            }
            newText.append(newLine);
            newText.append("\n");
        }
        cury+=lh;
        b=b.next();
        lpos++;
    }
    if(!loaded){
        foreach(rTextEdit* e,updateEdits){
            e->textEdited();
        }
    }
    foreach(rSheme* sh,layshemes){
        sh->llayout();
    }
    if(!herror) redraw_all_edits(500);
    else tfixherror.start();
    tprocessNewFunc.start();
    return 0;
}

void rIDocument::ensFocus(QWidget* w,int ms){
    w->setFocus();
    for(int i=0;i<ms/100;i++){
        QTimer* t=new QTimer(this);
        t->setSingleShot(true);
        t->setInterval(100*i);
        connect(t,SIGNAL(timeout()),w,SLOT(setFocus()));
        connect(t,SIGNAL(timeout()),t,SLOT(deleteLater()));
        t->start();
    }
}

void rIDocument::ed_cursorPositionChanged(){
    if(nblockcursor){
        nblockcursor--;
        return;
    }
    tfixcursor.start();
}

void rIDocument::sceneChanged(bool trans, bool byedit){
    if(!allowEdit){
        return;
    }
    if(trans){
        prepareCursorRestore();
        nblockcursor=300;
        allowEdit=false;
        if(byedit) m_ed->undo();
        allowEdit=true;
        rTranslator trans(0,false,true);//todo single
        QStringList gresult;
        trans.translateScene(pscene,&gresult);
        gresult.prepend("/**/");//cursor trap
        QTextCursor sc(m_doc);
        sc.setPosition(0);
        sc.setPosition(m_doc->lastBlock().position()+m_doc->lastBlock().length()-1,QTextCursor::KeepAnchor);
        gresult.append("\n");
        QByteArray data;
        QTextStream s(&data);
        pscene->saveToStream(s);
        s.flush();
        QString s64=QString(data.toBase64());
        timeStamp="//"+QString::number(QDateTime::currentMSecsSinceEpoch());
        gresult.append("//"+s64+"\n"+timeStamp+"\n//"+QString::number(s64.length())+"rdocument\n");
        sc.insertText(gresult.join("\n"));
        nblockcursor=0;
        if(byedit){
            trestore_txp.start();
        }else{
            hideCursor();
        }
    }
    saveOK=false;
}



bool rIDocument::load(bool call_con)
{
    updatefontsize();
    int posb;
    if(hasSignature(m_ed->editor(),&posb,&timeStamp)){
        QTextBlock b=m_doc->findBlock(posb);
        QString doc=b.next().text();
        doc.remove(0,2);
        QByteArray ar=doc.toAscii();
        ar=ar.fromBase64(ar);
        QTextStream s(&ar);
        pscene->loadFromStream(s,false);
        if(call_con){
            doc_contentsChanged();
        }
        fastUpdateAll+=2;
        redraw_all_edits(500);
        return true;
    }
    return false;
}

void rIDocument::updatefontsize(bool force)
{
    if(!allowEdit){
        return;
    }
    QFont fo=m_ed->font();
    if(fo.family()!="Courier New"){
        fo.setFamily("Courier New");
        m_ed->setFont(fo);
    }
    QFontMetrics fm(m_ed->font());
    int nfcharh=fm.height();
    int nfcharw=fm.width(" ");
    extraw=m_ed->extraAreaWidth();
    doc_m=m_ed->document()->documentMargin();
    curw=m_ed->cursorWidth();
    if((force)||((nfcharh!=0)&&(nfcharw!=0)&&((nfcharh!=fcharh)||(nfcharw!=fcharw)))){
        fcharh=nfcharh;
        fcharw=nfcharw;
        qDebug()<<"New Font h="<<nfcharh<<" w="<<nfcharw;
        doc_contentsChanged();
    }
}

void rIDocument::requestZoom()
{
    qDebug()<<"doc zoomed";
    fastUpdateAll+=2;
    updatefontsize(true);
}

void rIDocument::restore_txp()
{
    QTextCursor c(m_doc);
    rTextEdit* e=getEd(QString::number(txp_y));
    if(e){
        c.setPosition(e->m_tb_beg_pos+txp_x);
        m_ed->setTextCursor(c);
    }
}

void rIDocument::processmEdMouse()
{
    QPoint mp=trackMouseP;
    rTextEdit* ee=dynamic_cast<rTextEdit*>(itemAt(mp));
    QRect r(0,0,size().width()-(verticalScrollBar()->isVisible()?verticalScrollBar()->width():0),
            size().height()-(horizontalScrollBar()->isVisible()?horizontalScrollBar()->height():0));
    if((ee)&&(r.contains(mp))){
        setAttribute(Qt::WA_TransparentForMouseEvents);
        mouEd=ee->id;
        QPoint viewpos=(mapFromScene(ee->scenePos())-ee->getEdRect().topLeft()-QPoint(
                            ((mp.x()-mapFromScene(ee->scenePos()).x()>extraw)&&
                             (mp.y()-mapFromScene(ee->scenePos()).y()<fcharh))
                            ?ee->loffset*fcharw:0,0))+QPoint(1,1);
        m_ed->move(viewpos);
    }else{
        setAttribute(Qt::WA_TransparentForMouseEvents,false);
    }
}

void rIDocument::autoupdate(int t)
{
    updatefontsize();
    if(m_ed==0){
        return;
    }
    if(t){
        tautoupdate.setInterval(t);
    }
    if(tautoupdate.interval()<5000){
        tautoupdate.setInterval(tautoupdate.interval()+100);
    }
    if(m_ed->parentWidget()!=m_edParent){
        if(m_edParent){
            m_edParent->removeEventFilter(this);
        }
        m_edParent=m_ed->parentWidget();
        if(m_edParent){
            setAttribute(Qt::WA_TransparentForMouseEvents,false);
            m_edParent->installEventFilter(this);
            m_edParent->setAttribute(Qt::WA_MouseTracking);
        }
    }
    if((m_edParent)&&(isVisible())){
        QPoint np=m_ed->parentWidget()->mapTo(parentWidget(),QPoint());
        setGeometry(np.x(),np.y(),m_edParent->width(),m_edParent->height());
    }
    m_ed_resize();



    if((tautoupdate.interval()>=4000)){

    }
}

void rIDocument::updatereq(const QRect &rect, int dy)
{
    //qDebug()<<"updatereq"<<rect<<dy;
    if(rect.y()==0) tfixherror.start();
    if(block_updatereq.isActive()){
        return;
    }
    if(rect.top()>=maxh){
        return;
    }
    rTextEdit* fe1=getEd(pscene->curEd);
    rTextEdit* fe2=getEd(mouEd);
    if(fe1==fe2) fe2=NULL;
    if((fe1)&&(rect.intersects(fe1->getEdRect()))){
        fe1->updateLater();
    }
    if((fe2)&&(rect.intersects(fe2->getEdRect()))){
        fe2->updateLater();
    }
    if(rect.height()>=maxh){
        redraw_all_edits(500);
        return;
    }
    foreach(rTextEdit* ed,pscene->edits){
        if((rect.intersects(ed->getEdRect()))&&(ed!=fe1)&&(ed!=fe2)){
            ed->updateLater(false);
        }
    }
}

void rIDocument::redraw_all_edits(int del, bool hold)
{
    if(del>0){
        redraw_all_edits_timer.start(del);
        return;
    }
    if(fastUpdateAll){
        fastUpdateAll--;
    }
    rTextEdit* ae=getEd(pscene->curEd);
    if(!ae){
        ae=getEd(mouEd);
    }
    if(ae){
        foreach(rTextEdit* ed,pscene->edits){
            ed->updatetimer.setInterval(300+(ed->scenePos()-ae->scenePos()).manhattanLength()*6/(fastUpdateAll+1));//2!
            ed->updatetimer.start();
        }
    }else foreach(rTextEdit* ed,pscene->edits){
        ed->updatetimer.start();
    }
}

void rIDocument::m_ed_destroyed()
{
    hide();
    delete this;
}


void rIDocument::m_ed_resize()
{
    int di=500;
    int ww=maxw;
    int wh=maxdisph;
    ww=(ww/di+1)*di+fcharw*0;
    wh=(wh/di+1)*di+fcharh*0;
    if(m_ed->size()!=QSize(ww,wh)){
        m_edSize=QSize(ww,wh);
        m_ed->resize(ww,wh);
    }
}

void rIDocument::prepareCursorRestore(int dy){
    int curpos=m_ed->textCursor().position();
    txp_y=-1;
    foreach(rTextEdit* e,pscene->edits){
        if((e->m_tb_beg_pos<=curpos)&&(curpos<=e->m_tb_end_pos)){
            txp_y=e->id.toInt()+dy;
            txp_x=m_ed->textCursor().position()-e->m_tb_beg_pos;
            break;
        }
    }
}

void rIDocument::ResetCurBlock()
{
    nblockcursor=0;
}

void rIDocument::hideCursor(){
    QTextCursor c=m_ed->textCursor();
    c.setPosition(2);
    m_ed->setTextCursor(c);
}

void rIDocument::fixcursor(){
    if(!m_ed){
        return;
    }
    QTextCursor c=m_ed->textCursor();
    int curpos=c.position();
    int doclen=m_doc->characterCount();
    bool hideCur=(c.position()==2);
    bool find=false;
    if(!hideCur){
        foreach(rTextEdit* e,pscene->edits){
            if((e->m_tb_beg_pos<=curpos)&&(curpos<=e->m_tb_end_pos)){
                pscene->curEd=e->id;
                find=true;
                break;
            }
        }
    }
    if((!find)&&(!hideCur)){
        pscene->curEd="";
        bool cch=false;
        int ll=doclen;
        if((!c.hasSelection())&&(curpos<doclen-1)){
            foreach(rTextEdit* e,pscene->edits){
                if(e->m_tb_beg_pos-curpos>0){
                    if(e->m_tb_beg_pos-curpos<ll){
                        ll=e->m_tb_beg_pos-curpos;
                        c.setPosition(e->m_tb_beg_pos);
                        cch=true;
                    }
                }else if(curpos-e->m_tb_end_pos>0){
                    if(curpos-e->m_tb_end_pos<ll){
                        ll=curpos-e->m_tb_end_pos;
                        c.setPosition(e->m_tb_end_pos);
                        cch=true;
                    }
                }
            }
            if(cch){
                m_ed->setTextCursor(c);
                return;
            }
        }
    }
    if((c.hasSelection())&&(!find)){
        rTextEdit* eee=0;
        eee=getEd(lastcurEd);
        if(eee){
            c.setPosition(eee->m_tb_beg_pos);
            c.setPosition(eee->m_tb_end_pos,QTextCursor::KeepAnchor);
            m_ed->setTextCursor(c);
            return;
        }
    }
    if(!find) {
        pscene->curEd="";
        inputLocked=true;
    }else{
        m_ed->setEnabled(true);
        inputLocked=false;
    }
    if(pscene->curEd!=lastcurEd){
        rTextEdit* e=getEd(pscene->curEd);
        if(e){
            e->my_setFocus(Qt::OtherFocusReason, QDateTime::currentMSecsSinceEpoch()-lastMouseDown<100);
        }
        foreach(rTextEdit* e2,pscene->edits){
            if(e!=e2){
                e2->my_clearFocus();
            }
        }
    }
    lastcurpos=c.position();
    if(getEd(pscene->curEd)){
        rTextEdit* ee=getEd(pscene->curEd);
        int dy=c.block().firstLineNumber()-ee->m_tb_beg-1;
        int dx=c.positionInBlock();
        if(dy==0){
            dx-=ee->loffset;
        }
        QPoint curp=QPoint(ee->scenePos().x()+extraw+dx*fcharw+fcharw/2,ee->scenePos().y()+dy*fcharh+fcharh/2);
        if((mapFromScene(ee->scenePos()).x()>>0)&&(mapFromScene(curp).x()+10<width()))
            curp.setX(ee->scenePos().x());
        ensureVisible(QRect(curp,QSize(1,1)),fcharw,fcharh);
    }else{
        hideCursor();
    }
    redraw_all_edits(500);
    lastcurEd=pscene->curEd;
    //qDebug()<<"CurEd:"<<m_scene->curEd;
}

bool rIDocument::processNewFunc()
{
    if(!allowEdit){
        return false;
    }
    bool ch=false;
    foreach(int key,unusedLines.keys()){
        if((unusedLines[key]=="{")&&(unusedLines.contains(key-1))&&
                (unusedLines.contains(key+1))&&(unusedLines[key+1]=="}")){
            QString newFun=unusedLines[key-1];
            if(newFun.length()){
                createFun(newFun,"",true,false);
                ch=true;
            }
        }
    }
    if(ch) {
        m_ed->undo();
        pscene->sl_changed();
    }
    return false;
}

void rIDocument::createFun(QString hea, QString body, bool onelog, bool ch)
{
    pscene->setEns(false,false);
    rSheme* ns=pscene->logicPaste(NULL,false,false);
    pscene->setEns(false,false);
    ns->logics[0]->setKind(kSheme);
    ns->logics[0]->UpText->setText(hea);
    ns->logics[0]->DownText->setText(body);
    txp_y=ns->logics[0]->UpText->id.toInt();
    txp_x=0;
    ns->logics[1]->UpText->setText("");
    ns->logics[1]->DownText->setText("");
    if(!onelog){
        QList<rLogic*> ll;
        ll.append(ns->logics.item(2));//logic
        pscene->elemDelete(&ll,false);
    }
    pscene->setEns(true,true);
    if(ch) pscene->sl_changed();
    restore_txp();
}

bool rIDocument::checkTimeStamp()
{
    QString stamp;
    if((hasSignature(m_ed->editor(),NULL,&stamp))&&(stamp!=timeStamp)){
        load(false);
        return true;
    }
    return false;
}

void rIDocument::ensmEdFocus(int d)
{
    if(d==0){
        if(pscene->mouseDown) {
            ensmEdFocus(tensmEdFocus.interval());
        }else{
            m_ed->setFocus();
        }
    }else{
        tensmEdFocus.start(d);
    }
}
