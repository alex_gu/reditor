#ifndef RPLUGINCONSTANTS_H
#define RPLUGINCONSTANTS_H

namespace rplugin {
namespace Constants {

const char ACTION_ID[] = "rplugin.Action";
const char MENU_ID[] = "rplugin.Menu";

} // namespace rplugin
} // namespace Constants

#endif // RPLUGINCONSTANTS_H

