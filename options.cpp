#include "options.h"
#include "ui_options.h"

static Options* inst=0;

Options::Options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Options)
{
    curLang=-2;
    ui->setupUi(this);
    set=new QSettings(QApplication::applicationDirPath()+"/rEditor_options.ini", QSettings::IniFormat);
    show(false);
}

Options::~Options()
{
    delete set;
    delete ui;
}

Options *Options::instance()
{
    static Options opt;
    return &opt;
}

void Options::show(bool s)
{
    ui->plainTESeq->setPlainText(set->value("cpp/SeqGen").toStringList().join("\n"));
    ui->plainTEBlock->setPlainText(set->value("cpp/BlockGen").toStringList().join("\n"));
    ui->mbdlineEdit->setText(set->value("ui/Scene/MouseButtonDelay",300).toString());
    ui->checkBoxShowCapt->setChecked(set->value("ui/checkBoxShowCapt",false).toBool());
    ui->checkBoxShowShort->setChecked(set->value("ui/checkBoxShowShort",false).toBool());
    ui->checkBoxStartMax->setChecked(set->value("ui/checkBoxStartMax",true).toBool());
    ui->checkBoxKeybEditNodes->setChecked(set->value("ui/checkBoxKeybEditNodes",true).toBool());
    ui->checkBoxKeybEditLogics->setChecked(set->value("ui/checkBoxKeybEditLogics",false).toBool());
    ui->checkBoxKeybEditVerticals->setChecked(set->value("ui/checkBoxKeybEditVerticals",false).toBool());
    compact=set->value("ui/Scene/compact",false).toBool();
    int l=0;
    QString dL = QLocale::system().name();
    dL.truncate(dL.lastIndexOf('_'));
    if(dL=="ru"){
        l=1;
    }else  if(dL=="ua"){
        l=2;
    }
    int v=set->value("ui/Language",l).toInt();
    setLI(v);
    if(s){
       QDialog::show();
    }
}

void Options::on_buttonBox_accepted()
{
    save();
}

void Options::setLI(int i)
{
    ui->LanguagecomboBox->setCurrentIndex(i);
    if(i>=0 && curLang==i) return;
    if(i>=0){
        curLang=i;
    }
    switch (curLang){
    case 1:
        emit retranslate("ru","ru");
        break;
    case 2:
        emit retranslate("ua","ru");//todo
        break;
    default:
        emit retranslate("en","en");
    }
    ui->retranslateUi(this);
}

void Options::save(bool lang)
{
    cppSeqgen=ui->plainTESeq->toPlainText().split("\n");
    set->setValue("cpp/SeqGen",cppSeqgen);
    cppBlockgen=ui->plainTEBlock->toPlainText().split("\n");
    set->setValue("cpp/BlockGen",cppBlockgen);
    uiSceneMousebuttondelay=ui->mbdlineEdit->text().toInt();
    set->setValue("ui/Scene/MouseButtonDelay",QString::number(uiSceneMousebuttondelay));
    showCaptions=ui->checkBoxShowCapt->isChecked();
    showShorts=ui->checkBoxShowShort->isChecked();
    startMax=ui->checkBoxStartMax->isChecked();
    set->setValue("ui/checkBoxShowCapt",showCaptions);
    set->setValue("ui/checkBoxShowShort",showShorts);
    set->setValue("ui/checkBoxStartMax",startMax);
    keditNodes=ui->checkBoxKeybEditNodes->isChecked();
    keditLogics=ui->checkBoxKeybEditLogics->isChecked();
    keditVerticals=ui->checkBoxKeybEditVerticals->isChecked();
    set->setValue("ui/checkBoxKeybEditNodes",keditNodes);
    set->setValue("ui/checkBoxKeybEditLogics",keditLogics);
    set->setValue("ui/checkBoxKeybEditVerticals",keditVerticals);
    set->setValue("ui/Scene/compact",compact);
    if(lang && ui->LanguagecomboBox->isEnabled()){
        setLI(ui->LanguagecomboBox->currentIndex());
    }
    setLI(curLang);
    set->setValue("ui/Language",curLang);
    emit changed();
}

void Options::forceLang(int i)
{
    setLI(i);
    ui->LanguagecomboBox->setEnabled(false);
    ui->checkBoxStartMax->setEnabled(false);
}

