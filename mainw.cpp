#define NOMINMAX
#include "mainw.h"
//#include <QtSvg>
#include <windows.h>
#include "debugdialog.h"
#include "rtranslator.h"
#include "qzipreader_p.h"
#include "qzipwriter_p.h"

#include "rdocument.h"
#include "rscene.h"
#include "rsheme.h"
#include "rctexedit.h"
#include "rlogic.h"
#include "rnode.h"
#include "rtranslator.h"
#include "options.h"
#include "redselection.h"

static bool rmDir(const QString &dirPath,bool self=true);
static bool cpDir(const QString &srcPath, const QString &dstPath1);

MainW::MainW(QWidget *parent, bool create_doc) :
    DocumentWindow(parent),
    ui(new Ui::MainW)
{
    fresh=false;
    noOnTreeWidgetItemChanged=false;
    ui->setupUi(this);
    delete ui->tab;
    delete ui->tab_2;
    intgScene=0;
    qDebug()<<"dir= "+QApplication::applicationDirPath();
    wdir=QApplication::applicationDirPath()+"/wdir";
    QDir().mkdir(wdir);
    bdir=QApplication::applicationDirPath()+"/bdir";
    QDir().mkdir(bdir);
    mwi=new QTreeWidgetItem(ui->treeWidget);
    treeWidgetUpdate();
    onActionSaveAS=false;
    QObject::connect(QApplication::instance(),SIGNAL(focusChanged(QWidget *, QWidget *)),this,SLOT(focusChanged(QWidget *, QWidget *)));
    connect(Options::instance(),SIGNAL(retranslate(QString,QString)),this,SLOT(retranslate(QString,QString)));
    Options::instance()->setLI(-1);
    Options::instance()->save(false);
#ifndef DEFINTEG
    if((!mwi->childCount())){
        if(create_doc){
            QTimer::singleShot(1000,this,SLOT(on_actionNew_triggered()));
        }
    }else{
        QList<QTreeWidgetItem*> q;
        q.append(mwi);
        while(q.length()){
            QTreeWidgetItem* item=q.first();
            if(item->text(3).toInt()==2){
                item->setExpanded(!item->isExpanded());
                tabwidgetShowDoc(item->text(1)+"/doc.rdoc");
                break;
            }else{
                for(int i=0;i<item->childCount();i++){
                    q.append(item->child(i));
                }
            }
            q.removeFirst();
        }
    }
    registerFileType("rpj.Project",          // Document type name
                     "REditor Project",// User readable file type name
                     ".rpj",                  // file extension
                     0,                         // index of the icon to use for the files.
                     false);                     // register for DDE events
#endif
    QList<int> w; w.append(125); w.append(ui->splitter->width());
    ui->splitter->setSizes(w);
    ui->mainToolBar->installEventFilter(this);
    ui->mainToolBar2->installEventFilter(this);
    ui->mainToolBarBig->installEventFilter(this);
    tcheckToolBar.setSingleShot(true);
    tcheckToolBar.start(200);
    connect(&tcheckToolBar,SIGNAL(timeout()),this,SLOT(checkToolBar()));
    if(Options::instance()->startMax){
        setWindowState(Qt::WindowMaximized);
    }
    toolbv=true;
}

bool MainW::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::Resize) {
        tcheckToolBar.start();
    }
    return QObject::eventFilter(obj, event);
}

void MainW::showToolBar(bool b)
{
    if(b==toolbv) return;
    toolbv=b;
    tcheckToolBar.start();
}

void MainW::sceneShanged()
{
    fresh=false;
}

void MainW::handleMessage(const QString& message)
{
    QMessageBox* mb;
    enum Action {
        Nothing,
        Open,
        Print
    } action;
    action = Nothing;
    QString filename = message;
    if (message.toLower().startsWith("/print ")) {
        filename = filename.mid(7);
        action = Print;
    } else if (!message.isEmpty()) {
        action = Open;
    }
    if (action == Nothing) {
        emit needToShow();
        return;
    }
    switch(action) {
    case Print:
        break;
    case Open:{
        qDebug()<<filename;
        projectOpen(filename);
    }
        emit needToShow();
        break;
    default:
        break;
    }
}

void MainW::treewidgetRen(QString path,QTreeWidgetItem* it){
    QDir dir(path);
    dir.setSorting(QDir::Name|QDir::DirsLast);
    int ch_count=0;
    foreach(QString sup,dir.entryList()){
        if((sup!=".")&&(sup!="..")){
            QString spath=path+"/"+sup;
            QString show_name=sup;
            int k=0;
            if(QFileInfo(spath).isDir()){
                show_name=textFromFile(spath+"/name");
                k=1;
                if(QFileInfo(spath+"/doc.rdoc").exists()){
                    k=2;
                }
            }else if(QFileInfo(spath).suffix()=="rsns"){
                k=3;
                show_name=textFromFile(spath);
            }else{
                continue;
            }
            QTreeWidgetItem * si=NULL;
            for(int i=0;i<it->childCount();i++){
                if(it->child(i)->text(1)==spath){
                    si=it->child(i);
                }
            }
            if(!si){
                si=new QTreeWidgetItem(it,NULL);
                if (k!=3){
                    it->setExpanded(true);
                }
            }
            si->setText(0,show_name);
            si->setText(1,spath);
            si->setText(2,path);
            si->setText(3,QString::number(k));
            if(k==3){
                si->setFlags(si->flags()&~ Qt::ItemIsEditable);
            }else{
                si->setFlags(si->flags()| Qt::ItemIsEditable);
            }
            if(k==1){
                if(si->parent()==mwi){
                    si->setIcon(0,QIcon(":/icons/package.png"));
                } else{
                    si->setIcon(0,QIcon(":/icons/folder.png"));
                }
            }else if(k==2){
                si->setIcon(0,QIcon(":/icons/page.png"));
            }
            treewidgetRen(path+"/"+sup,si);
            ch_count++;
        }
    }
}

void MainW::treewidgetClean(QTreeWidgetItem *it)
{
    for(int i=0;i<it->childCount();){
        if(QFileInfo(it->child(i)->text(1)).exists()){
            treewidgetClean(it->child(i));
            i++;
        }else{
            it->removeChild(it->child(i));
            i=0;
        }
    }
}

void MainW::treeWidgetUpdate(){
    treeWidgetLoded=false;
    mwi->setText(0,tr("Project Tree:"));//todo translate
    mwi->setFlags(mwi->flags()&(~(Qt::ItemIsSelectable)));
    mwi->setExpanded(1);
    treewidgetRen(wdir,mwi);
    treewidgetClean(mwi);
    ui->treeWidget->sortByColumn(0,Qt::AscendingOrder);
    treeWidgetLoded=true;
}

MainW::~MainW()
{
    QDir(wdir).removeRecursively();
    QDir(bdir).removeRecursively();
    delete ui;
}

void MainW::setIntgScene(rScene *sc)
{
    intgScene=sc;
}


void MainW::on_actionCopy_triggered(bool *ok)
{
    if(!scene()){
        return;
    }
    QByteArray bufferAr;
    QTextStream bufferStream(&bufferAr);
    bool ret=scene()->elemCopy(bufferStream);
    if(ok) *ok=ret;
    bufferStream.flush();
    QMimeData *md=new QMimeData;
    md->setData(rScene::mimeT(),bufferAr);
    QApplication::clipboard()->setMimeData(md);
}

void MainW::on_actionDelete_triggered()
{
    if(!scene()){
        return;
    }
    scene()->elemDelete();
}

void MainW::on_actionInsert_triggered()
{
    if(!scene()){
        return;
    }
    const QMimeData *md=QApplication::clipboard()->mimeData();
    if(md->formats().contains(rScene::mimeT())){
        QByteArray bufferAr=md->data(rScene::mimeT());;
        QTextStream bufferStream(&bufferAr);
        bufferStream.flush();
        scene()->elemPaste(bufferStream,NULL,false,!ui->actionAdv->isChecked());
    }
}

void MainW::on_actionInsertLogic_triggered()
{   if(!scene()){
        return;
    }
    scene()->logicPaste();
}

rScene* MainW::scene()
{
    if(intgScene){
        return intgScene;
    }
    rDocument* w=dynamic_cast<rDocument*>(ui->tabWidget->currentWidget());
    if (w){
        return w->pscene;
    }
    return NULL;
}

void MainW::on_actionUndo_2_triggered()
{
    if(!scene()){
        return;
    }
    scene()->doUndo();
}

void MainW::on_actionRedo_2_triggered()
{
    if(!scene()){
        return;
    }
    scene()->doRedo();
}

void MainW::on_tabWidget_tabCloseRequested(int index)
{
    rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(index));
    if(doc==NULL){
        return;
    }
    doc->pscene->save(doc->pscene->path);
    ui->tabWidget->removeTab(index);
}

QString MainW::projectOpen(QString ppath)
{
    QZipReader zip(ppath);
    QString fName=QDir(ppath).dirName();
    fName.chop(4);
    if (!(zip.exists())){
        return "";
    }
    int p=0;
    QString intName;
    do{
        intName=QString::number(p);
        p++;
    }while (QDir(wdir+"/"+intName).exists());
    QZipReader zip_reader(ppath);
    if (zip_reader.exists()){
        QDir().mkdir(wdir+"/"+intName);
        zip_reader.extractAll(wdir+"/"+intName);
    }
    QFile n(wdir+"/"+intName+"/name");
    n.open(QFile::WriteOnly|QFile::Text);
    QTextStream ns(&n);
    ns<< fName;
    n.close();
    QFile f(wdir+"/"+intName+"/"+"opph.txt");
    f.open(QFile::WriteOnly|QFile::Text);
    QTextStream ts(&f);
    ts<< QDir(ppath).path().section('/',0,-2);
    f.close();
    treeWidgetUpdate();
    return wdir+"/"+intName;
}
void MainW::projectSave(QString path,QString as)
{
    QString filename=as;
    QFile(filename).remove();
    QZipWriter zip(filename);
    if (zip.status() != QZipWriter::NoError){
        return;
    }
    zip.setCompressionPolicy(QZipWriter::AutoCompress);
    path =path+"/";
    QDirIterator it(path, QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot,
                    QDirIterator::Subdirectories);
    while (it.hasNext()){
        QString file_path = it.next();
        if(it.fileInfo().isDir()){
            zip.setCreationPermissions(QFile::permissions(file_path));
            zip.addDirectory(file_path.remove(path));
        }else if (it.fileInfo().isFile()){
            QFile file(file_path);
            if (!file.open(QIODevice::ReadOnly)){
                continue;
            }
            zip.setCreationPermissions(QFile::permissions(file_path));
            QByteArray ba = file.readAll();
            zip.addFile(file_path.remove(path), ba);
        }
    }
    zip.close();
    QFile f(path+"/"+"opph.txt");
    f.open(QFile::WriteOnly|QFile::Text);
    QTextStream ts(&f);
    ts<< QDir(as).path().section('/',0,-2);
    f.close();
}

void MainW::on_treeWidget_itemChanged(QTreeWidgetItem *item, int column)
{
    if(noOnTreeWidgetItemChanged){
        return;
    }
    if (!treeWidgetLoded){
        return;
    }
    QString oldph=item->text(1);
    switch(item->text(3).toInt()){
    case 1:
    case 2:
    {
        QFile f(oldph+"/name");
        f.open(QFile::WriteOnly|QFile::Text);
        QTextStream ts(&f);
        ts<< item->text(0);
        f.close();
    }
        break;
    case 3:
        break;
    }
    for(int i=0;i<ui->tabWidget->count();i++){
        rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
        doc->pscene->setPath(doc->pscene->path);
    }
    treeWidgetUpdate();
    fresh=false;
}

void MainW::on_tabWidget_currentChanged(int index)
{
    rDocument* doc = dynamic_cast<rDocument*>(ui->tabWidget->widget(index));
    if(doc==NULL){
        return;
    }
    QList<QString> sl;
    sl.append(QFileInfo(doc->pscene->path).dir().path());
    treewidgetSelectOne(sl);
    QWidgetList wl;
    QStringList tl;
    for(int i=ui->tabWidget->currentIndex()+1;i<ui->tabWidget->count();i++){
        tl.append(ui->tabWidget->tabText(i));
        wl.append(ui->tabWidget->widget(i));
    }
    for(int i=0;i<ui->tabWidget->currentIndex();i++){
        tl.append(ui->tabWidget->tabText(i));
        wl.append(ui->tabWidget->widget(i));
    }
    foreach(QWidget* w,wl){
        ui->tabWidget->insertTab(-1,w,tl[wl.indexOf(w)]);
    }
    doc->setFocus();
}

rDocument*  MainW::tabwidgetGetopened(QString path)
{
    for(int i=0;i<ui->tabWidget->count();i++){
        rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
        if (doc->pscene->path==path){
            return doc;
        }
    }
    return NULL;
}

void MainW::tabwidgetShowDoc(QString path )
{
    QDir f(path);
    if(f.exists()){
        return;
    }
    if(tabwidgetGetopened(path)){
        ui->tabWidget->setCurrentIndex(ui->tabWidget->indexOf(tabwidgetGetopened(path)));
    }else{
        rDocument* ndoc=new rDocument(0,ui->tabWidget);
        QString tms2("");
        ui->tabWidget->insertTab(-1,(QWidget*)ndoc,tms2);
        ndoc->pscene->open(path);
        connect(ndoc->pscene,SIGNAL(si_tree_up()),this,SLOT(treeWidgetUpdate()));
        connect(ndoc->pscene,SIGNAL(siChanged(bool,bool)),this,SLOT(sceneShanged()));
        ui->tabWidget->setCurrentIndex(ui->tabWidget->indexOf(ndoc));
        on_actionAdv_triggered();
    }
}

void MainW::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    switch(item->text(3).toInt()){
    case 1:
        break;
    case 2:
        item->setExpanded(!item->isExpanded());
        tabwidgetShowDoc(item->text(1)+"/doc.rdoc");
        break;
    case 3:
        tabwidgetShowDoc(item->parent()->text(1)+"/doc.rdoc");
        QString name=QFileInfo(item->text(1)).fileName();
        name.chop(5);
        rSheme* sh=scene()->shemes.item(name.toInt());
        if(sh->getFirstLogic())
            scene()->views()[0]->centerOn(sh->getFirstLogic());
        break;
    }
}

void MainW::on_actionOpen_triggered()
{
    QStringList paths = QFileDialog::getOpenFileNames(this, tr("Open R Project"), QString::null, tr("R Projects")+" (*.rpj)");
    foreach(QString path, paths){
        QList<QString> sl;
        sl.append(projectOpen(path));
        treewidgetSelectOne(sl);
    }
}

void MainW::on_actionSave_triggered()
{
    QList<QTreeWidgetItem*> s_prj_List;
    if(ui->treeWidget->selectedItems().count()==0){
        if(mwi->childCount()==1){
            mwi->child(0)->setSelected(true);
        }
    }
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        while (it->parent()!=mwi){
            it=it->parent();
        }
        if(!s_prj_List.contains(it)){
            s_prj_List.append(it);
        }
    }
    foreach(QTreeWidgetItem *it,s_prj_List){
        QFile f(it->text(1)+"/opph.txt");
        f.open(QFile::Text|QFile::ReadOnly);
        QTextStream str(&f);
        QString dir=str.readAll();
        QString path =dir+"/"+it->text(0)+".rpj";
        qDebug()<<"saving:"<<path<<" ex "<<(QFile::exists(path));
        f.close();
        if (!fresh &&((dir.isEmpty())||(!QFile::exists(path))||(onActionSaveAS))){
            QFileDialog dialog(this,tr("Save R Project \"") +" \""+ it->text(0) + "\"",
                               QFileInfo(path).dir().path(),
                               tr("R Projects")+" (*.rpj)");
            dialog.selectFile(it->text(0));
            dialog.setDefaultSuffix("rpj");
           // dialog.setOption(QFileDialog::DontUseNativeDialog,true);
            dialog.setAcceptMode(QFileDialog::AcceptSave);
            if(dialog.exec()){
                path=dialog.selectedFiles().first();
            }else{
                path.clear();
            }
        }
        if(path.length()){
            for(int i=0;i<ui->tabWidget->count();i++){
                rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
                if(doc->pscene->path.left(it->text(1).length())==it->text(1)){
                    doc->pscene->save(doc->pscene->path);
                }
            }
            projectSave(it->text(1),path);
            path+="/";
            QString newName=path.section('/',-2,-1);
            newName.chop(5);
            noOnTreeWidgetItemChanged=true;
            it->setText(0,newName);
            noOnTreeWidgetItemChanged=false;
        }
    }
    onActionSaveAS=false;
}

void MainW::on_actionSave_As_triggered()
{
    onActionSaveAS=true;
    on_actionSave_triggered();
}

void MainW::on_tabWidget_currentChanged(QWidget *arg1)
{
    QGraphicsView* gw=(dynamic_cast<QGraphicsView*>(arg1));
    if(gw){
        if(gw->scene()){
            ui->graphicsView->setScene(gw->scene());
        }
        gw->setFocus();
    }
}

bool MainW::RemoveDirectory(QDir aDir)
{
    bool has_err = false;
    if (aDir.exists()){
        QFileInfoList entries = aDir.entryInfoList(QDir::NoDotAndDotDot |
                                                   QDir::Dirs | QDir::Files);
        int count = entries.size();
        for (int idx = 0; ((idx < count) && (false == has_err)); idx++)
        {
            QFileInfo entryInfo = entries[idx];
            QString path = entryInfo.absoluteFilePath();
            if (entryInfo.isDir()){
                has_err = RemoveDirectory(QDir(path));
            }else{
                QFile file(path);
                if (!file.remove()){
                    has_err = true;
                }
            }
        }
        if (!aDir.rmdir(aDir.absolutePath())){
            has_err = true;
        }
    }
    return(has_err);
}

void MainW::on_actionClose_triggered()
{
    on_actionSave_triggered();
    QList<QTreeWidgetItem*> s_prj_List;
    if(ui->treeWidget->selectedItems().count()==0){
        if(mwi->childCount()==1){
            mwi->child(0)->setSelected(true);
        }
    }
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        while (it->parent()!=mwi){
            it=it->parent();
        }
        if(!s_prj_List.contains(it)){
            s_prj_List.append(it);
        }
    }
    foreach(QTreeWidgetItem *it,s_prj_List){
        for(int i=0;i<ui->tabWidget->count();i++){
            rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
            if (doc->pscene->path.left(it->text(1).length())==it->text(1)){;
                on_tabWidget_tabCloseRequested(i);
            }
        }
        rmDir(it->text(1));
    }
    treeWidgetUpdate();
}

void MainW::focusChanged(QWidget *old, QWidget *now)
{
}

void MainW::retranslate(QString id,QString qid)
{
    qDebug()<<"MainW::retranslate "<<id<<qid;
    translator.load(":/loc/"+id);
    translator2.load(":/loc/qt_"+qid);
    QApplication::installTranslator(&translator2);
    QApplication::installTranslator(&translator);
    ui->retranslateUi(this);
    ui->actionEnglish->setChecked(false);
    ui->actionUkrainian->setChecked(false);
    ui->actionRussian->setChecked(true);
    treeWidgetUpdate();
    emit retranslated();
    checkToolBar();
    qDebug()<<fresh;
    if(fresh){
        on_actionClose_All_triggered();
        on_actionNew_triggered();
    }
}

void MainW::checkToolBar(bool r){
    if(!toolbv){
        ui->mainToolBarBig->hide();
        ui->mainToolBar->hide();
        ui->mainToolBar2->hide();
        return;
    }
    if(Options::instance()->showCaptions){
        ui->mainToolBarBig->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        ui->mainToolBar2->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    }else{
        ui->mainToolBarBig->setToolButtonStyle(Qt::ToolButtonIconOnly);
        ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        ui->mainToolBar2->setToolButtonStyle(Qt::ToolButtonIconOnly);
    }
    ui->mainToolBarBig->show();
    ui->mainToolBar->show();
    ui->mainToolBar2->show();
    int w=ui->mainToolBarBig->isVisible()?ui->mainToolBarBig->width():ui->mainToolBar->width();
    int w1=0;
    foreach(QAction* a,ui->mainToolBar2->actions()){
        if(ui->mainToolBar2->widgetForAction(a))
            w1+=ui->mainToolBar2->widgetForAction(a)->width();
    }
    int w2=0;
    foreach(QAction* a,ui->mainToolBar->actions()){
        if(ui->mainToolBar->widgetForAction(a))
            w2+=ui->mainToolBar->widgetForAction(a)->width();
    }
    foreach(QAction* a,ui->mainToolBar->actions()+ui->mainToolBar2->actions()){
        if(!a->shortcut().isEmpty()){
            a->setToolTip(a->text().left(a->text().indexOf('(')-1)+" ("+a->shortcut().toString()+")");
            if(Options::instance()->showShorts){
                a->setText(a->toolTip());
            }
        }
    }
    ui->mainToolBarBig->clear();
    if(w1+w2<w){
        ui->mainToolBarBig->addActions(ui->mainToolBar->actions()+ui->mainToolBar2->actions());
        ui->mainToolBarBig->show();
        ui->mainToolBar->hide();
        ui->mainToolBar2->hide();
    }else{
        ui->mainToolBarBig->hide();
        ui->mainToolBar->show();
        ui->mainToolBar2->show();
    }
    if(r){
        QApplication::processEvents();
        checkToolBar(false);
    }
}

void MainW::on_actionNew_triggered()
{
    bool canFresh=!mwi->childCount();
    int p=0;
    QString intName;
    do{
        intName=QString::number(p);
        p++;
    }while ((QDir(wdir+"/"+intName).exists())&&(p<1000));
    QString fPath=wdir+"/"+intName;
    QDir().mkdir(fPath);
    QFile f1(fPath+"/name");
    f1.open(QFile::WriteOnly);
    QTextStream out1(&f1);
    out1<<tr("Unnamed");
    f1.close();
    treeWidgetUpdate();
    QList<QString> sl;
    sl.append(fPath);
    treewidgetSelectOne(sl);
    on_actionNew_Document_triggered();
    if(canFresh) fresh=true;
}

void MainW::on_actionNew_Folder_triggered()
{
    QString lastCreated;
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        if(QDir(it->text(1)).exists()){
            int p=0;
            do{
                lastCreated=it->text(1)+"/"+QString::number(p);
                p++;
            }while ((QFile(lastCreated).exists()));
            QDir().mkdir(lastCreated);
            QFile f1(lastCreated+"/name");
            f1.open(QFile::WriteOnly);
            QTextStream out1(&f1);
            out1<<tr("Unnamed");
            f1.close();
            treeWidgetUpdate();
            QList<QString> sl;
            sl.append(lastCreated);
            treewidgetSelectOne(sl);
        }
    }
}

QTreeWidgetItem* MainW::treewidgetItemByPath(QTreeWidgetItem* par,QString& path)
{
    for(int i=0;i<par->childCount();i++){
        if(par->child(i)->text(1)==path){
            return par->child(i);
        } else{
            QTreeWidgetItem *ret=treewidgetItemByPath(par->child(i),path);
            if(ret){
                return ret;
            }
        }
    }
    return NULL;
}

void MainW::treewidgetSelectOne(QList<QString> paths,bool clean)
{
    if(clean){
        ui->treeWidget->clearSelection();
    }
    foreach(QString path,paths){
        QTreeWidgetItem *it=treewidgetItemByPath(mwi,path);
        if(it){
            it->setSelected(1);
            ui->treeWidget->scrollToItem(it);
        }
    }

}

void MainW::on_actionNew_Document_triggered()
{
    QString lastCreated;
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        if(QDir(it->text(1)).exists()){
            int p=0;
            do{
                lastCreated=it->text(1)+"/"+QString::number(p);
                p++;
            }while ((QDir(lastCreated).exists()));
            QDir().mkdir(lastCreated);
            QFile f1(lastCreated+"/name");
            f1.open(QFile::WriteOnly);
            QTextStream out1(&f1);
            out1<<tr("Unnamed");
            f1.close();
            QString new_name=lastCreated+"/doc.rdoc";
            QFile f(new_name);
            QTextStream str(&f);
            f.open(QFile::Text|QFile::WriteOnly);
            str<<0;
            f.close();
            tabwidgetShowDoc(new_name);
            QList<QString> sl;
            sl.append(lastCreated);
            treeWidgetUpdate();
            treewidgetSelectOne(sl);
            on_actionInsertLogic_triggered();
            scene()->getSel()->placePoint1(scene()->shemes.values()[0]->nodes.values()[0]->scenePos());//todo
        }
    }
    if(scene()){
        scene()->views()[0]->setFocus(Qt::MouseFocusReason);
        scene()->focusInEvent(0);
    }
}

void MainW::on_actionSave_All_triggered()
{
    QList<QString> s_prj_List;
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        if(!s_prj_List.contains(it->text(1)))
            s_prj_List.append(it->text(1));
    }
    for(int i=0;i<mwi->childCount();i++){
        mwi->child(i)->setSelected(1);
    }
    on_actionSave_triggered();
    treewidgetSelectOne(s_prj_List);
}

void MainW::on_actionClose_All_triggered()
{
    QList<QString> s_prj_List;
    foreach(QTreeWidgetItem *it,ui->treeWidget->selectedItems()){
        if(!s_prj_List.contains(it->text(1)))
            s_prj_List.append(it->text(1));
    }
    for(int i=0;i<mwi->childCount();i++){
        mwi->child(i)->setSelected(1);
    }
    on_actionClose_triggered();
    treewidgetSelectOne(s_prj_List);
}


void MainW::closeEvent(QCloseEvent *)
{
    on_actionClose_All_triggered();
}

void MainW::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    int t=ui->treeWidget->itemAt(pos)->text(3).toInt();
    if((t==1)||(t==2)){
        QMenu* pmenu=new QMenu(ui->treeWidget);
        pmenu->addActions(ui->menuProject->actions());
        pmenu->exec(ui->treeWidget->mapToGlobal(pos));
    }
}

void MainW::on_actionHide_text_triggered()
{
    if(!scene()){
        return;
    }
    scene()->hideText();
}

void MainW::on_actionCopy_2_triggered()
{
    rmDir(bdir,false);
    foreach(QTreeWidgetItem* it,ui->treeWidget->selectedItems()){
        if(!((it->text(3)=="1")||(it->text(3)=="2"))){
            continue;
        }
        for(int i=0;i<ui->tabWidget->count();i++){
            rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
            if (doc->pscene->path.left(it->text(1).length())==it->text(1)){;
                doc->pscene->save(doc->pscene->path);
            }
        }
        int p=0;
        QString pp;
        do{
            pp=bdir+"/"+QString::number(p);
            p++;
        }while (QDir(pp).exists());
        cpDir(it->text(1),pp);
    }
}

void MainW::on_actionDelete_2_triggered()
{
    foreach(QTreeWidgetItem* it,ui->treeWidget->selectedItems()){
        for(int i=0;i<ui->tabWidget->count();i++){
            rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
            if (doc->pscene->path.left(it->text(1).length())==it->text(1)){;
                on_tabWidget_tabCloseRequested(i);
            }
        }
        if(QDir(it->text(1)).exists()){
            rmDir(it->text(1));
        }else{
            QFile f(it->text(1));
            f.remove();
        }
    }
    treeWidgetUpdate();
}

static bool rmDir(const QString &dirPath,bool self)
{
    QDir dir(dirPath);
    if (!dir.exists()){
        return true;
    }
    foreach(const QFileInfo &info, dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        if(info.isDir()){
            if(!rmDir(info.filePath())){
                return false;
            }
        }else{
            if(!dir.remove(info.fileName())){
                return false;
            }
        }
    }
    QDir parentDir(QFileInfo(dirPath).path());
    if(self){
        return parentDir.rmdir(QFileInfo(dirPath).fileName());
    }else{
        return true;
    }
}

static bool cpDir(const QString &srcPath, const QString &dstPath1)
{
    QString dstPath=dstPath1;
    QString upD=dstPath;
    while(upD.length()){
        if(upD==srcPath){
            return 0;
        }
        upD=upD.section('/',0,-2);
    }
    int p=0;
    do{
        dstPath=dstPath1+((p==0)?QString(""):("("+QString::number(p)+")"));
        p++;
    }while (QDir(dstPath).exists());

    QDir parentDstDir(QFileInfo(dstPath).path());
    if (!parentDstDir.mkdir(QFileInfo(dstPath).fileName())){
        return false;
    }
    QDir srcDir(srcPath);
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)){
        QString srcItemPath = srcPath + "/" + info.fileName();
        QString dstItemPath = dstPath + "/" + info.fileName();
        if(info.isDir()){
            if(!cpDir(srcItemPath, dstItemPath)){
                return false;
            }
        } else if (info.isFile()) {
            if (!QFile::copy(srcItemPath, dstItemPath)) {
                return false;
            }
        } else {
            qDebug()<<"Unhandled item"<<info.filePath()<<"in cpDir";
        }
    }
    return true;
}
void MainW::on_actionPaste_triggered()
{
    QDir srcDir(bdir);
    foreach(QTreeWidgetItem* it,ui->treeWidget->selectedItems()){
        foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
            QString srcItemPath = bdir + "/" + info.fileName();
            int p=0;
            QString pp;
            do{
                pp=it->text(1)+"/"+QString::number(p);
                p++;
            }while (QDir(pp).exists());
            cpDir(srcItemPath,pp);
        }
    }
    treeWidgetUpdate();
}

void MainW::on_actionChange_Color_triggered()
{
    if (scene()) {
        scene()->setColor(QColorDialog::getColor(Qt::white, this));
    }
}

void MainW::on_actionClipboard_triggered()
{
    rScene *s = scene();
    if (s != 0) {
        QImage image(s->itemsBoundingRect().size().toSize()+QSize(20,20), QImage::Format_RGB32);
        QPainter painter(&image);
        painter.setRenderHint(QPainter::Antialiasing, true);
        doPaint(painter, s, false);
        QApplication::clipboard()->setImage(image);
    }
}

void MainW::doPaint(QPainter &painter, rScene *scene, bool border)
{
    painter.save();
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter.setRenderHint(QPainter::HighQualityAntialiasing, true);
    painter.setRenderHint(QPainter::TextAntialiasing, true);
    QRectF source=scene->itemsBoundingRect();
    QRectF target =QRectF(QPointF(0,0),source.size());
    scene->clearSelection();
    scene->clearFocus();
    scene->setBackgroundBrush(QBrush(Qt::white));
    source.adjust(-10,-10,10,10);
    target.adjust(0,0,20,20);
    scene->render(&painter,target,source);
    painter.restore();
}

void MainW::doPrint(QPrinter &printer, int min, int max, bool border)
{
    QPainter painter(&printer);
    painter.setRenderHint(QPainter::Antialiasing, true);
    if(min<=0){
        min=1;
    }
    if((max<=0)||(max>(ui->tabWidget->count()))){
        max=ui->tabWidget->count();
    }
    for (int i=min-1;i<max;i++){
        rScene *s=dynamic_cast<rDocument*>(ui->tabWidget->widget(i))->pscene;
        s->clearSelection();
        s->clearFocus();
        if (s!=0){
            doPaint(painter, s, border);
            if(i<ui->tabWidget->count()-1){
                printer.newPage();
            }
        }
    }
}

QList<rDocument *> MainW::rdocs()
{
    QList<rDocument *> ret;
    for(int i=0;i<ui->tabWidget->count();i++){
        rDocument* doc=dynamic_cast<rDocument*>(ui->tabWidget->widget(i));
        if(doc){
            ret.append(doc);
        }
    }
    return ret;
}

QString MainW::textFromFile(QString nname)
{
    QFile f1(nname);
    f1.open(QFile::ReadOnly);
    QTextStream out1(&f1);
    QString show_name=out1.readAll();
    f1.close();
    return show_name;
}


void MainW::on_actionPrint_triggered()
{
    if (ui->tabWidget->count() == 0){
        return;
    }
    QPrinter printer;
    printer.setResolution(300);
    QPrintDialog dialog(&printer, this);
    dialog.setWindowTitle(tr("Print Documents"));
    if (dialog.exec() != QDialog::Accepted){
        return;
    }
    qDebug()<<"printer dpi:"<<printer.resolution();
    doPrint(printer, dialog.minPage(), dialog.maxPage());
}

void MainW::on_actionSvg_triggered()
{
    /*if(scene()){
        QString path;
        QFileDialog dialog(this,tr("Export to Svg Image"),
                           "",
                           tr("Svg images (*.svg)"));
        dialog.selectFile(tr("untitled"));
        dialog.setDefaultSuffix("svg");
        //dialog.setOption(QFileDialog::DontUseNativeDialog,true);
        dialog.setAcceptMode(QFileDialog::AcceptSave);
        if(dialog.exec()){
            path=dialog.selectedFiles().first();
        }
        if (path.isNull()){
            return;
        }
        rScene *s=scene();
        if (s!=0){
            QSvgGenerator svg;
            svg.setFileName(path);
            svg.setSize(s->itemsBoundingRect().size().toSize()+QSize(20,20));
            QPainter painter(&svg);
            painter.setRenderHint(QPainter::Antialiasing, true);
            doPaint(painter, s);
        }
    }*/
}

void MainW::on_actionPng_triggered()
{
    if(scene()){
        QString path;
        QFileDialog dialog(this,tr("Export to Png Image"),
                           "",
                           tr("Png images (*.png)"));
        dialog.selectFile(tr("untitled"));
        dialog.setDefaultSuffix("png");
        //dialog.setOption(QFileDialog::DontUseNativeDialog,true);
        dialog.setAcceptMode(QFileDialog::AcceptSave);
        if(dialog.exec()) path=dialog.selectedFiles().first();
        if (path.isNull()){
            return;
        }
        rScene *s = scene();
        if (s != 0) {
            QImage image(s->itemsBoundingRect().size().toSize() + QSize(20, 20), QImage::Format_RGB32);
            QPainter painter(&image);
            painter.setRenderHint(QPainter::Antialiasing, true);
            doPaint(painter, s, false);
            image.save(path, "PNG");
        }
    }
}

void MainW::on_actionPdf_triggered()
{
    if(scene()){
        QString path;
        QFileDialog dialog(this,tr("Export to Pdf document"),
                           "",
                           tr("Pdf documents (*.pdf)"));
        dialog.selectFile(tr("untitled"));
        dialog.setDefaultSuffix("pdf");
        //dialog.setOption(QFileDialog::DontUseNativeDialog,true);
        dialog.setAcceptMode(QFileDialog::AcceptSave);
        if(dialog.exec()) path=dialog.selectedFiles().first();
        if (path.isNull()){
            return;
        }
        QPrinter printer;
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(path);
        doPrint(printer);
    }
}

void MainW::on_actionRussian_triggered()
{

}

void MainW::on_actionUkrainian_triggered()
{

}

void MainW::on_actionEnglish_triggered()
{

}

void MainW::on_actionExit_triggered()
{
    close();
}

void MainW::on_actionFont_triggered()
{
    bool ok;
    QFont f=QFontDialog::getFont(&ok,QFont("Arial", 8),this);
    if (ok){
        scene()->setFont(f);
    }
}

void MainW::on_actionSheme_from_Text_triggered()
{
    if(scene()){
        scene()->createShemeFromSelText();
    }
}

DebugDialog * dial=NULL;
void MainW::on_actionTranslate_triggered()
{
    if(dial==NULL){
        dial=new DebugDialog(ui->centralWidget);
    }
    if(!scene()){
        return;
    }
    if(!scene()->shemes.count()){
        return;
    }
    QStringList d_info,res;
    rTranslator trans(0,false,false);
    trans.translateScene(scene(),&res,&d_info);
    dial->setWindowTitle("C++ ["+scene()->caption+"]");
    dial->show();
    dial->raise();
    //info
    int shemeTextCount=0;
    int cppTextCount=res.join("\n").count();//todo
    foreach(rSheme* sh,scene()->shemes.values())
        foreach(rLogic* lo,sh->logics.values())
            shemeTextCount+=lo->getUpText()->text().count()+lo->getDownText()->text().count();
    int otherCppTextCount=cppTextCount-shemeTextCount;
    d_info.prepend("");
    d_info.prepend("");
    d_info.prepend("");
    res.prepend("");
    res.prepend(tr("Other C++ symbols")+":"+QString::number(otherCppTextCount)+"("+QString::number(100*otherCppTextCount/cppTextCount)+"%)");
    res.prepend(tr("Shemes symbols")+":"+QString::number(shemeTextCount)+"("+QString::number(100*shemeTextCount/cppTextCount)+"%)");
    //
    dial->setDebug(scene(),d_info,res);
}

void MainW::on_actionCut_2_triggered()
{
    bool r=false;
    on_actionCopy_triggered(&r);
    if(r){
        on_actionDelete_triggered();
    }
}

void MainW::on_actionRevTranslate_triggered()
{
    if ((scene())){
        rTranslator a;
        a.revTranslateSheme(scene());
    }
}

void MainW::on_actionRemove_Defaults_triggered()
{
    if(scene()){
        scene()->removeDefaults();
    }
}

void MainW::on_actionAdv_triggered()
{
    foreach(rDocument* d,rdocs()){
        d->pscene->setAdv(ui->actionAdv->isChecked());
    }
}

void MainW::on_actionSimplify_triggered()
{
    rTranslator trans(0,false,false);
    if(scene()){
        trans.simplify(scene());
    }
}

void MainW::on_actionOptions_triggered()
{
    Options::instance()->show();
}

void MainW::on_actionNope_triggered()
{

}

void MainW::on_actionExport_as_image_to_triggered()
{
    QMenu menu(this);
    menu.addAction(ui->actionClipboard);
    menu.addAction(ui->actionPng);
    //menu.addAction(ui->actionSvg);
    menu.exec(QCursor::pos());
}
