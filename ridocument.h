#ifndef RIDOCUMENT_H
#define RIDOCUMENT_H

#include <QGraphicsView>
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/documentmanager.h>
#include <coreplugin/vcsmanager.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/ieditor.h>
#include <texteditor/autocompleter.h>
#include <texteditor/texteditorplugin.h>
#include <texteditor/basetexteditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/icore.h>
#include <coreplugin/minisplitter.h>
#include <coreplugin/editortoolbar.h>
#include <coreplugin/editormanager/openeditorsview.h>
#include <coreplugin/editormanager/openeditorswindow.h>
#include <texteditor/plaintexteditor.h>
#include <aggregation/aggregate.h>
#include <QAction>
#include <QDateTime>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QtGui/QtEvents>
#include <QComboBox>
#include <QApplication>
#include <QTextEdit>
#include <QtGui/QTextLayout>
#include <QtGui/QTextBlock>
#include <QScrollBar>
#include <QtCore/QtPlugin>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGroupBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QtCore/QFutureWatcher>
#include <QQueue>
#include <QTimer>
#include "rdocument.h"

class rDocument;
class rTextEdit;
class rScene;
class rSheme;
class rLogic;
class rTranslator;

class rIDocument : public rDocument
{
    Q_OBJECT
public:
    explicit rIDocument(QWidget *parent = 0);
    ~rIDocument();
    QPointer<TextEditor::BaseTextEditorWidget> m_ed;
    QPointer<QTextDocument> m_doc;
    QPointer<QWidget> m_edParent;
    rTranslator* trans;
    int fcharh,fcharw,extraw;
    QTimer tautoupdate;
    QString mouEd;
    bool checkTimeStamp();
    QTimer block_updatereq;
    void ensFocus(QWidget *w, int ms);
    bool eventFilter(QObject *obj, QEvent *event);
    rTextEdit* getEd(QString id);
    static bool hasSignature(Core::IEditor* e,int* rpos=0,QString* stamp=NULL);
    void hideCursor();
signals:
    void si_restore_txp();
    void si_focusCall();
public slots:
    void editorsClosed(QList<Core::IEditor *> l);
    void ed_cursorPositionChanged();
    void doc_contentsChange(int from, int charsRemoves, int charsAdded);
    int doc_contentsChanged();
    void sceneChanged(bool trans=true,bool byedit=false);
    bool load(bool call_con=true);
    void updatefontsize(bool force=false);
    void requestZoom();
    void restore_txp();
    void processmEdMouse();
    void autoupdate(int t=0);
    void updatereq(const QRect &rect, int dy);
    void redraw_all_edits(int del=0,bool hold=false);
    void m_ed_destroyed();
    void m_ed_resize();
    void prepareCursorRestore(int dy=0);
    void ResetCurBlock();
    void fixcursor();
    bool processNewFunc();
    void createFun(QString hea,QString body,bool onelog=true,bool ch=true);
    void ensmEdFocus(int d=0);
    inline int extW(){return doc_m+extraw+curw;}
private:
    bool PaintEventProc;
    int txp_y;
    int txp_x;
    QTimer tResetCurBlock;
    QTimer redraw_all_edits_timer;
    QTimer tensmEdFocus;
    bool allowEdit;
    QTimer tfixcursor;
    QTimer tfixherror;
    QTimer trestore_txp;
    QTimer tprocessNewFunc;
    QQueue<Core::IEditor*> qfix;
    Qt::ScrollBarPolicy m_vsp,m_hsp;
    QTabWidget* tabWidget;
    bool forced_resize;
    QWidget* testw;
    QPoint trackMouseP;
    int  lastcurpos;
    QString lastcurEd;
    int maxw;
    int maxdisph;
    int maxh;
    int nblockcursor;
    int fastUpdateAll;
    int doc_m;
    QHash<int,QString> unusedLines;
    bool saveOK;
    QString timeStamp;
    QSize m_edSize;
    int lastAvailableRedoSteps;
    int lastAvailableUndoSteps;
    bool lastIsUndoAvailable;
    bool lastIsRedoAvailable;
    qint64 lastMouseDown;
    int curw;
    bool inputLocked;
};

#endif // RIDOCUMENT_H
