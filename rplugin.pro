DEFINES += RPLUGIN_LIBRARY \
           DEFINTEG

TRANSLATIONS += ru.ts ua.ts
QT       += core gui svg widgets printsupport

# rplugin files
SOURCES += rpluginplugin.cpp \
        ritextedit.cpp \
        ridocument.cpp \
        rtransparentgraphicsellipseitem.cpp \
        rtranslator.cpp \
        rsheme.cpp \
        rscene.cpp \
        rnode.cpp \
        rlogic.cpp \
        options.cpp \
        mainw.cpp \
        debugdialog.cpp \
        rhighlighter.cpp \
        rdocument.cpp

HEADERS += rpluginplugin.h\
        rplugin_global.h\
        rpluginconstants.h\
        ritextedit.h \
        ridocument.h \
        rtransparentgraphicsellipseitem.h \
        rtranslator.h \
        rsheme.h \
        rscene.h \
        rnode.h \
        rlogic.h \
        options.h \
        mainw.h \
        debugdialog.h \
        qzipwriter_p.h \
        qzipreader_p.h \
        rhighlighter.h \
        rdocument.h \
        rctexedit.h

OTHER_FILES =

FORMS += \
    options.ui \
    mainw.ui \
    debugdialog.ui

RESOURCES += \
    icons.qrc \
    translate.qrc

# Qt Creator linking

QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=C:/Users/Alex/Downloads/qt-creator-opensource-src-3.1.1

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=C:/Users/Alex/Downloads/qt-creator-opensource-src-3.1.1

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes
PROVIDER = rplugin

QTC_PLUGIN_NAME = rplugin
QTC_LIB_DEPENDS += \
    # nothing here at this time

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    texteditor

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)
include($$QTCREATOR_SOURCES/src/plugins/coreplugin/coreplugin.pri)
include($$QTCREATOR_SOURCES/src/plugins/texteditor/texteditor.pri)
include(src/qtsingleapplication.pri)

LIBS += -L$$IDE_PLUGIN_PATH/QtProject
DEFINES -= QT_NO_CAST_TO_ASCII QT_NO_CAST_FROM_ASCII
